# README #

Alpha phase project to create a tool that finds errors in assembly code. To make this process the tool uses POCTPL logic. In this logic the user introduces a logic formula and POCTPL returns the possible execution paths that match  with the specified formula.

In this moment, the application is only tested on a windows 7, and with the free version of IDA Pro herrramienta installed on that computer.

In future versions we will create an installable binary. Thus, it will download the  programs necessary for the execution of the tool, making easier the tool installation.

### What is the porpouse of this repository? ###

The main objective of this repository is the creation and improvement of a tool that can demonstrate the effectiveness of POCTPL for finding vulnerabilities in assembly code. In future versions we will introduce a small manual explaining how to do POCTPL formulas and introduce them into the program.

### What tools I need? ###

The tools that are currently required to run this project are:

* IDA Pro 5.0 (http://out7.hex-rays.com/files/idafree50.exe)
* Python 2.6 (https://www.python.org/ftp/python/2.6/python-2.6.msi)
* ocamlc (http://gallium.inria.fr/~protzenk/caml-installer/ocaml-4.01.0-i686-mingw64-installer3.exe)
* ply (http://www.dabeaz.com/ply/ply-3.4.tar.gz)

### Running ###

To run the application you just have to be in the directory in which the file "programa.py" and run it. This program has two input parameters for it to function properly, if you do not enter one program display help for operation. The input parameters of the application are:

* Application Path to be analyzed.
* Formula to be tested.

Therefore the correct way to call the program is:

./programa.py <binary path> <formula>

### Contact ###

ivan.garcia.ferreira@deusto.es