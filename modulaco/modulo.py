#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import re
import os
#import pydot

class assemblyLine:
	def __init__(self,number, op):
		self.jumpList= []
		self.num = number
		self.opcode = op

	def addJump(self,num):
		self.jumpList.append(num)

def limpiarCadena(cadena):
	cadena = cadena.replace('\t', ' ')
	cadena = cadena.strip()
	cadena = re.sub(";.*", '', cadena)
	return cadena

def saltos(count, funcion):
	count2 = count
	startCount=count
	encuentro=0
	countLinea=0
	dataOp = []
	jumpList = []
	for lista in funcion:
		x = lista.opcode
		if x[:3]=='jmp':
			salto=x[-7:]
			#print salto
			for w in funcion:
				j = w.opcode
				if salto in j and j[:1]!='j':
					funcion[countLinea].addJump(count2)
					count2=startCount
					count+=1
					break
				count2+=1
		else:
			#if (x[:3]=='jnz' or x[:3]=='JNZ' or x[:2]=='jz' or x[:2]=="JZ") and x[:3]!='jmp':
			if (x[:1]=='j' or x[:1]=='J') and x[:3]!='jmp':
				salto=x[-7:]
				encuentro=0
				for o in funcion:
					encuentro=0
					j = o.opcode
					if salto in j and (j[:1]!='j' and j[:1]!='J'):
						funcion[countLinea].addJump(lista.num+1)
						funcion[countLinea].addJump(count2)
						count2=startCount
						count+=1
						encuentro=1
						break
					count2+=1
				if encuentro==0:
					funcion[countLinea].addJump(lista.num+1)
					count2=startCount
			else:
				if x[:4]!='retn':
					funcion[countLinea].addJump(lista.num+1)
				count+=1
		countLinea+=1
	return funcion

#def buscarSaltosMuertos(programa, fichero):
#	countFuncion=0
#	countLinea=0
#	for funcion in programa:
#		for linea in funcion:
#			if linea.opcode[:3]=='jmp' and len(linea.jumpList)==0:
#				print "entro con: "+linea.opcode
#				print "y busco: "+ linea.opcode[-7:]
#				salto=linea.opcode[-7:]
#				for funcion2 in programa:
#					print "[---]"
#					for linea2 in funcion2:
#						print "-------> "+linea2.opcode
#						if salto in linea2.opcode:
#							print "Encuentro algo en: "+linea2.opcode
#						if salto in linea2.opcode and linea2.opcode[:1]!='j':
#							print "y corrijo: "+str(programa[countFuncion][countLinea].num) + " - "+programa[countFuncion][countLinea].opcode
#							print "encontrado en: "+str(linea2.num)+" - "+linea2.opcode
#							print "añado el salto: "+str(programa[countFuncion][countLinea].num)+"-"+str(linea2.num)
#							programa[countFuncion][countLinea].addJump(linea2.num)
#			if len(linea.jumpList)==0:
#				archi = open(fichero, 'r')
#				linea2 = archi.readline()
#				linea2 = re.sub (";.*", '', linea)
#				while salto not in linea:
#					linea = archi.readline().rstrip('\r\n')
#					linea = limpiarCadena(linea)
#			countLinea+=1
#		countLinea=0
#		countFuncion+=1
#	return programa


def leerStartAsm(fichero):
	funcion = []
	archi=open(fichero,'r')
	linea = archi.readline()
	count=1
	lista = []
	linea = re.sub (";.*", '', linea)
	while (linea[:5]!="start" and linea[:4]!="main" and linea[:5]!="_main" and linea[:6]!="_wmain" and linea[:8]!="_WinMain" and linea[:9]!="_wWinMain"):
		linea = archi.readline().rstrip('\r\n')
		linea = limpiarCadena(linea)
	linea=archi.readline().rstrip('\r\n')
	linea = limpiarCadena(linea)
	while (linea[:5]!="start" or linea[:6]!="_wmain" or linea[:8]!="_WinMain") and "end" not in linea:
		if len(linea)>1 and linea[0]!=';' and linea[:4]!="argc" and linea[:4]!="argv" and linea[:4]!="envp":
			aline = assemblyLine(count, linea.replace('\t', ' '))
			slinea = aline.opcode.split()
			if slinea[0].capitalize()=="Mov" or slinea[0].capitalize()=="Push" or slinea[0].capitalize()=="Call" or \
				slinea[0].capitalize()=="Pop" or slinea[0].capitalize()=="Cmp" or slinea[0].capitalize()=="Jz" or \
				slinea[0].capitalize()=="Lea" or slinea[0].capitalize()=="Test" or slinea[0].capitalize()=="Jmp" or \
				slinea[0].capitalize()=="Add" or slinea[0].capitalize()=="Jnz" or slinea[0].capitalize()=="Retn" or \
				slinea[0].capitalize()=="Xor" or slinea[0].capitalize()=="And" or slinea[0][:3]=="loc":
				count+=1
				funcion.append(aline)
				linea = limpiarCadena(linea)
				lista.append(linea)
		linea = archi.readline().rstrip('\r\n')
		linea = limpiarCadena(linea)
	funcion=saltos(1, funcion)
	archi.close()
	return funcion

def leerFuncionesAsm(programa, fichero):
	listaFuncion = []
	funcion = 0
	funcionStart = programa[-1]
	contador = (funcionStart[-1].num)+1
	countStFun=1
	with open(fichero, 'r') as archi:
		for l in archi:
			if l!='':
				linea = limpiarCadena(l)
				if funcion == 0:
					if "sub_" in linea[:4] and "proc near" in linea:
						if linea!='':
							opc = assemblyLine(contador,linea.replace('\t', ' '))
							countStFun=contador
							listaFuncion.append(opc)
							funcion=1
							contador+=1
				else:
					if "endp" not in linea:
						if linea!='' and "= dword ptr " not in linea and "= word ptr " not in linea and "= byte ptr " not in linea:
							listaFuncion.append(assemblyLine(contador, linea))
							contador+=1
					else:
						funcion=0
						listaFuncion=saltos(countStFun,listaFuncion)
						programa.append(listaFuncion)
						listaFuncion=[]
	archi.close()
	return programa

def llamadasFun(programa):
	countFuncion=0
	countLinea=0
	for funcion in programa:
		for linea in funcion:
			if "ReadFile" in linea.opcode:
				print linea.opcode
			if "call sub" in linea.opcode:
				llamada = linea.opcode[9:]
				#print llamada
				for funcion2 in programa:
					if llamada in funcion2[0].opcode and funcion2[0].opcode!=linea.opcode:
						if programa[countFuncion][countLinea].num < funcion2[0].num:
							# print "----------------"
							# print "Llamada de: "+str(programa[countFuncion][countLinea].num)+" a: "+str(funcion2[0].num)
							programa[countFuncion][countLinea].jumpList.append(funcion2[0].num+1)
							#print program[countFuncion][countLinea].jumpList
							break
			if "jmp loc_" in linea.opcode:
				llamada = linea.opcode[8:]
				for funcion2 in programa:
					if llamada in funcion2[0].opcode and funcion2[0].opcode!=linea.opcode:
						if programa[countFuncion][countLinea].num < funcion2[0].num:
							# print "----------------"
							# print "Encuentro un jmp loc:"+linea.opcode
							# print "Salto de: "+str(programa[countFuncion][countLinea].num)+" a: "+str(funcion2[0].num)
							programa[countFuncion][countLinea].jumpList.append(funcion2[0].num+1)
							break
			if "jmp short loc_" in linea.opcode:
				llamada = linea.opcode[14:]
				for funcion2 in programa:
					if llamada in funcion2[0].opcode and funcion2[0].opcode!=linea.opcode:
						if programa[countFuncion][countLinea].num < funcion2[0].num:
							# print "----------------"
							# print "Encuentro un jmp short loc:"+linea.opcode
							# print "Salto de: "+str(programa[countFuncion][countLinea].num)+" a: "+str(funcion2[0].num)
							programa[countFuncion][countLinea].jumpList.append(funcion2[0].num+1)
							break
			if "jmp sub_" in linea.opcode:
				llamada = linea.opcode[8:]
				#print "-->"+llamada
				for funcion2 in programa:
					if llamada in funcion2[0].opcode and funcion2[0].opcode!=linea.opcode:
						if programa[countFuncion][countLinea].num < funcion2[0].num:
							# print "----------------"
							# print "Encuentro un jmp short loc:"+linea.opcode
							#print "Salto de: "+str(programa[countFuncion][countLinea].num)+" a: "+str(funcion2[0].num+1)
							programa[countFuncion][countLinea].jumpList.append(funcion2[0].num+1)
							break
			countLinea+=1
		countLinea=0
		countFuncion+=1
	return programa

def esRegistro(dato):
	if 'eax' == dato:
		return 'Eax'
	elif 'ebx' == dato:
		return 'Ebx'
	elif 'ecx' == dato:
		return 'Ecx'
	elif 'edx' == dato:
		return 'Edx'
	elif 'esi' == dato:
		return 'Esi'
	elif 'edi' == dato:
		return 'Edi'
	elif 'ebp' == dato:
		return 'Ebp'
	elif 'esp' == dato:
		return 'Esp'
	elif 'eip' == dato:
		return 'Eip'
	elif 'ax' == dato:
		return 'Eax'
	elif 'bx' == dato:
		return 'Ebx'
	elif 'cx' == dato:
		return 'Ecx'
	elif 'dx' == dato:
		return 'Edx'
	elif 'ah' == dato:
		return 'AH'
	elif 'al' == dato:
		return 'AL'
	elif 'bh' == dato:
		return 'BH'
	elif 'bl' == dato:
		return 'BL'
	elif 'ch' == dato:
		return 'CH'
	elif 'cl' == dato:
		return 'CL'
	elif 'dh' == dato:
		return 'DH'
	elif 'dl' == dato:
		return 'DL'
	else: return ''

def esPeligrosa(funcionaca):
	archi=open("utils\\funciones.txt",'r')

	linea = archi.readline().rstrip('\r\n')
	while linea!="":
		#print "----->" + linea +" - " + funcionaca
		#raw_input("Press enter to continue")
		if linea==funcionaca:
			archi.close()
			if linea[0]=='_':

				return linea[1:].title()
			else:
				return funcionaca.title()
		linea = archi.readline().rstrip('\r\n')
	archi.close()
	return "Generic"

def pillarUlt(arg):
	tmp=''
	#print arg
	if "+" in arg:
		tmp = arg.split("+")
	elif "-" in arg:
		tmp = arg.split("-")
	elif "*" in arg:
		tmp = arg.split("*")
	elif "/" in arg:
		tmp = arg.split("/")
	return tmp[-1]

def is_number(s):
	try:
		float(s)
		return True
	except ValueError:
		pass

	try:
		import unicodedata
		unicodedata.numeric(s)
		return True
	except (TypeError, ValueError):
		pass

	return False

def processArgument(arg):
	#print "argumento: "+arg
	tmp=''
	if arg[-1:]==',':
		arg=arg[:-1]
	if arg[0]=='[':
		# Descomentar esto si se quiere hacer más preciso
		#if arg[4:5]=='+':
		#	tmp= '('+esRegistro(arg[1:4])+'_plus '
		#	if arg[-2:-1]=='h':
		#		numeraco=arg[5:-2]
		#		if '+' not in numeraco or '-' not in numeraco or '*' not in numeraco:
		#			if '-' not in arg[5:-2] and '+' not in arg[5:-2] and '*' not in arg[5:-2]:
		#				#print (arg[5:-2],16)[0]
		#				# Arreglar esto pero ya
		#				if is_number((arg[5:-2],16)[0]):
		#					tmp+=str(int(arg[5:-2],16))+')'
		#				else:
		#					tmp+="3)"
		#			else:
		#				tmp+="2)"
		#		else:
		#			#print str(int(pillarUlt(numeraco),16))
		#			tmp+=str(int(numeraco[4:],16))+')'
		#	else:
		#		if "+" in arg[5:-1] or "-" in arg[5:-1] or "*" in arg[5:-1]:
		#			tmp+= arg[-2:-1]+')'
		#		else:
		#			# arreglar esto
		#			#tmp+=arg[5:-1]+')'
		#			tmp+='3)'
		#			#print tmp
		#elif arg[4:5]=='-':
		#	tmp= '('+esRegistro(arg[1:4])+'_minus '
		#	if arg[-2:-1]=='h':
		#		numeraco=arg[5:-2]
		#		if '+' not in numeraco or '-' not in numeraco:
		#			tmp+=str(int(arg[5:-2],16))+')'
		#		else:
		#			tmp+=str(int(numeraco[4:],16))+')'
		#	else:
		#		tmp+=arg[5:-1]+')'
		#else:
		#	tmp=''+esRegistro(arg[1:-1])
		tmp = 'Mem '
	elif esRegistro(arg)!='':
		tmp=''+esRegistro(arg)
	elif arg[-1:]=='h':
		tmp='Mem'
	elif arg[:6]=='dword_' or arg[:9]=="ds:dword_":
		tmp= 'Mem '
	elif arg[:3]=='ds:' or arg[:3]=='cs:':
		if esPeligrosa(arg[3:]) == "Readfile":
			tmp=arg[3:].capitalize()
		else:
			tmp = "Mem"
			tmp = "Mem"
	elif arg.isdigit():
		tmp='Mem'
	else:
		tmp='Mem'
	return tmp

def bitsRegistro(registro):
	registro=registro.lower()
	if registro=="eax" or registro=="ebx" or registro=="ecx" or registro=="edx" or \
		registro=="esi" or registro=="edi" or registro=="ebp" or registro=="esp" or \
		registro=="eip" or registro=="cs" or registro=="ds" or registro=="ss" or \
		registro=="es" or registro=="fs" or registro=="gs":
		return 32
	elif registro=="ax" or registro=="bx" or registro=="cx" or registro=="dx" or \
		registro=="si" or registro=="di" or registro=="sp" or registro=="bp" or \
		registro=="ip":
		return 16
	elif registro=="ah" or registro=="al" or registro=="bh" or registro=="bl" or \
		registro=="ch" or registro=="cl" or registro=="dh" or registro=="dl":
		return 8
	else:
		return 0

def memOrReg(argu1, argu2):
	if argu1==0 and argu2==0:
		return "AddAdd32"
	elif argu1==32 and argu2==0:
		return "RegAdd32"
	elif argu1==16 and argu2==0:
		return "RegAdd16"
	elif argu1==8 and argu2==0:
		return "RegAdd8"
	elif argu1==0 and argu2==32:
		return "AddReg32"
	elif argu1==0 and argu2==16:
		return "AddReg16"
	elif argu1==0 and argu2==8:
		return "AddReg8"
	elif argu1==32 and argu2==32:
		return "RegReg32"
	elif argu1==16 and argu2==16:
		return "RegReg16"
	elif argu1==8 and argu2==8:
		return "RegReg8"
	elif argu1==32 and argu2==8:
		return "RegReg8"
	elif argu1==8 and argu2==32:
		return "RegReg8"
	else:
		return "error "+str(argu1)+" "+str(argu2)

def igualarArgumentos(arg1, arg2):
	if (arg1=="Eax" or arg1=="Ebx" or arg1=="Ecx" or arg1=="Edx" or \
		arg1=="Esi" or arg1=="Edi" or arg1=="Ebp" or arg1=="Esp" or \
		arg1=="Eip" or arg1=="Cs" or arg1=="Ds" or arg1=="Ss" or \
		arg1=="Es" or arg1=="Fs" or arg1=="Gs") and \
		(arg2=="Ax" or arg2=="Bx" or arg2=="Cx" or arg2=="Dx" or \
		arg2=="SI" or arg2=="DI" or arg2=="SP" or arg2=="BP" or \
		arg2=="IP"):
		print "32 y 16"
		print str(arg1)+"-"+str(arg2)
		return 2
	if (arg1=="Eax" or arg1=="Ebx" or arg1=="Ecx" or arg1=="Edx" or \
		arg1=="Esi" or arg1=="Edi" or arg1=="Ebp" or arg1=="Esp" or \
		arg1=="Eip" or arg1=="Cs" or arg1=="Ds" or arg1=="Ss" or \
		arg1=="Es" or arg1=="Fs" or arg1=="Gs") and \
		(arg2=="AH" or arg2=="AL" or arg2=="BH" or arg2=="BL" or \
		arg2=="CH" or arg2=="CL" or arg2=="DH" or arg2=="DL"):
		print "32 y 8"
		print str(arg1)+"-"+str(arg2)
		return 1
	if (arg1=="Ax" or arg1=="Bx" or arg1=="Cx" or arg1=="Dx" or \
		arg1=="SI" or arg1=="DI" or arg1=="SP" or arg1=="BP" or \
		arg1=="IP") and \
		(arg2=="AH" or arg2=="AL" or arg2=="BH" or arg2=="BL" or \
		arg2=="CH" or arg2=="CL" or arg2=="DH" or arg2=="DL"):
		print str(arg1)+"-"+str(arg2)
		print "16 y 8"
		return 1
	if (arg2=="Eax" or arg2=="Ebx" or arg2=="Ecx" or arg2=="Edx" or \
		arg2=="Esi" or arg2=="Edi" or arg2=="Ebp" or arg2=="Esp" or \
		arg2=="Eip" or arg2=="Cs" or arg2=="Ds" or arg2=="Ss" or \
		arg2=="Es" or arg2=="Fs" or arg2=="Gs") and \
		(arg1=="Ax" or arg1=="Ax" or arg1=="Cx" or arg1=="Dx" or \
		arg1=="SI" or arg1=="DI" or arg1=="SP" or arg1=="BP" or \
		arg1=="IP"):
		print "16 y 32"
		print str(arg1)+"-"+str(arg2)
		return 4
	if (arg2=="Eax" or arg2=="Ebx" or arg2=="Ecx" or arg2=="Edx" or \
		arg2=="Esi" or arg2=="Edi" or arg2=="Ebp" or arg2=="Esp" or \
		arg2=="Eip" or arg2=="Cs" or arg2=="Ds" or arg2=="Ss" or \
		arg2=="Es" or arg2=="Fs" or arg2=="Gs") and \
		(arg1=="AH" or arg1=="AL" or arg1=="BH" or arg1=="BL" or \
		arg1=="CH" or arg1=="CL" or arg1=="DH" or arg1=="DL"):
		print "8 y 32"
		print str(arg1)+"-"+str(arg2)
		return 3
	if (arg2=="Ax" or arg2=="Bx" or arg2=="Cx" or arg2=="Dx" or \
		arg2=="SI" or arg2=="DI" or arg2=="SP" or arg2=="BP" or \
		arg2=="IP") and \
		(arg1=="AH" or arg1=="AL" or arg1=="BH" or arg1=="BL" or \
		arg1=="CH" or arg1=="CL" or arg1=="DH" or arg1=="DL"):
		print "8 y 16"
		print str(arg1)+"-"+str(arg2)
		return 3
	return 0

def cambiarArgumentos(arg, tipo):
	print arg+"-"+str(tipo)
	print
	if (tipo == 16):
		if (arg == "Eax"):
			print "cambio Eax a Ax"
			return "Ax"
		elif (arg == "Ebx"):
			print "cambio Ebx a Bx"
			return "Bx"
		elif (arg == "Ecx"):
			print "cambio Ecx a Cx"
			return "Cx"
		elif (arg == "Edx"):
			print "cambio Edx a Dx"
			return "Dx"
	elif (tipo == 8):
		if (arg == "Eax"):
			print "cambio Eax a AH"
			return "AH"
		elif (arg == "Ebx"):
			print "cambio Ebx a BH"
			return "BH"
		elif (arg == "Ecx"):
			print "cambio Ecx a CH"
			return "CH"
		elif (arg == "Edx"):
			print "cambio Edx a DH"
			return "DH"
		# a partir de aquí mal
		elif (arg == "Esi"):
			print "cambio Esi a SI"
			return "AH"
		elif (arg == "Edi"):
			print "cambio Edi a DI"
			return "AH"
		elif (arg == "Ebp"):
			print "cambio Ebp a AH"
			return "AH"
		elif (arg == "Ax"):
			print "cambio Ax a AH"
			return "AH"
		elif (arg == "Bx"):
			print "cambio Ebx a BH"
			return "BH"
		elif (arg == "Cx"):
			print "cambio Cx a CH"
			return "CH"
		elif (arg == "Dx"):
			print "cambio Dx a DH"
			return "DH"

def determinarArgumentos(lineaArgumentos):
	argumentos = lineaArgumentos.split()
	numArgs = len(argumentos)
	if numArgs==1:
		bits=bitsRegistro(argumentos[0])
		#print "bits:"+str(bits)
		#print "argumentos:"+ argumentos[0]
		if bits!=0:
			return "Regi"+str(bits)
		else:
			return "Addr32"
	elif numArgs==2:
		igAr = igualarArgumentos(argumentos[0],argumentos[1])
		if (igAr != 0):
			if (igAr == 1):
				#print "Cambio el primer argumento a 8 bits"
				argumentos[0] = cambiarArgumentos(argumentos[0], 8)
				#print "nuevos argumentos: "+argumentos[0]+" "+argumentos[1]
			elif (igAr == 2):
				#print "Cambio el primer argumento a 16 bits"
				argumentos[0] = cambiarArgumentos(argumentos[0], 16)
				#print "nuevos argumentos: "+argumentos[0]+" "+argumentos[1]
			elif (igAr == 3):
				#print "Cambio el segundo argumento a 8 bits"
				argumentos[1] = cambiarArgumentos(argumentos[1], 8)
				#print "nuevos argumentos: "+argumentos[0]+" "+argumentos[1]
			elif (igAr == 4):
				#print "Cambio el segundo argumento a 16 bits"
				argumentos[1] = cambiarArgumentos(argumentos[1], 16)
				#print "nuevos argumentos: "+argumentos[0]+" "+argumentos[1]
			#print "---------------"
		arg1=bitsRegistro(argumentos[0])
		arg2=bitsRegistro(argumentos[1])
		tmp = memOrReg(arg1, arg2)
		return memOrReg(arg1, arg2)
	elif numArgs==3:
		arg1=bitsRegistro(argumentos[0])
		arg2=bitsRegistro(argumentos[1])
		arg3=bitsRegistro(argumentos[2])
		if argumentos[0][0]=='(':
			return memOrReg(bitsRegistro(argumentos[0][1:4]), bitsRegistro(argumentos[2]))
		elif argumentos[1][0]=='(':
			return memOrReg(bitsRegistro(argumentos[1][1:4]), bitsRegistro(argumentos[0]))
		return "0"
	else:
		return ""
		
def cambiarLoc(programa):
	d={}
	contador = 1
	for funcion in programa:
		for linea in funcion:
			if linea.opcode[:3] == "loc":
				if d.has_key(linea.opcode[4:10]):
					linea.opcode = "Loc "+ str(d[linea.opcode[4:10]])
				else:
					linea.opcode = "Loc " + str(contador)
					d[linea.opcode[4:10]] = contador
					contador+=1
	return programa
def ensPoctpl(programa):
	for funcion in programa:
		for linea in funcion:
			# para debuguear como hace le proceso de traduccion
			#if linea.num == 15:
			#	print linea.opcode
			slinea = linea.opcode.split()
			if slinea[0].capitalize()=="Mov" or slinea[0].capitalize()=="Push" or slinea[0].capitalize()=="Call" or \
				slinea[0].capitalize()=="Pop" or slinea[0].capitalize()=="Cmp" or slinea[0].capitalize()=="Jz" or \
				slinea[0].capitalize()=="Lea" or slinea[0].capitalize()=="Test" or slinea[0].capitalize()=="Jmp" or \
				slinea[0].capitalize()=="Add" or slinea[0].capitalize()=="Jnz" or slinea[0].capitalize()=="Retn" or \
				slinea[0].capitalize()=="Xor" or slinea[0].capitalize()=="And" or slinea[0].capitalize()=="Loc":
				elementos = len(slinea)
				tmp=''
				if elementos==1:
					if 'loc'==slinea[0][:3]:
						tmp = slinea[0][:3].capitalize()+' '+slinea[0][4:-1].capitalize()
					else:
						tmp = slinea[0].capitalize()
				elif elementos==2:
					argum = processArgument(slinea[1])
					if "push" in slinea[0]:
						arg = processArgument(slinea[1])
						#if "+" in slinea[1] or "-" in slinea[1]:
						#	am = 'Regi32'
						#else:
						am = determinarArgumentos(slinea[1])
						tmp+='Push ('+am+' '+arg+')'
					elif slinea[0]!="call" and (slinea[0]!="jmp" and argum[1]!='_' and slinea[0]!="retn"):
						contenido = determinarArgumentos(argum)
						tmp = slinea[0].capitalize()+' ('+contenido+' '+argum+')'
					elif slinea[0]=="jmp":
						tmp="Jmp (Addr32 Mem)"
					elif slinea[0]=="retn":
						tmp="Retn"
					elif slinea[0] == "call":
						if argum == "Mem" or argum == "Eax" or argum == "Ebx" or argum == "Ecx" or argum == "Edx" or argum == "Esi" or argum == "Edi" or argum == "Ebp":
							tmp = slinea[0].capitalize() + " Generic"
						else:
							tmp = slinea[0].capitalize() + " " + argum
					elif slinea[0]=="loc":
						print slinea
					else:
						tmp = slinea[0].capitalize()+' '+esPeligrosa(slinea[1])

				elif elementos==3:
					tmp = slinea[0].capitalize()
					if slinea[1]=='short':
						tmp+=' (Addr32 Mem)'
					elif 'push' in slinea[0]:
						arg = processArgument(slinea[2])
						if "offset" in slinea[1]:
								am = "Addr32"
								arg = "Mem"
						else:
							am = determinarArgumentos(slinea[2])
						tmp+=' ('+am+' '+arg+')'
					else:
						arg1 = processArgument(slinea[1])
						arg2 = processArgument(slinea[2])
						if "+" in arg1 or "-" in arg1:
							arg1 = "Mem"
						if "+" in arg2 or "-" in arg2:
							arg2 = "Mem"
						am = determinarArgumentos(arg1+' '+arg2)
						tmp += ' ('+am+' ('+arg1+', ' + arg2+'))'
						if "Ecx_plus" in tmp:
							print linea.opcode
							print tmp
							print arg1
							print "--------------"
				elif elementos==4:
					tmp = slinea[0].capitalize()
					if "call" in slinea[0]:
						tmp = "Call Generic"
					#depurar esto mejor
					elif "cmp" in slinea[0]:
						arg1 = processArgument(slinea[1])
						arg2 = 'Mem'
						am = determinarArgumentos(arg1+ ' '+arg2)
						tmp += ' ('+am+' ('+arg1+', '+arg2+'))'
					elif slinea[1]=='large':
						arg1 = processArgument(slinea[3])
						am = determinarArgumentos('0 '+arg1)
						tmp+= ' ('+am+' (Mem, '+arg1+'))'
					elif slinea[2]=='large':
						arg1 = processArgument(slinea[1])
						am = determinarArgumentos(arg1+'  0')
						tmp+= ' ('+am+' ('+arg1+', Mem)) '
					elif slinea[2]=='ptr':
						arg1 = processArgument(slinea[3])
						if "minus" in arg1 or "plus" in arg1:
							am = determinarArgumentos(arg1[1:4])
						else:
							am = determinarArgumentos(arg1)
						tmp+= ' ('+am+' '+arg1+')'
					elif slinea[2]=='offset':
						arg1 = processArgument(slinea[1])
						am = determinarArgumentos(arg1+' Mem')
						tmp += ' ('+am+' ('+arg1+', Mem))'
					# atencion a la chapuza
					# controla esto que esta mal
					if tmp=="Mov":
						tmp = 'Mov (RegReg32 (Ebp, Esp))'

				elif elementos==5:
					tmp= slinea[0].capitalize()
					if 'push' in slinea[0]:
						arg = processArgument(slinea[2])
						am = determinarArgumentos(slinea[2])
						tmp+=' ('+am+' '+arg+')'
					elif slinea[2]=='ptr':
						arg1 = processArgument(slinea[3])
						arg2 = processArgument(slinea[4])
						am = determinarArgumentos(arg1+ ' '+arg2)
						tmp+= ' ('+am+' ('+processArgument(slinea[3])+', '+arg2+'))'
					elif slinea[3]=='ptr':
						arg1 = processArgument(slinea[1])
						arg2 = processArgument(slinea[4])
						am = determinarArgumentos(arg1+ ' '+arg2)
						tmp+= ' ('+am+' ('+processArgument(slinea[1])+', '+processArgument(slinea[4])+'))'
				linea.opcode=tmp
				#if 'Cmp'==tmp:
				#	print "----------"
				#	print elementos
				#print tmp
				#raw_input("pulsa una tecla para continuar")
			else:				
				if slinea[0][:4] == "loc_":
					linea.opcode = "loc "+ slinea[0][4:]
				elif slinea[0][:1] == 'j':
					linea.opcode = "Jz (Addr32 Mem)"
				else:
					linea.opcode='Retn'
					linea.jumpList = []
			# para debuguear como hace le proceso de traduccion
			#if linea.num == 15:
			#	print tmp
			#	print "elementos: "+str(elementos)
			#	print linea.num
			#	#print "---------------"
			#	raw_input("Pulsa una tecla para continuar...")
	return programa


def crearKripke(programa):
	s_list="{s_List="
	r_list="r_List="
	l_list="l_List="
	saltoTomado=0;
	r_list_provisional = ""
	r_list_provisional2 = ""
	ultimaFuncion = programa[-1]
	ultimaLinea = ultimaFuncion[-1]
	for funcion in programa:
		for linea in funcion:
			if linea.opcode!="":
				#print "Nodo: "+str(linea.num)
				#print "Saltos: "+str(len(linea.jumpList))
				s_list+=str(linea.num)+" @-> \n"
				#if linea.num <> 1:
				#	print r_list_provisional+"("+str(linea.num)+":nat))) @-> \n"
				#	r_list+= r_list_provisional+"("+str(linea.num)+":nat))) @-> \n"
				#	if r_list_provisional2 <> "":
				#		print r_list_provisional2+"("+str(linea.num)+":nat))) @-> \n"
				#		r_list+= r_list_provisional2+"("+str(linea.num)+":nat))) @-> \n"
				for salto in linea.jumpList:
					if linea.num<salto:
						r_list+= "(Pair(("+str(linea.num)+":nat),("+str(salto)+":nat))) @-> \n"
						#if saltoTomado == 0:
						#	print "Preparo primer salto"
						#	saltoTomado=1
						#	r_list_provisional="(Pair(("+str(linea.num)+":nat),"
						#	r_list_provisional2 = ""
						#else:
						#	print "Preparo segundo salto"
						#	r_list_provisional2="(Pair(("+str(linea.num)+":nat),"
				if "Retn" in linea.opcode and linea.num!=ultimaLinea.num:
					l_list+="(Pair(("+str(linea.num)+":nat), Retn)) @-> \n"
					r_list+="(Pair(("+str(linea.num)+":nat), ("+str(ultimaLinea.num)+":nat))) @-> \n"
				else:
					l_list+="(Pair(("+str(linea.num)+":nat), "+linea.opcode+")) @-> \n"
				saltoTomado=0
			#raw_input()
	s_list += "Nil;"
	r_list += "Nil;"
	l_list += "Nil}"

	f=open("results\kripke.txt","w")
	f.write("("+s_list+" "+r_list+" "+l_list+")")
	f.close()

	print "Fichero kripke.txt generado"

def crearGrafica(programa):
	saltoTomado=0;
	ultimaFuncion = programa[-1]
	ultimaLinea = ultimaFuncion[-1]
	ultimo = 0
	ultimoSalto = 1
	graficaFinal = ""
	inserciones = 0
	for funcion in programa:
		for linea in funcion:
			if linea.opcode!="":
				for salto in linea.jumpList:
					if linea.num<salto:
						if linea.num != ultimo:
							#print "[Treenode("+str(linea.num)+", "+str(salto)+")]"
							saltoTomado = 1
							if len(linea.jumpList)<2:
								#print "[Treenode("+str(linea.num)+", "+str(salto)+")]"
								graficaFinal += "[Treenode("+str(linea.num)+", "+str(salto)
								inserciones += 1
						else:
							if saltoTomado == 0:
								#print "[Treenode("+str(ultimo)+", "+str(ultimoSalto)+")]"
								graficaFinal += "[Treenode("+str(ultimo)+", "+str(ultimoSalto)
								inserciones += 1
							else:
								#print "[Treenode ("+str(linea.num)+", [Treenode ("+str(salto)+", "+str(ultimoSalto)+")]"
								graficaFinal += "[Treenode ("+str(linea.num)+", [Treenode ("+str(salto)+", "+str(ultimoSalto)
								inserciones += 2
								saltoTomado = 0
						ultimo = linea.num
						ultimoSalto = str(salto)
	while(inserciones!=0):
		graficaFinal += ")]"
		inserciones -= 1
	f=open("results\grafica.txt","w")
	f.write("("+graficaFinal+")")
	f.close()

	print "Fichero grafica.txt generado"
"""
# Crear el gráfico con: dot -Tps grafico.dot -o grafico.ps
def puntoDot(programa):
	f = open("results\grafico.dot","w")
	d = pydot.Dot()
	f.write("digraph G {\n")
	f.write("{\n")
	for funcion in programa:
		for linea in funcion:
			if "Retn" in linea.opcode:
				f.write(str(linea.num)+" [shape=circle, style=filled, fillcolor=red]\n")
			#if "Jmp" in linea.opcode or "Jz" in linea.opcode or "Jnz" in linea.opcode:
			#	f.write(str(linea.num)+" [shape=circle, style=filled, fillcolor=yellow]\n")
			if "Readfile" in linea.opcode:
				f.write(str(linea.num+1)+" [shape=circle, style=filled, fillcolor=yellow]\n")
	
	ficheraco = open("tmp\inicial.dat","r")
	for lineaInicios in ficheraco:
		f.write(lineaInicios.strip('\n')+" [shape=circle, style=filled, fillcolor=green]\n")
	ficheraco.close()
	f.write("}\n")
	huboSalto = 0
	varTemp = 1
	lineaAnterior1 = 1
	saltoAnterior = 1
	for funcion in programa:
		for linea in funcion:
			
			###################################################################
			#
			# Dibuja el grafo total
			#
			###################################################################
			
			for salto in linea.jumpList:
				if salto > linea.num and linea.opcode <> "":
					f.write(str(linea.num)+" -> "+str(salto)+";\n")
		
			###################################################################
			#
			# Parte nueva para los grafos, todavia no va bien, hay que pensar mas
			#
			###################################################################
		
			#print "linea: "+str(linea.num)
			#print "Numero de saltos: "+ str(len(linea.jumpList))
			#print "salto a:"
			#for salto in linea.jumpList:
			#	print salto
			#print "Calculo el salto anterior:"
			#print str(saltoAnterior)+" - "+str(linea.num)
			#if saltoAnterior <> linea.num:
			#	lineaAnterior1 = linea.num
			#if (len(linea.jumpList)>1):
			#	print "Escribo este salto: "+str(lineaAnterior1)+" -> "+str(linea.num)
			#	f.write(str(lineaAnterior1)+" -> "+str(linea.num)+";\n")
			#	for salto in linea.jumpList:
			#		if (salto <> linea.num+1):
			#			print "Escribo este salto: "+str(linea.num)+" -> "+str(salto)
			#			lineaAnterior1 = linea.num
			#		else:
			#			saltoAnterior = salto
			#else:
			#	print "Solo hay un salto"
			#	if len(linea.jumpList) == 1:
			#		if linea.jumpList[0] <> linea.num+1:
			#			print "Escribo este salto: "+str(lineaAnterior1)+" -> "+str(linea.num)
			#			print "Escribo este salto: "+str(linea.num)+" -> "+str(linea.jumpList[0])
			#			f.write(""+str(lineaAnterior1)+" -> "+str(linea.num)+";\n")
			#			f.write(str(linea.num)+" -> "+str(linea.jumpList[0])+";\n")
			#		else:
			#			saltoAnterior = linea.jumpList[0]
			#print "---------------"
			#raw_input ()
			
			###################################################################
			#
			# Parte antigua para los grafos
			#
			###################################################################
			
			#		if linea.num<salto:
			#			print str(linea.num)+" - "+str(salto)
			#			if(linea.num+1)!= salto:
			#				print "Entro en el primero\n"
			#				if lineaAnterior1 == linea.num:
			#					print "Escribo : "+str(lineaAnterior1)+" -> "+str(lineaAnterior2)+";\n"
			#					f.write(str(lineaAnterior1)+" -> "+str(lineaAnterior2)+";\n")
			#				print "Escribo : "+str(varTemp)+" -> "+str(linea.num)+";\n"
			#				f.write(str(varTemp)+" -> "+str(linea.num)+";\n")
			#				print "Escribo: "+str(linea.num)+" -> "+str(salto)+";\n"
			#				f.write(str(linea.num)+" -> "+str(salto)+";\n")
			#				huboSalto = 1
			#				raw_input ()
			#			else:
			#				print "Entro en el segundo\n"
			#				if huboSalto == 1:
			#					huboSalto = 0
			#					varTemp = linea.num
			#				lineaAnterior1 = linea.num
			#				lineaAnterior2 = salto
			#			if "Retn" in linea.opcode:
			#				f.write(str(lineaAnterior1)+" -> "+str(linea.num+1)+";\n")
	#f.write(str(varTemp)+" -> "+str(lineaAnterior2)+";\n")
	f.write("}")
	#d.write_png("grafico.png")
	f.close()
"""
def leerEstados():
	archi=open("results\grafico.dot",'r')
	lineas = len(open('results\grafico.dot').readlines())
	contador = 1
	linea = archi.readline()
	linea = archi.readline().rstrip(';\n')
	lista = ['1']
	while '}' not in linea:
		linea2 = linea.split(' ')
		porcentaje= str((contador/float(lineas))*100).split('.')
		print "Completado: "+porcentaje[0]+"%\r",
		if lista.count(linea2[0])!=1:
			lista.append(linea2[0])
		if lista.count(linea2[2])!=1:
			lista.append(linea2[2])
		contador+=1
		#print linea2
		#print lista
		#raw_input("pausa")
		linea = archi.readline().rstrip(';\n')
	print "Numero total de estados: "+ str(len(lista))
	archi.close()
	return lista

def leerCaminos(estados):
	print "leyendo los caminos"
	contadorCaminos = 0
	contador = 1
	archi=open("results\grafico.dot",'r')
	lineas = len(open('results\grafico.dot').readlines())
	linea = archi.readline()
	linea = archi.readline().rstrip(';\n')
	lista = []
	while '}' not in linea:
		porcentaje= str((contador/float(lineas))*100).split('.')
		print "Completado: "+porcentaje[0]+"%\r",
		linea2 = linea.split(' ')
		lista.append(linea2[0])
		linea = archi.readline().rstrip(';\n')
		contador+=1
	for estado in estados:
		if lista.count(estado)>1:
			contadorCaminos+=1
	print "Numero total de caminos: " + str(contadorCaminos)
	archi.close()

def nuevaKripke(estados):
	archi=open("results\kripke.txt",'r')
	f = open("tmp\kripke2.txt","w")
	contador=1
	print "Generando nueva kripke"
	lineas = len(open('results\kripke.txt').readlines())
	linea = archi.readline().rstrip('\n')
	while 'Nil})' not in linea:
		porcentaje= str((contador/float(lineas))*100).split('.')
		print "Completado: "+porcentaje[0]+"%\r",
		for estado in estados:
			if (str(estado)+' @->') in linea or ('(Pair(('+estado+':nat),') in linea:
				f.write(linea+'\n')
		linea = archi.readline().rstrip('\n')
		contador+=1
	f.write(linea)
	f.close()
	archi.close()
	os.remove("tmp\kripke2.txt")

def nuevoDot(estados):
	archi = open("results\grafico.dot","r")
	f = open("tmp\grafico2.dot","w")
	print "generando nuevo dot"
	contador=1
	linea = archi.readline().strip('\n')
	lineas = len(open('results\grafico.dot').readlines())
	f.write(linea+'\n')
	while '}' not in linea:
		porcentaje= str((contador/float(lineas))*100).split('.')
		print "Completado: "+porcentaje[0]+"%\r",
		for estado in estados:
			for estado2 in estados:
				if (str(estado)+' -> '+str(estado2)+';')== linea:
					f.write(linea+'\n')
		linea = archi.readline().rstrip('\n')
		contador+=1
	f.write(linea)
	f.close()
	archi.close()
	os.remove("tmp\grafico2.dot")
	
def nodosIniciales(programa):
	inicial = []
	for funcion in programa:
		for linea in funcion:
			if linea.opcode != "":
				#if linea.num >= 315 and linea.num <365:
				#	print str(linea.num) + "-"+ linea.opcode
				#	for saltazo in linea.jumpList:
				#		print "--> "+ str(saltazo)
				nodo = linea.num
				encontrado = 0
				if "Retn" in linea.opcode:
					encontrado = 1;
				else:
					for funcion2 in programa:
						for linea2 in funcion2:
							if linea2.opcode != "":
								if linea2.num <= nodo:
									for salto in linea2.jumpList:
										#if nodo == 336:
										#	print str(linea2.num)+"-"+linea2.opcode+":"+str(salto)+"->"+str(nodo)
										if salto == nodo:
										#	if nodo == 336:
										#		print "Entro"
											encontrado = 1
											break
								if encontrado == 1:
									break	
						if encontrado == 1:
							break
				if encontrado == 0:
					inicial.append(nodo)
	f = open("tmp\inicial.dat","w")
	for nodo in inicial:
		f.write(str(nodo)+"\n")
	f.close()
				
					

def asmToPoctpl(ruta, opciones):
	program = []
	print "Leyendo la funcion de inicio"
	program.append(leerStartAsm(ruta))
	print "Analizando las funciones"
	program = leerFuncionesAsm(program, ruta)
	print "Analizando llamadas a funciones"
	program = llamadasFun(program)
	#print "Buscando saltos muertos"
	#program = buscarSaltosMuertos(program)
	print "Convirtiendo a POCTPL"
	program = ensPoctpl(program)
	print "Cambiando los Loc"
	program = cambiarLoc(program)
	print "Creando estructura kripke"
	crearKripke(program)
	# Esto sería para hacer la grafica para la interfaz
	#print "Creando datos para la grafica"
	#crearGrafica(program)
	#if (opciones==1):
	#print "Creando grafico"
	#puntoDot(program)
	print "Buscando nodos iniciales"
	nodosIniciales(program)
	return
