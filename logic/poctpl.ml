type bool =
| True
| False

(*type nat =
| O
| S of nat*)
type nat = int

type 'a option =
| Some of 'a
| None

type ('a, 'b) prod =
| Pair of 'a * 'b

type 'a list =
| Nil
| Cons of 'a * 'a list

(** val app : 'a1 list -> 'a1 list -> 'a1 list **)

let rec app l m =
  match l with
  | Nil -> m
  | Cons (a, l1) -> Cons (a, (app l1 m))

type comparison =
| Eq
| Lt
| Gt

(**let natFirst p =
	match p with
		| Nil -> 0
		| Cons ((a, b), l) -> b**)

(** val nat_compare : nat -> nat -> comparison **)

(*let rec nat_compare n m =
  match n with
  | O ->
    (match m with
     | O -> Eq
     | S n0 -> Lt)
  | S n' ->
    (match m with
     | O -> Gt
     | S m' -> nat_compare n' m')*)
let nat_compare n m = 
    match n,m with
        | x,y when x>y -> Gt
        | x,y when x=y -> Eq
        | _,_ -> Lt

type register32 =
| Eax
| Eax_plus of nat
| Eax_minus of nat
| Ebx
| Ebx_plus of nat
| Ebx_minus of nat
| Ecx
| Ecx_plus of nat
| Ecx_minus of nat
| Edx
| Edx_plus of nat
| Edx_minus of nat
| Esi
| Esi_plus of nat
| Esi_minus of nat
| Edi
| Edi_plus of nat
| Edi_minus of nat
| Ebp
| Ebp_plus of nat
| Ebp_minus of nat
| Esp
| Esp_plus of nat
| Esp_minus of nat
| Eip
| Cs
| Ds
| Ss
| Es
| Fs
| Gs
| Register32

type register16 =
| Ax
| Bx
| Cx
| Dx
| SI
| DI
| SP
| SP_plus of nat
| SP_minus of nat
| BP
| BP_plus of nat
| BP_minus of nat
| IP
| Register16

type register8 =
| AH
| AL
| BH
| BL
| CH
| CL
| DH
| DL
| Register8

type mem =
| Mem

type hold =
| RegAdd32 of register32 * mem
| AddAdd32 of mem * mem
| AddReg32 of mem * register32
| RegReg32 of register32 * register32
| RegVal32 of register32 * nat
| AddVal32 of mem * nat
| Regi32 of register32
| Addr32 of mem
| RegAdd16 of register16 * mem
| AddAdd16 of mem * mem
| AddReg16 of mem * register16
| RegReg16 of register16 * register16
| RegVal16 of register16 * nat
| AddVal16 of mem * nat
| Regi16 of register16
| Addr16 of mem
| RegAdd8 of register8 * mem
| AddAdd8 of mem * mem
| AddReg8 of mem * register8
| RegReg8 of register8 * register8
| RegVal8 of register8 * nat
| AddVal8 of mem * nat
| Regi8 of register8
| Addr8 of mem

type function0 =
| Strcpy
| StrcpyA
| StrcpyW
| Wcscpy
| Coq__tcscpy
| Coq__mbscpy
| StrCpy
| StrCpyA
| StrCpyW
| Lstrcpy
| LstrcpyA
| LstrcpyW
| Coq__tccpy
| Coq__mbccpy
| Coq__ftcscpy
| Strncpy
| Wcsncpy
| Coq__tcsncpy
| Coq__mbsncpy
| Coq__mbsnbcpy
| StrCpyN
| StrCpyNA
| StrCpyNW
| StrNCpy
| StrcpynA
| StrNCpyA
| StrNCpyW
| Lstrcpyn
| LstrcpynA
| LstrcpynW
| Strcat
| StrcatA
| StrcatW
| Wcscat
| Coq__tcscat
| Coq__mbscat
| StrCat
| StrCatA
| StrCatW
| Lstrcat
| LstrcatA
| LstrcatW
| StrCatBuff
| StrCatBuffA
| StrCatBuffW
| StrCatChainW
| Coq__tccat
| Coq__mbccat
| Coq__ftcscat
| Strncat
| Wcsncat
| Coq__tcsncat
| Coq__mbsncat
| Coq__mbsnbcat
| StrCatN
| StrCatNA
| StrCatNW
| StrNCat
| StrNCatA
| StrNCatW
| Lstrncat
| LstrcatnA
| LstrcatnW
| Lstrcatn
| SprintfW
| SprintfA
| Wsprintf
| WsprintfW
| WsprintfA
| Sprintf
| Swprintf
| Coq__stprintf
| Wvsprintf
| WvsprintfA
| WvsprintfW
| Vsprintf
| Coq__vstprintf
| Vswprintf
| Coq__fstrncpy
| Coq__fstrncat
| Gets
| Coq__getts
| Coq__gettws
| IsBadWritePtr
| IsBadHugeWritePtr
| IsBadReadPtr
| IsBadHugeReadPtr
| IsBadCodePtr
| IsBadStringPtr
| Memcpy
| RtlCopyMemory
| CopyMemory
| Wmemcpy
| Generic
| Readfile
| Mem

type opcode =
| Mov of hold
| Push of hold
| Call of function0
| Pop of hold
| Cmp of hold
| Jz of hold
| Lea of hold
| Test of hold
| Jmp of hold
| Add of hold
| Jnz of hold
| Retn
| Xor of hold
| And of hold
| Loc of nat

(** val natCompB : nat -> nat -> bool **)

let natCompB a b =
  match nat_compare a b with
  | Eq -> True
  | _ -> False

(** val regCompare32 : register32 -> register32 -> bool **)

let regCompare32 r1 r2 =
  match r1 with
  | Eax ->
    (match r2 with
     | x when x = Eax -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Eax_plus x ->
    (match r2 with
     | Eax_plus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Eax_minus x ->
    (match r2 with
     | Eax_minus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Ebx ->
    (match r2 with
     | x when x = Ebx -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Ebx_plus x ->
    (match r2 with
     | Ebx_plus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Ebx_minus x ->
    (match r2 with
     | Ebx_minus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Ecx ->
    (match r2 with
     | x when x = Ecx -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Ecx_plus x ->
    (match r2 with
     | Ecx_plus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Ecx_minus x ->
    (match r2 with
     | Ecx_minus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Edx ->
    (match r2 with
     | x when x = Edx -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Edx_plus x ->
    (match r2 with
     | Edx_plus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Edx_minus x ->
    (match r2 with
     | Edx_minus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Esi ->
    (match r2 with
     | x when x = Esi -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Esi_plus x ->
    (match r2 with
     | Esi_plus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Esi_minus x ->
    (match r2 with
     | Esi_minus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Edi ->
    (match r2 with
     | x when x = Edi -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Edi_plus x ->
    (match r2 with
     | Edi_plus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Edi_minus x ->
    (match r2 with
     | Edi_minus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Ebp -> 
    (match r2 with
     | x when x = Ebp -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Ebp_plus x ->
    (match r2 with
     | Ebp_plus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Ebp_minus x ->
    (match r2 with
     | Ebp_minus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Esp ->
    (match r2 with
     | x when x = Esp -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Esp_plus x ->
    (match r2 with
     | Esp_plus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Esp_minus x ->
    (match r2 with
     | Esp_minus y when  y = x -> True
	 | Register32 -> True
     | _ -> False)
  | Eip ->
    (match r2 with
     | x when x = Eip -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Cs ->
    (match r2 with
     | x when x = Cs -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Ds ->
    (match r2 with
     | x when x = Ds -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Ss ->
    (match r2 with
     | x when x = Ss -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Es ->
    (match r2 with
     | x when x = Es -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Fs ->
    (match r2 with
     | x when x = Fs -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Gs ->
    (match r2 with
     | x when x = Gs -> True
	 | x when x = Register32 -> True
     | _ -> False)
  | Register32 -> True

(** val regCompare32Aux : register32 -> register32 -> bool **)

let regCompare32Aux r1 r2 =
  regCompare32 r1 r2

(** val regCompare16 : register16 -> register16 -> bool **)

let regCompare16 r1 r2 =
  match r1 with
  | Ax ->
    (match r2 with
     | x when x = Ax -> True
	 | x when x = Register16 -> True
     | _ -> False)
  | Bx ->
    (match r2 with
     | x when x = Bx -> True
	 | x when x = Register16 -> True
     | _ -> False)
  | Cx ->
    (match r2 with
     | x when x = Cx -> True
	 | x when x = Register16 -> True
     | _ -> False)
  | Dx ->
    (match r2 with
     | x when x = Dx -> True
	 | x when x = Register16 -> True
     | _ -> False)
  | SI ->
    (match r2 with
     | x when x = SI -> True
	 | x when x = Register16 -> True
     | _ -> False)
  | DI ->
    (match r2 with
     | x when x = DI -> True
	 | x when x = Register16 -> True
     | _ -> False)
  | SP ->
    (match r2 with
     | x when x = SP -> True
	 | x when x = Register16 -> True
     | _ -> False)
  | SP_plus x ->
    (match r2 with
	 | SP_plus y when  y = x -> True
	 | Register16 -> True
     | _ -> False)
  | SP_minus x ->
    (match r2 with
     | SP_minus y when  y = x -> True
	 | Register16 -> True
     | _ -> False)
  | BP ->
    (match r2 with
     | x when x = BP -> True
	 | x when x = Register16 -> True
     | _ -> False)
  | BP_plus x ->
    (match r2 with
     | BP_plus y when  y = x -> True
	 | Register16 -> True
     | _ -> False)
  | BP_minus x ->
    (match r2 with
     | BP_plus y when  y = x -> True
	 | Register16 -> True
     | _ -> False)
  | IP ->
    (match r2 with
     | x when x = IP -> True
	 | x when x = Register16 -> True
     | _ -> False)
  | register16 -> True

(** val regCompare16Aux : register16 -> register16 -> bool **)

let regCompare16Aux r1 r2 =
  regCompare16 r1 r2

(** val regCompare8 : register8 -> register8 -> bool **)

let regCompare8 r1 r2 =
  match r1 with
  | AH ->
    (match r2 with
     | x when x = AH -> True
	 | x when x = Register8 -> True
     | _ -> False)
  | AL ->
    (match r2 with
     | x when x = AL -> True
	 | x when x = Register8 -> True
     | _ -> False)
  | BH ->
    (match r2 with
     | x when x = BH -> True
	 | x when x = Register8 -> True
     | _ -> False)
  | BL ->
    (match r2 with
     | x when x = BL -> True
	 | x when x = Register8 -> True
     | _ -> False)
  | CH ->
    (match r2 with
     | x when x = CH -> True
	 | x when x = Register8 -> True
     | _ -> False)
  | CL ->
    (match r2 with
     | x when x = CL -> True
	 | x when x = Register8 -> True
     | _ -> False)
  | DH ->
    (match r2 with
     | x when x = DH -> True
	 | x when x = Register8 -> True
     | _ -> False)
  | DL ->
    (match r2 with
     | x when x = DL -> True
	 | x when x = Register8 -> True
     | _ -> False)
  | Register8 -> True

(** val regCompare8Aux : register8 -> register8 -> bool **)

let regCompare8Aux r1 r2 =
  regCompare8 r1 r2

(** val containCompare : hold -> hold -> bool **)

let containCompare c1 c2 = 
  match c1 with
  | RegAdd32 (a, b) ->
    (match c2 with
     | RegAdd32 (x, y) -> regCompare32 a x
     | _ -> False)
  | AddAdd32 (a, b) ->
    (match c2 with
     | AddAdd32 (x, y) -> True
     | _ -> False)
  | AddReg32 (a, b) ->
    (match c2 with
     | AddReg32 (x, y) -> regCompare32 b y
     | _ -> False)
  | RegReg32 (a, b) ->
    (match c2 with
     | RegReg32 (x, y) ->
       (match regCompare32 a x with
        | True -> regCompare32Aux b y
        | False -> False)
     | _ -> False)
  | RegVal32 (a, b) ->
    (match c2 with
     | RegVal32 (x, y) ->
       (match regCompare32 a x with
        | True -> natCompB b y
        | False -> False)
     | _ -> False)
  | AddVal32 (a, b) ->
    (match c2 with
     | AddVal32 (x, y) -> natCompB b y
     | _ -> False)
  | Regi32 a ->
    (match c2 with
     | Regi32 x -> regCompare32 a x
     | _ -> False)
  | Addr32 a ->
    (match c2 with
     | Addr32 x -> True
     | _ -> False)
  | RegAdd16 (a, b) ->
    (match c2 with
     | RegAdd16 (x, y) -> regCompare16 a x
     | _ -> False)
  | AddAdd16 (a, b) ->
    (match c2 with
     | AddAdd16 (x, y) -> True
     | _ -> False)
  | AddReg16 (a, b) ->
    (match c2 with
     | AddReg16 (x, y) -> regCompare16 b y
     | _ -> False)
  | RegReg16 (a, b) ->
    (match c2 with
     | RegReg16 (x, y) ->
       (match regCompare16 a x with
        | True -> regCompare16Aux b y
        | False -> False)
     | _ -> False)
  | RegVal16 (a, b) ->
    (match c2 with
     | RegVal16 (x, y) ->
       (match regCompare16 a x with
        | True -> natCompB b y
        | False -> False)
     | _ -> False)
  | AddVal16 (a, b) ->
    (match c2 with
     | AddVal16 (x, y) -> natCompB b y
     | _ -> False)
  | Regi16 a ->
    (match c2 with
     | Regi16 x -> regCompare16 a x
     | _ -> False)
  | Addr16 a ->
    (match c2 with
     | Addr16 x -> True
     | _ -> False)
  | RegAdd8 (a, b) ->
    (match c2 with
     | RegAdd8 (x, y) -> regCompare8 a x
     | _ -> False)
  | AddAdd8 (a, b) ->
    (match c2 with
     | AddAdd8 (x, y) -> True
     | _ -> False)
  | AddReg8 (a, b) ->
    (match c2 with
     | AddReg8 (x, y) -> regCompare8 b y
     | _ -> False)
  | RegReg8 (a, b) ->
    (match c2 with
     | RegReg8 (x, y) ->
       (match regCompare8 a x with
        | True -> regCompare8Aux b y
        | False -> False)
     | _ -> False)
  | RegVal8 (a, b) ->
    (match c2 with
     | RegVal8 (x, y) ->
       (match regCompare8 a x with
        | True -> natCompB b y
        | False -> False)
     | _ -> False)
  | AddVal8 (a, b) ->
    (match c2 with
     | AddVal8 (x, y) -> natCompB b y
     | _ -> False)
  | Regi8 a ->
    (match c2 with
     | Regi8 x -> regCompare8 a x
     | _ -> False)
  | Addr8 a ->
    (match c2 with
     | Addr8 x -> True
     | _ -> False)

(** val funCompare : function0 -> function0 -> bool **)

let funCompare a b =
  match a with
  | Strcpy -> 
    (match b with
     | Strcpy -> True
     | _ -> False)
  | StrcpyA ->
    (match b with
     | StrcpyA -> True
     | _ -> False)
  | StrcpyW ->
    (match b with
     | StrcpyW -> True
     | _ -> False)
  | Wcscpy ->
    (match b with
     | Wcscpy -> True
     | _ -> False)
  | Coq__tcscpy ->
    (match b with
     | Coq__tcscpy -> True
     | _ -> False)
  | Coq__mbscpy ->
    (match b with
     | Coq__mbscpy -> True
     | _ -> False)
  | StrCpy ->
    (match b with
     | StrCpy -> True
     | _ -> False)
  | StrCpyA ->
    (match b with
     | StrCpyA -> True
     | _ -> False)
  | StrCpyW ->
    (match b with
     | StrCpyW -> True
     | _ -> False)
  | Lstrcpy ->
    (match b with
     | Lstrcpy -> True
     | _ -> False)
  | LstrcpyA ->
    (match b with
     | LstrcpyA -> True
     | _ -> False)
  | LstrcpyW ->
    (match b with
     | LstrcpyW -> True
     | _ -> False)
  | Coq__tccpy ->
    (match b with
     | Coq__tccpy -> True
     | _ -> False)
  | Coq__mbccpy ->
    (match b with
     | Coq__mbccpy -> True
     | _ -> False)
  | Coq__ftcscpy ->
    (match b with
     | Coq__ftcscpy -> True
     | _ -> False)
  | Strncpy ->
    (match b with
     | Strncpy -> True
     | _ -> False)
  | Wcsncpy ->
    (match b with
     | Wcsncpy -> True
     | _ -> False)
  | Coq__tcsncpy ->
    (match b with
     | Coq__tcsncpy -> True
     | _ -> False)
  | Coq__mbsncpy ->
    (match b with
     | Coq__mbsncpy -> True
     | _ -> False)
  | Coq__mbsnbcpy ->
    (match b with
     | Coq__mbsnbcpy -> True
     | _ -> False)
  | StrCpyN ->
    (match b with
     | StrCpyN -> True
     | _ -> False)
  | StrCpyNA ->
    (match b with
     | StrCpyNA -> True
     | _ -> False)
  | StrCpyNW ->
    (match b with
     | StrCpyNW -> True
     | _ -> False)
  | StrNCpy ->
    (match b with
     | StrNCpy -> True
     | _ -> False)
  | StrcpynA ->
    (match b with
     | StrcpynA -> True
     | _ -> False)
  | StrNCpyA ->
    (match b with
     | StrNCpyA -> True
     | _ -> False)
  | StrNCpyW ->
    (match b with
     | StrNCpyW -> True
     | _ -> False)
  | Lstrcpyn ->
    (match b with
     | Lstrcpyn -> True
     | _ -> False)
  | LstrcpynA ->
    (match b with
     | LstrcpynA -> True
     | _ -> False)
  | LstrcpynW ->
    (match b with
     | LstrcpynW -> True
     | _ -> False)
  | Strcat ->
    (match b with
     | Strcat -> True
     | _ -> False)
  | StrcatA ->
    (match b with
     | StrcatA -> True
     | _ -> False)
  | StrcatW ->
    (match b with
     | StrcatW -> True
     | _ -> False)
  | Wcscat ->
    (match b with
     | Wcscat -> True
     | _ -> False)
  | Coq__tcscat ->
    (match b with
     | Coq__tcscat -> True
     | _ -> False)
  | Coq__mbscat ->
    (match b with
     | Coq__mbscat -> True
     | _ -> False)
  | StrCat ->
    (match b with
     | StrCat -> True
     | _ -> False)
  | StrCatA ->
    (match b with
     | StrCatA -> True
     | _ -> False)
  | StrCatW ->
    (match b with
     | StrCatW -> True
     | _ -> False)
  | Lstrcat ->
    (match b with
     | Lstrcat -> True
     | _ -> False)
  | LstrcatA ->
    (match b with
     | LstrcatA -> True
     | _ -> False)
  | LstrcatW ->
    (match b with
     | LstrcatW -> True
     | _ -> False)
  | StrCatBuff ->
    (match b with
     | StrCatBuff -> True
     | _ -> False)
  | StrCatBuffA ->
    (match b with
     | StrCatBuffA -> True
     | _ -> False)
  | StrCatBuffW ->
    (match b with
     | StrCatBuffW -> True
     | _ -> False)
  | StrCatChainW ->
    (match b with
     | StrCatChainW -> True
     | _ -> False)
  | Coq__tccat ->
    (match b with
     | Coq__tccat -> True
     | _ -> False)
  | Coq__mbccat ->
    (match b with
     | Coq__mbccat -> True
     | _ -> False)
  | Coq__ftcscat ->
    (match b with
     | Coq__ftcscat -> True
     | _ -> False)
  | Strncat ->
    (match b with
     | Strncat -> True
     | _ -> False)
  | Wcsncat ->
    (match b with
     | Wcsncat -> True
     | _ -> False)
  | Coq__tcsncat ->
    (match b with
     | Coq__tcsncat -> True
     | _ -> False)
  | Coq__mbsncat ->
    (match b with
     | Coq__mbsncat -> True
     | _ -> False)
  | Coq__mbsnbcat ->
    (match b with
     | Coq__mbsnbcat -> True
     | _ -> False)
  | StrCatN ->
    (match b with
     | StrCatN -> True
     | _ -> False)
  | StrCatNA ->
    (match b with
     | StrCatNA -> True
     | _ -> False)
  | StrCatNW ->
    (match b with
     | StrCatNW -> True
     | _ -> False)
  | StrNCat ->
    (match b with
     | StrNCat -> True
     | _ -> False)
  | StrNCatA ->
    (match b with
     | StrNCatA -> True
     | _ -> False)
  | StrNCatW ->
    (match b with
     | StrNCatW -> True
     | _ -> False)
  | Lstrncat ->
    (match b with
     | Lstrncat -> True
     | _ -> False)
  | LstrcatnA ->
    (match b with
     | LstrcatnA -> True
     | _ -> False)
  | LstrcatnW ->
    (match b with
     | LstrcatnW -> True
     | _ -> False)
  | Lstrcatn ->
    (match b with
     | Lstrcatn -> True
     | _ -> False)
  | SprintfW ->
    (match b with
     | SprintfW -> True
     | _ -> False)
  | SprintfA ->
    (match b with
     | SprintfA -> True
     | _ -> False)
  | Wsprintf ->
    (match b with
     | Wsprintf -> True
     | _ -> False)
  | WsprintfW ->
    (match b with
     | WsprintfW -> True
     | _ -> False)
  | WsprintfA ->
    (match b with
     | WsprintfA -> True
     | _ -> False)
  | Sprintf ->
    (match b with
     | Sprintf -> True
     | _ -> False)
  | Swprintf ->
    (match b with
     | Swprintf -> True
     | _ -> False)
  | Coq__stprintf ->
    (match b with
     | Coq__stprintf -> True
     | _ -> False)
  | Wvsprintf ->
    (match b with
     | Wvsprintf -> True
     | _ -> False)
  | WvsprintfA ->
    (match b with
     | WvsprintfA -> True
     | _ -> False)
  | WvsprintfW ->
    (match b with
     | WvsprintfW -> True
     | _ -> False)
  | Vsprintf ->
    (match b with
     | Vsprintf -> True
     | _ -> False)
  | Coq__vstprintf ->
    (match b with
     | Coq__vstprintf -> True
     | _ -> False)
  | Vswprintf ->
    (match b with
     | Vswprintf -> True
     | _ -> False)
  | Coq__fstrncpy ->
    (match b with
     | Coq__fstrncpy -> True
     | _ -> False)
  | Coq__fstrncat ->
    (match b with
     | Coq__fstrncat -> True
     | _ -> False)
  | Gets ->
    (match b with
     | Gets -> True
     | _ -> False)
  | Coq__getts ->
    (match b with
     | Coq__getts -> True
     | _ -> False)
  | Coq__gettws ->
    (match b with
     | Coq__gettws -> True
     | _ -> False)
  | IsBadWritePtr ->
    (match b with
     | IsBadWritePtr -> True
     | _ -> False)
  | IsBadHugeWritePtr ->
    (match b with
     | IsBadHugeWritePtr -> True
     | _ -> False)
  | IsBadReadPtr ->
    (match b with
     | IsBadReadPtr -> True
     | _ -> False)
  | IsBadHugeReadPtr ->
    (match b with
     | IsBadHugeReadPtr -> True
     | _ -> False)
  | IsBadCodePtr ->
    (match b with
     | IsBadCodePtr -> True
     | _ -> False)
  | IsBadStringPtr ->
    (match b with
     | IsBadStringPtr -> True
     | _ -> False)
  | Memcpy ->
    (match b with
     | Memcpy -> True
     | _ -> False)
  | RtlCopyMemory ->
    (match b with
     | RtlCopyMemory -> True
     | _ -> False)
  | CopyMemory ->
    (match b with
     | CopyMemory -> True
     | _ -> False)
  | Wmemcpy ->
    (match b with
     | Wmemcpy -> True
     | _ -> False)
  | Generic ->
    (match b with
     | Generic -> True
     | _ ->  False)
  | Readfile ->
    (match b with
     | Readfile -> True
     | _ -> False)
  | Mem ->
    (match b with
     | Mem -> True
     | _ -> False)

(** val opCompare : opcode -> opcode -> bool **)

let opCompare a b = 
  match a with
  | Mov x ->
    (match b with
     | Mov y -> containCompare x y
     | _ -> False)
  | Push x ->
    (match b with
     | Push y -> containCompare x y
     | _ -> False)
  | Call x ->
    (match b with
     | Call y -> funCompare x y
     | Mov _ -> False
     | Push y -> False
     | Pop y ->  False
     | Cmp y ->  False
     | Jz y ->  False
     | Lea y -> False
     | Test y -> False
     | Jmp y -> False
     | Add y -> False
     | Jnz y -> False
     | Retn -> False
     | Xor y -> False
     | And y -> False
	 | Loc y -> False)
  | Pop op ->
    (match b with
     | Pop op' -> containCompare op op'
     | _ -> False)
  | Cmp op ->
    (match b with
     | Cmp op' -> containCompare op op'
     | _ -> False)
  | Jz op ->
    (match b with
     | Jz op' -> containCompare op op'
     | _ -> False)
  | Lea op ->
    (match b with
     | Lea op' -> containCompare op op'
     | _ -> False)
  | Test op ->
    (match b with
     | Test op' -> containCompare op op'
     | _ -> False)
  | Jmp op ->
    (match b with
     | Jmp op' -> containCompare op op'
     | _ -> False)
  | Add op ->
    (match b with
     | Add op' -> containCompare op op'
     | _ -> False)
  | Jnz op ->
    (match b with
     | Jnz op' -> containCompare op op'
     | _ -> False)
  | Retn ->
    (match b with
     | Retn -> True
     | _ -> False)
  | Xor op ->
    (match b with
     | Xor op' -> containCompare op op'
     | _ -> False)
  | And op ->
    (match b with
     | And op' -> containCompare op op'
     | _ -> False)
  | Loc op ->
    (match b with
     | Loc op' -> natCompB op op'
     | _ -> False)
 
	 
(** val getOpcode : nat -> (nat, opcode) prod list -> opcode option **)

let rec getOpcode n = function
| Nil -> None
| Cons (p, l') ->
  let Pair (a, b) = p in
  (match natCompB n a with
   | True -> Some b
   | False -> getOpcode n l')

type 'a path =
| PNil
| PCons of opcode * 'a path

type path0 = nat list

type nodeTree = path0 list

type cTPLPath = opcode path

type cTPLTree = cTPLPath list

type kripke = { s_List : nat list; r_List : (nat, nat) prod list;
                l_List : (nat, opcode) prod list }

(** val r_List : kripke -> (nat, nat) prod list **)

let r_List x = x.r_List

(** val l_List : kripke -> (nat, opcode) prod list **)

let l_List x = x.l_List

(** Hacer que imprima algo por pantalla **)

let regShow8 r1 =
  match r1 with
  | AH -> print_string "ah "
  | AL -> print_string "al "
  | BH -> print_string "bh "
  | BL -> print_string "bl "
  | CH -> print_string "ch "
  | CL -> print_string "cl "
  | DH -> print_string "dh "
  | DL -> print_string "dl "
  | Register8 -> print_string "register32 "

let regShow16 r1 =
  match r1 with
  | Ax -> print_string "ax "
  | Bx -> print_string "bx "
  | Cx -> print_string "cx "
  | Dx -> print_string "dx "
  | SI -> print_string "si "
  | DI -> print_string "di "
  | SP -> print_string "sp "
  | SP_plus x -> print_string "sp+"
  | SP_minus x -> print_string "sp-"
  | BP -> print_string "bp "
  | BP_plus x -> print_string "bp+"
  | BP_minus x -> print_string "bp-"
  | IP -> print_string "ip "
  | Register16 -> print_string "register16 "

let regShow32 r1 =
  match r1 with
  | Eax -> print_string "eax "
  | Eax_plus x -> print_string "eax+"
  | Eax_minus x -> print_string "eax-"
  | Ebx -> print_string "ebx "
  | Ebx_plus x -> print_string "ebx+"
  | Ebx_minus x -> print_string "ebx-"
  | Ecx -> print_string "ecx "
  | Ecx_plus x -> print_string "ecx+"
  | Ecx_minus x -> print_string "ecx-"
  | Edx -> print_string "edx "
  | Edx_plus x -> print_string "edx+"
  | Edx_minus x ->  print_string "edx-"
  | Esi -> print_string "esi "
  | Esi_plus x -> print_string "esi+"
  | Esi_minus x -> print_string "esi-"
  | Edi -> print_string "edi "
  | Edi_plus x -> print_string "edi+"
  | Edi_minus x -> print_string "edi-"
  | Ebp -> print_string "ebp "
  | Ebp_plus x -> print_string "ebp+"
  | Ebp_minus x -> print_string "ebp-"
  | Esp -> print_string "esp "
  | Esp_plus x -> print_string "esp+"
  | Esp_minus x -> print_string "esp-"
  | Eip -> print_string "eip "
  | Cs -> print_string "cs "
  | Ds -> print_string "ds "
  | Ss -> print_string "ss "
  | Es -> print_string "es "
  | Fs -> print_string "fs "
  | Gs -> print_string "gs "
  | Register32 -> print_string "Register32 "

let funShow a =
  match a with
  | Strcpy -> print_string "Strcpy -> "
  | StrcpyA -> print_string "StrcpyA -> "
  | StrcpyW -> print_string "StrcpyW -> "
  | Wcscpy -> print_string "Wcscpy -> "
  | Coq__tcscpy -> print_string "Coq__tcscpy -> "
  | Coq__mbscpy -> print_string "Coq__mbscpy -> "
  | StrCpy -> print_string "StrCpy -> "
  | StrCpyA -> print_string "StrCpyA -> "
  | StrCpyW -> print_string "StrCpyW -> "
  | Lstrcpy -> print_string "Lstrcpy -> "
  | LstrcpyA -> print_string "LstrcpyA -> "
  | LstrcpyW -> print_string "LstrcpyW -> "
  | Coq__tccpy -> print_string "Coq__tccpy -> "
  | Coq__mbccpy -> print_string "Coq__mbccpy -> "
  | Coq__ftcscpy -> print_string "Coq__ftcscpy -> "
  | Strncpy -> print_string "Strncpy -> "
  | Wcsncpy -> print_string "Wcsncpy -> "
  | Coq__tcsncpy -> print_string "Coq__tcsncpy -> "
  | Coq__mbsncpy -> print_string "Coq__mbsncpy -> "
  | Coq__mbsnbcpy -> print_string "Coq__mbsnbcpy -> "
  | StrCpyN -> print_string "StrCpyN -> "
  | StrCpyNA -> print_string "StrCpyNA -> "
  | StrCpyNW -> print_string "StrCpyNW -> "
  | StrNCpy -> print_string "StrNCpy -> "
  | StrcpynA -> print_string "StrcpynA -> "
  | StrNCpyA -> print_string "StrNCpyA -> "
  | StrNCpyW -> print_string "StrNCpyW -> "
  | Lstrcpyn -> print_string "Lstrcpyn -> "
  | LstrcpynA -> print_string "LstrcpynA -> "
  | LstrcpynW -> print_string "LstrcpynW -> "
  | Strcat -> print_string "Strcat -> "
  | StrcatA -> print_string "StrcatA -> "
  | StrcatW -> print_string "StrcatW -> "
  | Wcscat -> print_string "Wcscat -> "
  | Coq__tcscat -> print_string "Coq__tcscat -> "
  | Coq__mbscat -> print_string "Coq__mbscat -> "
  | StrCat -> print_string "StrCat -> "
  | StrCatA -> print_string "StrCatA -> "
  | StrCatW -> print_string "StrCatW -> "
  | Lstrcat -> print_string "Lstrcat -> "
  | LstrcatA -> print_string "LstrcatA -> "
  | LstrcatW -> print_string "LstrcatW -> "
  | StrCatBuff -> print_string "StrCatBuff -> "
  | StrCatBuffA -> print_string "StrCatBuffA -> "
  | StrCatBuffW -> print_string "StrCatBuffW -> "
  | StrCatChainW -> print_string "StrCatChainW -> "
  | Coq__tccat -> print_string "Coq__tccat -> "
  | Coq__mbccat -> print_string "Coq__mbccat -> "
  | Coq__ftcscat -> print_string "Coq__ftcscat -> "
  | Strncat -> print_string "Strncat -> "
  | Wcsncat -> print_string "Wcsncat -> "
  | Coq__tcsncat -> print_string "Coq__tcsncat -> "
  | Coq__mbsncat -> print_string "Coq__mbsncat -> "
  | Coq__mbsnbcat -> print_string "Coq__mbsnbcat -> "
  | StrCatN -> print_string "StrCatN -> "
  | StrCatNA -> print_string "StrCatNA -> "
  | StrCatNW -> print_string "StrCatNW -> "
  | StrNCat -> print_string "StrNCat -> "
  | StrNCatA -> print_string "StrNCatA -> "
  | StrNCatW -> print_string "StrNCatW -> "
  | Lstrncat -> print_string "Lstrncat -> "
  | LstrcatnA -> print_string "LstrcatnA -> "
  | LstrcatnW -> print_string "LstrcatnW -> "
  | Lstrcatn -> print_string "Lstrcatn -> "
  | SprintfW -> print_string "SprintfW -> "
  | SprintfA -> print_string "SprintfA -> "
  | Wsprintf -> print_string "Wsprintf -> "
  | WsprintfW -> print_string "WsprintfW -> "
  | WsprintfA -> print_string "WsprintfA -> "
  | Sprintf -> print_string "Sprintf -> "
  | Swprintf -> print_string "Swprintf -> "
  | Coq__stprintf -> print_string "Coq__stprintf -> "
  | Wvsprintf -> print_string "Wvsprintf -> "
  | WvsprintfA -> print_string "WvsprintfA -> "
  | WvsprintfW -> print_string "WvsprintfW -> "
  | Vsprintf -> print_string "Vsprintf -> "
  | Coq__vstprintf -> print_string "Coq__vstprintf -> "
  | Vswprintf -> print_string "Vswprintf -> "
  | Coq__fstrncpy -> print_string "Coq__fstrncpy -> "
  | Coq__fstrncat -> print_string "Coq__fstrncat -> "
  | Gets -> print_string "Gets -> "
  | Coq__getts -> print_string "Coq__getts -> "
  | Coq__gettws -> print_string "Coq__gettws -> "
  | IsBadWritePtr -> print_string "IsBadWritePtr -> "
  | IsBadHugeWritePtr -> print_string "IsBadHugeWritePtr -> "
  | IsBadReadPtr -> print_string "IsBadReadPtr -> "
  | IsBadHugeReadPtr -> print_string "IsBadHugeReadPtr -> "
  | IsBadCodePtr -> print_string "IsBadCodePtr -> "
  | IsBadStringPtr -> print_string "IsBadStringPtr -> "
  | Memcpy -> print_string "Memcpy -> "
  | RtlCopyMemory -> print_string "RtlCopyMemory -> "
  | CopyMemory -> print_string "CopyMemory -> "
  | Wmemcpy -> print_string "Wmemcpy -> "
  | Generic -> print_string "Generic -> "
  | Readfile -> print_string "Readfile ->"
  | Mem -> print_string "Mem ->"

let containShow c =
  match c with
  | RegAdd32 (a, b) -> print_string "RegAdd32 "; regShow32 a; print_string "mem -> "
  | AddAdd32 (a, b) -> print_string "AddAdd32 "; print_string "mem mem -> "
  | AddReg32 (a, b) -> print_string "AddReg32 "; print_string "mem "; regShow32 b; print_string "-> "
  | RegReg32 (a, b) -> print_string "RegReg32 "; regShow32 a; regShow32 b; print_string "-> "
  | RegVal32 (a, b) -> print_string "RegVal32 "; regShow32 a; print_string "mem -> "
  | AddVal32 (a, b) -> print_string "AddVal32 "; print_string "mem mem -> "
  | Regi32 a -> print_string "Regi32 "; regShow32 a; print_string "-> "
  | Addr32 a -> print_string "Addr32 "; print_string "mem -> "
  | RegAdd16 (a, b) -> print_string "RegAdd16 "
  | AddAdd16 (a, b) -> print_string "AddAdd16 "
  | AddReg16 (a, b) -> print_string "AddAdd16 "
  | RegReg16 (a, b) -> print_string "RegReg16 "
  | RegVal16 (a, b) -> print_string "RegVal16 "
  | AddVal16 (a, b) -> print_string "AddVal16 "
  | Regi16 a -> print_string "Regi16 "
  | Addr16 a -> print_string "Addr16 "
  | RegAdd8 (a, b) -> print_string "RegAdd8 "
  | AddAdd8 (a, b) -> print_string "AddAdd8 "
  | AddReg8 (a, b) -> print_string "AddReg8 "
  | RegReg8 (a, b) -> print_string "RegReg8 "
  | RegVal8 (a, b) -> print_string "RegVal8 "
  | AddVal8 (a, b) -> print_string "AddVal8 "
  | Regi8 a -> print_string "Regi8 "
  | Addr8 a -> print_string "Addr8 "

let opShow a =
  match a with
  | Mov x -> print_string "Mov " ; containShow x
  | Push x -> print_string "Push " ; containShow x
  | Call x -> print_string "Call " ; funShow x
  | Pop x -> print_string "Pop " ; containShow x
  | Cmp x -> print_string "Cmp " ; containShow x
  | Jz x -> print_string "Jz " ; containShow x
  | Lea x -> print_string "Lea " ; containShow x
  | Test x -> print_string "Test " ; containShow x
  | Jmp x -> print_string "Jmp " ; containShow x
  | Add x -> print_string "Add " ; containShow x
  | Jnz x -> print_string "Jnz " ; containShow x
  | Retn -> print_string "Retn -> " 
  | Xor x -> print_string "Xor " ; containShow x
  | And x -> print_string "And " ; containShow x
  | Loc x -> print_string "Loc " ; print_int x

let rec print_list = function
     PNil -> print_string "[]\n"
   | PCons (a, l) -> opShow a; print_list l

let rec print_list2 = function
     Nil -> print_string ""
   | Cons (a, l) -> print_string "\n----------------\n"; print_list a; (print_list2 l);;
   
let rec print_list3 = function
     Nil -> print_string "[]\n";
   | Cons (a, l) -> opShow a; (print_list3 l);;
   
let rec print_list4 = function
     Nil -> print_string ""
   | Cons (a, l) -> print_string "camino 4\n"; print_list3 l; (print_list4 l);;
   
let rec pasarAPath = function
	 Nil -> PNil
	| Cons (a, l) -> PCons (a, (pasarAPath l));;
	
let rec incCamino l =
    Cons (l, Nil);;
	
   
let rec print_list5 = function
     Nil -> print_string "\nAnalisis Finalizado\n"
   | Cons (a, l) -> print_string "camino\n"; print_list4 l; (print_list5 l);;

(** val searchNode : nat -> (nat, nat) prod list -> path0 **)

let rec searchNode x = function
| Nil -> Nil
| Cons (p, m) -> 
  let Pair (a, b) = p in
  (match natCompB a x with
   | True -> Cons (b, (searchNode x m))
   | False -> searchNode x m)

(** val addToAll : nat -> nodeTree -> nodeTree **)

let rec addToAll x = function
| Nil -> Nil
| Cons (n, m) -> Cons ((Cons (x, n)), (addToAll x m))

(** val searchPath : (nat, nat) prod list -> nat -> nat -> nodeTree **)

let rec searchPath lNodos nI nF =
  let allNodes =
    let rec allNodes = function
    | Nil -> Nil
    | Cons (nInt, l) ->
      (match lNodos with
       | Nil -> Nil
       | Cons (p, l') ->
         app (addToAll nI (searchPath l' nInt nF)) (allNodes l))
    in allNodes
  in
  (match natCompB nI nF with
   | True -> Cons ((Cons (nF, Nil)), Nil)
   | False -> allNodes (searchNode nI lNodos))

(** val getListOpcode : path0 -> (nat, opcode) prod list -> cTPLPath **)

let rec getListOpcode l3 l4 =
  match l3 with
  | Nil -> PNil
  | Cons (n, l'') ->
    (match getOpcode n l4 with
     | Some x -> PCons (x, (getListOpcode l'' l4))
     | None -> getListOpcode l'' l4)

(** val natToOpcode : nodeTree -> (nat, opcode) prod list -> cTPLTree **)

let rec natToOpcode l1 l2 =
  match l1 with
  | Nil -> Nil
  | Cons (a, l') -> Cons ((getListOpcode a l2), (natToOpcode l' l2))

(** val opcodeList : (nat, opcode) prod list -> opcode list **)

let rec opcodeList = function
| Nil -> Nil
| Cons (p, l') -> let Pair (n, o) = p in Cons (o, (opcodeList l'))

(** val getRet : (nat, opcode) prod list -> nat **)

let rec getRet = function
| Nil -> 0
| Cons (p, l') ->
  let Pair (n, o) = p in
  (match o with
   | Retn -> print_int n; n
   | _ -> getRet l')

(** val getRet : (nat, opcode) prod list -> nat list **)
   
let rec getRetList = function
| Nil -> Nil
| Cons (p, l') -> 
  let Pair (n, o) = p in
  (match o with
   | Retn -> Cons (n, getRetList l')
   | _ -> getRetList l')


type opStateFormula =
| TRUE
| Op of opcode
| NoS of opStateFormula
| AndS of opStateFormula * opStateFormula
| OrS of opStateFormula * opStateFormula
| ImpS of opStateFormula * opStateFormula
| CoimpS of opStateFormula * opStateFormula

type opType =
| Id of opStateFormula
| NoC of opType
| OrC of opType * opType
| AndC of opType * opType
| ImpC of opType * opType
| CoimpC of opType * opType
| EX of opType
| EX2 of opStateFormula
| EU of opType * opType
| EU2 of opStateFormula * opStateFormula
| EG of opType
| EG2 of opStateFormula
| EF of opType
| EF2 of opStateFormula
| EFX of opType * opType
| EFX2 of opStateFormula * opStateFormula
| AG of opType
| AG2 of opStateFormula
| AX of opType
| AX2 of opStateFormula
| AF of opType
| AF2 of opStateFormula
| AU of opType * opType
| AU2 of opStateFormula * opStateFormula
| AFX of opType * opType
| AFX2 of opStateFormula * opStateFormula

(** val pathCompare : cTPLPath -> cTPLPath -> bool **)

let rec pathCompare l1 l2 =
  match l1 with
  | PNil ->
    (match l2 with
     | PNil -> True
     | PCons (o, p) -> False)
  | PCons (a, l1') ->
    (match l2 with
     | PNil -> False
     | PCons (b, l2') ->
       (match opCompare a b with
        | True -> pathCompare l1' l2'
        | False -> False))

(** val cutPath : cTPLPath -> cTPLPath -> cTPLPath **)

let rec cutPath l1 l2 =
  match l1 with
  | PNil -> PNil
  | PCons (a, l1') ->
    (match l2 with
     | PNil -> PNil
     | PCons (b, l2') ->
       (match opCompare a b with
        | True -> PNil
        | False -> PCons (b, (cutPath l1 l2'))))

(** val pathHoldFirst : cTPLPath -> cTPLPath -> bool **)

let rec pathHoldFirst l1 l2 =
  match l1 with
  | PNil -> True
  | PCons (a, l1') ->
    (match l2 with
     | PNil -> False
     | PCons (b, l2') ->
       (match opCompare a b with
        | True -> pathHoldFirst l1' l2'
        | False -> False))

(** val pathHold : cTPLPath -> cTPLPath -> bool **)

let rec pathHold l1 l2 =
  match l1 with
  | PNil -> True
  | PCons (a, l1') ->
    (match l2 with
     | PNil -> False
     | PCons (b, l2') ->
       (match opCompare a b with
        | True -> pathHoldFirst l1 l2
        | False -> pathHold l1 l2'))

(** val cutFirst : cTPLPath -> cTPLPath **)

let cutFirst = function
| PNil -> PNil
| PCons (a, l') -> l'

(** val joinTree : cTPLTree -> cTPLTree -> cTPLTree **)

let rec joinTree l1 l2 =
  match l1 with
  | Nil -> l2
  | Cons (a, l1') -> Cons (a, (joinTree l1' l2))
  
let rec joinTreePath l1 l2 =
  match l1 with
    | PNil -> l2
	| PCons (a, l1') -> Cons (l1, l2)

(** val opInList : opcode -> opcode list -> opcode list **)

let rec opInList o = function
| Nil -> Nil
| Cons (a, l') ->
  (match opCompare o a with
   | True -> Cons (a, Nil)
   | False -> opInList o l')

(** val neg' : cTPLPath -> cTPLTree -> bool **)

let rec neg' l1 = function
| Nil -> False
| Cons (b, l') ->
  (match pathCompare l1 b with
   | True -> True
   | False -> neg' l1 l')

(** val neg : cTPLTree -> cTPLTree -> cTPLTree **)

let rec neg l1 l2 =
  match l1 with
  | Nil -> Nil
  | Cons (a, l1') ->
    (match l2 with
     | Nil -> l1
     | Cons (b, l2') ->
       (match neg' a l2 with
        | True -> neg l1' l2
        | False -> Cons (a, (neg l1' l2))))

(** val interL2 : cTPLTree -> cTPLPath -> cTPLPath option **)

let rec interL2 l1 path1 =
  match l1 with
  | Nil -> None
  | Cons (a, l1') ->
    (match pathCompare a path1 with
     | True -> Some path1
     | False -> interL2 l1' path1)

(** val inter : cTPLTree -> cTPLTree -> cTPLTree **)

let rec inter l1 l2 =
  match l1 with
  | Nil -> Nil
  | Cons (a, l1') ->
    (match interL2 l2 a with
     | Some b -> Cons (b, (inter l1' l2))
     | None -> inter l1' l2)

(** val pathCompareXC : cTPLPath -> cTPLPath -> bool **)

let rec pathCompareXC l1 l2 =
  match l1 with
  | PNil -> True
  | PCons (a, l1') ->
    (match l2 with
     | PNil -> False
     | PCons (b, l2') ->
       (match opCompare a b with
        | True -> pathCompareXC l1' l2'
        | False -> False))

(** val auxOperationXC : cTPLTree -> cTPLPath -> cTPLTree **)

let rec auxOperationXC l1 l2 =
  match l1 with
  | Nil -> Nil
  | Cons (a, l1') ->
    (match pathCompareXC a l2 with
     | True -> Cons (l2, (auxOperationXC l1' l2))
     | False -> auxOperationXC l1' l2)

(** val operationXC : cTPLTree -> cTPLTree -> cTPLTree **)

let rec operationXC l1 = function
| Nil -> Nil
| Cons (a, l2') ->
  (match auxOperationXC l1 (cutFirst a) with
   | Nil -> operationXC l1 l2'
   | Cons (c, l0) -> joinTree (Cons (a, Nil)) (operationXC l1 l2'))

(** val operationUC2 : cTPLPath -> cTPLPath -> cTPLPath **)

let rec operationUC2 l1 l2 = match l2 with
| PNil -> PNil
| PCons (a, l) ->
  (match pathHold l1 l2 with
   | True -> cutPath l1 l2
   | False -> operationUC2 l1 l)

(** val operationUC1 : cTPLPath -> cTPLPath -> cTPLPath -> cTPLPath **)

let rec operationUC1 lfirst lU2 l =
  match lfirst with
  | PNil -> PNil
  | PCons (a, lf') ->
    (match pathHold (PCons (a, lf')) lU2 with
     | True -> l
     | False -> operationUC1 lf' lU2 l)

(** val operationUCPath : cTPLPath -> cTPLPath -> cTPLPath -> cTPLPath **)

let operationUCPath lf ll l =
  operationUC1 lf (operationUC2 ll l) l

(** val operationUCPathJuas2 : cTPLPath -> cTPLPath -> cTPLPath **)

let rec operationUCPathJuas2 l1 l2 = match l2 with
| PNil -> PNil
| PCons (a, l) ->
  (match pathHoldFirst l1 l2 with
   | True -> l2
   | False -> operationUCPathJuas2 l1 l)

(** val operationUCPathJuas :
    cTPLPath -> cTPLPath -> cTPLPath -> cTPLPath **)

let operationUCPathJuas l1 l2 l =
  operationUCPathJuas2 l2 (operationUCPath l1 l2 l)

(** val operationUC'' : cTPLPath -> cTPLPath -> cTPLTree -> cTPLTree **)

let rec operationUC'' lf ll = function
| Nil -> Nil
| Cons (a, l') ->
  (match operationUCPathJuas lf ll a with
   | PNil -> operationUC'' lf ll l'
   | PCons (o, p) -> Cons ((PCons (o, p)), (operationUC'' lf ll l')))

(** val operationUC' : cTPLPath -> cTPLTree -> cTPLTree -> cTPLTree **)

let rec operationUC' lf ll l =
  match ll with
  | Nil -> Nil
  | Cons (a, ll') ->
    (match operationUC'' lf a l with
     | Nil -> operationUC' lf ll' l
     | Cons (b, lf'') -> Cons (b, (operationUC' lf ll' l)))

(** val operationUC : cTPLTree -> cTPLTree -> cTPLTree -> cTPLTree **)

let rec operationUC lf ll l =
  match lf with
  | Nil -> Nil
  | Cons (a, lf') ->
    (match operationUC' a ll l with
     | Nil -> operationUC lf' ll l
     | Cons (b, lf'') -> Cons (b, (operationUC lf' ll l)))

(** val operationGC' : cTPLPath -> cTPLPath -> cTPLPath -> bool **)

let rec operationGC' l1 l2 lt =
  match l1 with
  | PNil ->
    (match l2 with
     | PNil -> True
     | PCons (b, l2') ->
       (match lt with
        | PNil -> False
        | PCons (x, lt') ->
          (match opCompare b x with
           | True -> operationGC' lt' l2' lt
           | False -> False)))
  | PCons (a, l1') ->
    (match l2 with
     | PNil -> False
     | PCons (b, l2') ->
       (match opCompare a b with
        | True -> operationGC' l1' l2' lt
        | False -> False))

(** val auxOperationGC : cTPLTree -> cTPLPath -> bool **)

let rec auxOperationGC l1 l2 =
  match l1 with
  | Nil -> False
  | Cons (a, l1') ->
    (match operationGC' a l2 a with
     | True -> True
     | False -> auxOperationGC l1' l2)

(** val operationGC : cTPLTree -> cTPLTree -> cTPLTree **)

let rec operationGC l1 = function
| Nil -> Nil
| Cons (a, l2') ->
  (match auxOperationGC l1 a with
   | True -> Cons (a, (operationGC l1 l2'))
   | False -> operationGC l1 l2')

(** val operationFXC2 : cTPLPath -> cTPLPath -> bool **)

let rec operationFXC2 l1 l2 =
  match l1 with
  | PNil -> True
  | PCons (a, l1') ->
    (match l2 with
     | PNil -> False
     | PCons (b, l2') ->
       (match opCompare a b with
        | True -> operationFXC2 l1' l2'
        | False -> False))

(** val operationFXC1 :
    cTPLPath -> cTPLPath -> cTPLPath -> cTPLPath -> cTPLPath **)

let rec operationFXC1 lf lFX2 l l2 =
  match lf with
  | PNil ->
    (match l with
     | PNil -> PNil
     | PCons (o, p) ->
       (match operationFXC2 lFX2 l with
        | True -> l
        | False -> PNil))
  | PCons (a, l1') ->
    (match l with
     | PNil -> PNil
     | PCons (b, l') ->
       (match opCompare a b with
        | True -> operationFXC1 l1' lFX2 l' l2
        | False -> operationFXC1 lf lFX2 l' l2))

(** val operationFXC'' : cTPLPath -> cTPLPath -> cTPLTree -> cTPLTree **)

let rec operationFXC'' lf ll = function
| Nil -> Nil
| Cons (a, l') ->
  (match operationFXC1 lf ll a a with
   | PNil -> operationFXC'' lf ll l'
   | PCons (o, p) -> Cons ((PCons (o, p)), (operationFXC'' lf ll l')))

(** val operationFXC' : cTPLPath -> cTPLTree -> cTPLTree -> cTPLTree **)

let rec operationFXC' lf ll l =
  match ll with
  | Nil -> Nil
  | Cons (a, ll') ->
    (match operationFXC'' lf a l with
     | Nil -> operationFXC' lf ll' l
     | Cons (b, lf'') -> Cons (b, (operationFXC' lf ll' l)))

(** val operationFXC : cTPLTree -> cTPLTree -> cTPLTree -> cTPLTree **)

let rec operationFXC lf ll l =
  match lf with
  | Nil -> Nil
  | Cons (a, lf') ->
    (match operationFXC' a ll l with
     | Nil -> operationFXC lf' ll l
     | Cons (b, lf'') -> Cons (b, (operationFXC lf' ll l)))

(** val inTree' : opcode -> cTPLPath -> cTPLPath option **)

let inTree' o l =
match l with
| PNil -> None
| PCons (a, l') ->
  (match opCompare a o with
   | True -> Some l
   | False -> None)

(** val inTree2 : opcode -> cTPLTree -> cTPLTree **)

let rec inTree2 o = function
| Nil -> Nil
| Cons (a, l') ->
  (match inTree' o a with
   | Some a' -> Cons (a', (inTree2 o l'))
   | None -> inTree2 o l')

(** val inTree : opcode list -> cTPLTree -> cTPLTree **)

let rec inTree l1 l2 =
  match l1 with
  | Nil -> Nil
  | Cons (a, l1') -> 
    (match inTree2 a l2 with
     | Nil -> inTree l1' l2
     | Cons (c, l) -> joinTree l2 (inTree l1' l2))

(** val negState' : opcode -> opcode list -> bool **)

let rec negState' o = function
| Nil -> False
| Cons (a, l') ->
  (match opCompare o a with
   | True -> True
   | False -> negState' o l')

(** val negState : opcode list -> opcode list -> opcode list **)

let rec negState l1 l2 =
  match l1 with
  | Nil -> Nil
  | Cons (a, l1') ->
    (match negState' a l2 with
     | True -> negState l1' l2
     | False -> Cons (a, (negState l1' l2)))

(** val interState' : opcode -> opcode list -> bool **)

let rec interState' o = function
| Nil -> False
| Cons (a, l') ->
  (match opCompare o a with
   | True -> True
   | False -> interState' o l')

(** val interState : opcode list -> opcode list -> opcode list **)

let rec interState l1 l2 =
  match l1 with
  | Nil -> Nil
  | Cons (a, l1') ->
    (match interState' a l2 with
     | True -> Cons (a, (interState l1' l2))
     | False -> interState l1' l2)

(** val second : cTPLPath -> opcode option **)

let second = function
| PNil -> None
| PCons (a, l') ->
  (match l' with
   | PNil -> None
   | PCons (b, l'') -> Some b)

(** val auxOperationX : opcode list -> cTPLPath -> cTPLTree **)

let rec auxOperationX l1 l2 =
  match l1 with
  | Nil -> Nil
  | Cons (a, l1') ->
    (match second l2 with
     | Some b ->
       (match opCompare a b with
        | True -> Cons ((cutFirst l2), (auxOperationX l1' l2))
        | False -> auxOperationX l1' l2)
     | None -> auxOperationX l1' l2)

(** val operationX : opcode list -> cTPLTree -> cTPLTree **)

let rec operationX l1 = function
| Nil -> Nil
| Cons (a, l2') ->
  (match auxOperationX l1 a with
   | Nil -> operationX l1 l2'
   | Cons (c, l0) -> joinTree (Cons (c, l0)) (operationX l1 l2'))

(** val operationU1_3 : opcode -> opcode -> cTPLPath -> bool **)

let rec operationU1_4 o l2 =
	match l2 with
	| PNil -> False
	| PCons (a, l2') ->
		(match opCompare o a with
			| False -> operationU1_4 o l2'
			| True -> True)

(** val operationU1_3 : opcode -> opcode -> cTPLPath -> bool **)
			
let rec operationU1_3 o o2 l2 =
match l2 with
| PNil -> False
| PCons (a, l2') -> 
  (match opCompare o2 a with
   | False -> operationU1_3 o o2 l2'
   | True -> 
     (match operationU1_4 o l2' with
      | True -> True
      | False -> False))

(** val operationU1_2 : opcode list -> opcode -> cTPLPath -> bool **)

let rec operationU1_2 l1 o2 l2 =
  match l1 with
  | Nil -> False
  | Cons (a, l1') -> 
    (match operationU1_3 a o2 l2 with
     | True -> True
     | False -> operationU1_2 l1' o2 l2)

(** val operationU1_1 : opcode list -> opcode list -> cTPLPath -> bool **)

let rec operationU1_1 l1 l2 l3 =
  match l2 with
  | Nil -> False
  | Cons (a, l2') ->
    (match operationU1_2 l1 a l3 with
     | True -> True
     | False -> operationU1_1 l1 l2' l3)

(** val operationU1 : opcode list -> opcode list -> cTPLTree -> cTPLTree **)

let rec operationU1 l1 l2 = function
| Nil -> Nil
| Cons (a, l3') ->
  (match operationU1_1 l1 l2 a with
   | True -> Cons (a, (operationU1 l1 l2 l3'))
   | False -> operationU1 l1 l2 l3')

(** val operationU2'' : opcode -> cTPLPath -> cTPLPath **)

let rec operationU2'' o l = match l with
| PNil -> PNil
| PCons (a, l') -> 
  (match opCompare o a with
   | True -> l
   | False -> operationU2'' o l')

(** val operationU2' : opcode list -> cTPLPath -> cTPLPath **)

let rec operationU2' l1 l2 =
  match l1 with 
  | Nil -> PNil
  | Cons (a, l1') -> 
    (match operationU2'' a l2 with
     | PNil -> operationU2' l1' l2
     | PCons (o, p) -> PCons (o, p))

(** val operationU2 : opcode list -> cTPLTree -> cTPLTree **)

let rec operationU2 o = function
| Nil -> Nil
| Cons (a, l2') -> 
  (match operationU2' o a with
   | PNil -> operationU2 o l2'
   | PCons (o0, p) -> Cons ((PCons (o0, p)), (operationU2 o l2')))

(** val operationU : opcode list -> opcode list -> cTPLTree -> cTPLTree **)

let operationU l1 l2 l3 =
  (operationU1 l1 l2 l3)

(** val operationG' : opcode -> cTPLPath -> bool **)

let rec operationG' o = function
| PNil -> True
| PCons (a, l') ->
  (match opCompare o a with
   | True -> operationG' o l'
   | False -> False)

(** val auxOperationG : opcode -> cTPLTree -> cTPLTree **)

let rec auxOperationG o = function
| Nil -> Nil
| Cons (a, l1') ->
  (match operationG' o a with
   | True -> Cons (a, (auxOperationG o l1'))
   | False -> auxOperationG o l1')

(** val operationG : opcode list -> cTPLTree -> cTPLTree **)

let rec operationG l1 l2 =
  match l1 with
  | Nil -> Nil
  | Cons (a, l1') ->
    (match auxOperationG a l2 with
     | Nil -> operationG l1' l2
     | Cons (c, l0) -> joinTree (Cons (c, l0)) (operationG l1' l2))

(** val operationFX''' : opcode -> opcode -> cTPLPath -> cTPLPath **)

let rec operationFX''' o1 o2 l = match l with
| PNil -> PNil
| PCons (a, l') ->
  (match l' with
   | PNil -> PNil
   | PCons (o, p) ->
     (match opCompare a o1 with
      | True ->
        (match l' with
         | PNil -> PNil
         | PCons (b, l'') ->
           (match opCompare b o2 with
            | True -> l
            | False -> operationFX''' o1 o2 l'))
      | False -> operationFX''' o1 o2 l'))

(** val operationFX'' : opcode -> opcode list -> cTPLPath -> cTPLTree **)

let rec operationFX'' o l2 l =
  match l2 with
  | Nil -> Nil
  | Cons (a, l2') ->
    (match operationFX''' o a l with
     | PNil -> operationFX'' o l2' l
     | PCons (o0, p) -> Cons ((PCons (o0, p)), (operationFX'' o l2' l)))

(** val operationFX' : opcode list -> opcode list -> cTPLPath -> cTPLTree **)

let rec operationFX' l1 l2 l =
  match l1 with
  | Nil -> Nil
  | Cons (a, l1') ->
    (match operationFX'' a l2 l with
     | Nil -> operationFX' l1' l2 l
     | Cons (c, l0) -> joinTree (Cons (c, l0)) (operationFX' l1' l2 l))

(** val operationFX : opcode list -> opcode list -> cTPLTree -> cTPLTree **)

let rec operationFX l1 l2 = function
| Nil -> Nil
| Cons (a, l') ->
  (match operationFX' l1 l2 a with
   | Nil -> operationFX l1 l2 l'
   | Cons (c, l0) -> joinTree (Cons (c, l0)) (operationFX l1 l2 l'))

(** val satState : opStateFormula -> opcode list -> opcode list **)

let rec satState o l =
  match o with
  | TRUE -> l
  | Op a -> opInList a l
  | NoS a -> negState l (satState a l)
  | AndS (a, b) -> interState (satState a l) (satState b l)
  | OrS (a, b) ->
    negState l
      (interState (negState l (satState a l)) (negState l (satState b l)))
  | ImpS (a, b) ->
    negState l (interState (satState a l) (negState l (satState b l)))
  | CoimpS (a, b) ->
    interState
      (negState l (interState (satState a l) (negState l (satState b l))))
      (negState l (interState (satState b l) (negState l (satState a l))))

(** val lo2CTPLTree : opcode list -> cTPLTree **)

let rec lo2CTPLTree = function
| Nil -> Nil
| Cons (a, l) -> Cons ((PCons (a, PNil)), (lo2CTPLTree l))

(** val sat : opType -> opcode list -> cTPLTree -> cTPLTree **)

let rec sat o lo l =
  match o with
  | Id a -> inTree (satState a lo) l
  | NoC a -> neg l (sat a lo l)
  | OrC (a, b) -> neg l (inter (neg l (sat a lo l)) (neg l (sat b lo l)))
  | AndC (a, b) -> inter (sat a lo l) (sat b lo l)
  | ImpC (a, b) -> neg l (inter (sat a lo l) (neg l (sat b lo l)))
  | CoimpC (a, b) ->
    inter (neg l (inter (sat a lo l) (neg l (sat b lo l))))
      (neg l (inter (sat b lo l) (neg l (sat a lo l))))
  | EX a -> operationXC (sat a lo l) l
  | EX2 a -> operationX (satState a lo) l
  | EU (a, b) -> operationUC (sat a lo l) (sat b lo l) l
  | EU2 (a, b) -> operationU (satState a lo) (satState b lo) l
  | EG a -> operationGC (sat a lo l) l
  | EG2 a -> operationG (satState a lo) l
  | EF a -> operationUC (lo2CTPLTree lo) (sat a lo l) l
  | EF2 a -> operationU (satState TRUE lo) (satState a lo) l
  | EFX (a, b) -> operationFXC (sat a lo l) (sat b lo l) l
  | EFX2 (a, b) -> operationFX (satState a lo) (satState b lo) l
  | AG a -> neg l (operationUC (lo2CTPLTree lo) (neg l (sat a lo l)) l)
  | AG2 a -> 
    neg l (operationU (satState TRUE lo) (negState lo (satState a lo)) l)
  | AX a -> neg l (operationXC (neg l (sat a lo l)) l)
  | AX2 a -> neg l (operationX (negState lo (satState a lo)) l)
  | AF a -> neg l (operationGC (neg l (sat a lo l)) l)
  | AF2 a -> neg l (operationG (negState lo (satState a lo)) l)
  | AU (a, b) ->
    inter
      (neg l
        (operationUC (neg l (sat b lo l))
          (inter (neg l (sat a lo l)) (neg l (sat b lo l))) l))
      (neg l (operationGC (neg l (sat b lo l)) l))
  | AU2 (a, b) ->
    inter
      (neg l
        (operationU (negState (satState b lo) lo)
          (interState (negState (satState a lo) lo)
            (negState (satState b lo) lo)) l))
      (neg l (operationG (negState (satState b lo) lo) l))
  | AFX (a, b) ->
    inter
      (neg l
        (operationFXC (neg l (sat b lo l))
          (inter (neg l (sat a lo l)) (neg l (sat b lo l))) l))
      (neg l (operationGC (neg l (sat b lo l)) l))
  | AFX2 (a, b) ->
    inter
      (neg l
        (operationFX (negState (satState b lo) lo)
          (interState (negState (satState a lo) lo)
            (negState (satState b lo) lo)) l))
      (neg l (operationG (negState (satState b lo) lo) l))

(** val sonIgualesCaminos_2 : cTPLPath -> cTPLPath -> bool **)

let rec sonIgualesCaminos_2 l1 l2 =
  match l1 with
  | PNil ->
    (match l2 with
     | PNil -> True
     | PCons (o, p) -> False)
  | PCons (a, l1') ->
    (match l2 with
     | PNil -> False
     | PCons (b, l2') ->
       (match opCompare a b with
        | True -> sonIgualesCaminos_2 l1' l2'
        | False -> False))

(** val sonIgualesCaminos : cTPLPath -> cTPLPath -> bool **)

let rec sonIgualesCaminos l1 l2 =
  match l1 with
  | PNil -> False
  | PCons (a, l1') ->
    (match l2 with
     | PNil -> False
     | PCons (b, l2') ->
       (match sonIgualesCaminos_2 l1 l2 with
        | True -> True
        | False -> sonIgualesCaminos l1 l2'))

(** val buscarCaminosCompletos_2 : cTPLPath -> cTPLTree -> cTPLPath **)

let rec buscarCaminosCompletos_2 l1 = function
| Nil -> PNil
| Cons (a, l2') ->
  (match sonIgualesCaminos l1 a with
   | True -> a
   | False -> buscarCaminosCompletos_2 l1 l2')

(** val buscarCaminosCompletos : cTPLTree -> cTPLTree -> cTPLTree **)

let rec buscarCaminosCompletos l1 l2 =
  match l1 with
  | Nil -> Nil
  | Cons (a, l1') ->
    (match buscarCaminosCompletos_2 a l2 with
     | PNil -> buscarCaminosCompletos l1' l2
     | PCons (o, p) -> Cons ((PCons (o, p)), (buscarCaminosCompletos l1' l2)))

(** val comprobarIguales_2 : cTPLPath -> cTPLTree -> bool **)

let rec comprobarIguales_2 l1 = function
| Nil -> False
| Cons (a, l2') ->
  (match pathCompare l1 a with
   | True -> True
   | False -> comprobarIguales_2 l1 l2')

(** val comprobarIguales : cTPLTree -> cTPLTree **)

let rec comprobarIguales =  function
| Nil -> Nil
| Cons (a, l') ->
  (match comprobarIguales_2 a l' with
   | True -> comprobarIguales l'
   | False -> Cons (a, (comprobarIguales l')))

(** val start3 : opType -> opcode list -> cTPLTree -> cTPLTree **)

let start3 o lo l =
  comprobarIguales (buscarCaminosCompletos (sat o lo l) l)
  
  
let rec prueba x nodos = 
    match nodos with
		| Nil -> Nil
		| Cons (p, m) -> 
			let Pair (a, b) = p in
				(match natCompB a x with
					| True -> Cons (b, (prueba x m))
					| False -> prueba x m)

(** val start : opType -> kripke -> cTPLTree **)
(**
let rec startFalso o k l =
  match l with
	| Nil ->  Nil
	| Cons (p, m) -> 
	match (start3 o (opcodeList k.l_List)
    (natToOpcode (searchPath k.r_List (1) p) k.l_List)) with
		| Nil -> startFalso o k m
		| a -> joinTree a (startFalso o k m)


let start o k =
	startFalso o k (getRetList k.l_List)
	
   
	
	
let (@->) el l = Cons (el,l);;
let (@>) el l = PCons (el,l);;
*)
(** let z = (start (Id TRUE) ({s_List=(1 @;r_List=(1,1);l_List=(1,Retn)});;
let z = (start (Id TRUE) ({s_List=(1 @-> Nil);r_List=(Pair((1:nat),(1:nat)))@->Nil;l_List=(Pair((1:nat),Retn))@->Nil}));;**)

(**let z = (start (Id TRUE) 
let z = (start (EFX2((Op (Mov (RegReg32 (Ebp, Esp)))), (Op (Cmp (RegAdd32 (Eax, Mem)))))) 
let z = (start (EF2 (Op (Mov (RegReg32 (Edi, Edi))))) 

let file = "example.dat"

type reader = {read_next : unit -> string option};;

let make_reader file_name =
  let in_channel = open_in file_name in
  let closed = ref false in
  let read_next_line = fun () ->
    if !closed then
      None
    else
      try
        Some (Scanf.fscanf in_channel "%[^\r\n]\n" (fun x -> x))
      with
        End_of_file ->
          let _ = close_in_noerr in_channel in
          let _ = closed := true in
          None in
  {read_next = read_next_line};;
  
let r = make_reader "example.dat";;
let next_line = r.read_next ();;**)

(**let z = (start (EF2 (Op (Mov (RegReg32 (Ebp, Esp)))))
let z = (start next_line
({s_List=1 @-> 
2 @-> 
3 @-> 
4 @-> 
Nil; r_List=(Pair((1:nat), (2:nat))) @-> 
(Pair((2:nat), (3:nat))) @-> 
(Pair((3:nat), (4:nat))) @-> 
Nil; 
l_List=(Pair((1:nat), Mov (RegReg32 (Ebp, Esp)))) @-> 
(Pair((2:nat), Mov (RegReg32 (Ebp, Esp)))) @-> 
(Pair((3:nat), Mov (RegReg32 (Ebp, Esp)))) @-> 
(Pair((4:nat), Retn)) @-> **)
