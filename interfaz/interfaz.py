#!/usr/bin/env python

"""
A test of some simple animation

this is very old-style code: don't imitate it!

"""

import wx
import re
import numpy as N

## import local version:
import sys

# ver = 'local'
ver = 'installed'

if ver == 'installed':  ## import the installed version
    from wx.lib.floatcanvas import NavCanvas, Resources
    from wx.lib.floatcanvas import FloatCanvas as FC
    from wx.lib.floatcanvas.Utilities import BBox

    print "using installed version:", wx.lib.floatcanvas.__version__
# elif ver == 'local':
# ## import a local version
#    import sys
#    sys.path.append("..")
#    from floatcanvas import NavCanvas
#    from floatcanvas import FloatCanvas

#FC = FloatCanvas

class choosePaths(wx.Dialog):
    def __init__(self, *args, **kw):
        super(choosePaths, self).__init__(*args, **kw)

        self.InitUI()
        self.SetSize((250, 150))
        self.SetTitle("Choose paths")

    def InitUI(self):
        pnl = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)

        sb = wx.StaticBox(pnl, label="paths")
        sbs = wx.StaticBoxSizer(sb, orient=wx.VERTICAL)

        self.radio1 = wx.RadioButton(pnl, label='Exists in one path', style=wx.RB_GROUP)
        self.radio2 = wx.RadioButton(pnl, label='Exists in all paths')
        sbs.Add(self.radio1)
        sbs.Add(self.radio2)

        pnl.SetSizer(sbs)

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        okButton = wx.Button(self, label='Ok')
        closeButton = wx.Button(self, label='Close')
        hbox2.Add(okButton)
        hbox2.Add(closeButton, flag=wx.LEFT, border=5)

        vbox.Add(pnl, proportion=1, flag=wx.ALL | wx.EXPAND, border=5)
        vbox.Add(hbox2, flag=wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, border=10)

        self.SetSizer(vbox)

        okButton.Bind(wx.EVT_BUTTON, self.OnOk)
        closeButton.Bind(wx.EVT_BUTTON, self.OnClose)

    def OnClose(self, e):
        self.Destroy()

    def OnOk(self, e):
        self.EndModal(wx.ID_OK)

    def GetValue(self):
        # devolvemos uno si es exists
        if self.radio1.GetValue():
            return 1
        # o devolvemos dos si es forall
        elif self.radio2.GetValue():
            return 2


class choosePathsUntil(wx.Dialog):
    def __init__(self, *args, **kw):
        super(choosePathsUntil, self).__init__(*args, **kw)

        self.InitUI()
        self.SetSize((250, 150))
        self.SetTitle("Choose paths")

    def InitUI(self):
        pnl = wx.Panel(self)
        vbox = wx.BoxSizer(wx.VERTICAL)

        sb = wx.StaticBox(pnl, label="paths")
        sbs = wx.StaticBoxSizer(sb, orient=wx.VERTICAL)

        self.radio1 = wx.RadioButton(pnl, label='Exists in one path (first part)', style=wx.RB_GROUP)
        self.radio2 = wx.RadioButton(pnl, label='Exists in all paths (second part)')
        self.radio3 = wx.RadioButton(pnl, label='U (second part)')
        sbs.Add(self.radio1)
        sbs.Add(self.radio2)
        sbs.Add(self.radio3)

        pnl.SetSizer(sbs)

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        okButton = wx.Button(self, label='Ok')
        closeButton = wx.Button(self, label='Close')
        hbox2.Add(okButton)
        hbox2.Add(closeButton, flag=wx.LEFT, border=5)

        vbox.Add(pnl, proportion=1, flag=wx.ALL | wx.EXPAND, border=5)
        vbox.Add(hbox2, flag=wx.ALIGN_CENTER | wx.TOP | wx.BOTTOM, border=10)

        self.SetSizer(vbox)

        okButton.Bind(wx.EVT_BUTTON, self.OnOk)
        closeButton.Bind(wx.EVT_BUTTON, self.OnClose)

    def OnClose(self, e):
        self.Destroy()

    def OnOk(self, e):
        self.EndModal(wx.ID_OK)

    def GetValue(self):
        # devolvemos uno si es exists
        if self.radio1.GetValue():
            return 1
        # o devolvemos dos si es forall
        elif self.radio2.GetValue():
            return 2
        elif self.radio3.GetValue():
            return 3


## here we create some new mixins:
## fixme: These really belong in floatcanvas package -- but I kind of want to clean it up some first

class MovingObjectMixin:
    """
    Methods required for a Moving object

    """

    def GetOutlinePoints(self):
        """
        Returns a set of points with which to draw the outline when moving the
        object.

        Points are a NX2 array of (x,y) points in World coordinates.


        """
        BB = self.BoundingBox
        OutlinePoints = N.array(( (BB[0, 0], BB[0, 1]),
                                  (BB[0, 0], BB[1, 1]),
                                  (BB[1, 0], BB[1, 1]),
                                  (BB[1, 0], BB[0, 1]),
        )
        )

        return OutlinePoints


class ConnectorObjectMixin:
    """
    Mixin class for DrawObjects that can be connected with lines

    Note that this version only works for Objects that have an "XY" attribute:
      that is, one that is derived from XHObjectMixin.

    """

    def GetConnectPoint(self):
        return self.XY


class MovingBitmap(FC.ScaledBitmap, MovingObjectMixin, ConnectorObjectMixin):
    """
    ScaledBitmap Object that can be moved
    """
    ## All we need to do is is inherit from:
    ##  ScaledBitmap, MovingObjectMixin and ConnectorObjectMixin
    pass


class MovingCircle(FC.Circle, MovingObjectMixin, ConnectorObjectMixin):
    """
    ScaledBitmap Object that can be moved
    """
    ## All we need to do is is inherit from:
    ##  Circle MovingObjectMixin and ConnectorObjectMixin
    pass


class MovingGroup(FC.Group, MovingObjectMixin, ConnectorObjectMixin):
    def GetConnectPoint(self):
        return self.BoundingBox.Center


class NodeObject(FC.Group, MovingObjectMixin, ConnectorObjectMixin):
    """
    A version of the moving group for nodes -- an ellipse with text on it.
    """

    def __init__(self,
                 Label,
                 XY,
                 WH,
                 BackgroundColor="Yellow",
                 TextColor="Black",
                 InForeground=False,
                 IsVisible=True):
        self.Name = Label
        XY = N.asarray(XY, N.float).reshape(2, )
        WH = N.asarray(WH, N.float).reshape(2, )
        print "-------"
        print WH
        print "-------"
        Label = FC.ScaledText(Label,
                              XY,
                              Size=WH[1] / 2.0,
                              Color=TextColor,
                              Position='cc',
        )
        self.Ellipse = FC.Ellipse((XY - WH / 2.0),
                                  WH,
                                  FillColor=BackgroundColor,
                                  LineStyle="Solid",
        )
        FC.Group.__init__(self, [self.Ellipse, Label], InForeground, IsVisible)

    def GetConnectPoint(self):
        return self.BoundingBox.Center


class MovingText(FC.ScaledText, MovingObjectMixin, ConnectorObjectMixin):
    """
    ScaledBitmap Object that can be moved
    """
    ## All we need to do is is inherit from:
    ##  ScaledBitmap, MovingObjectMixin and ConnectorObjectMixin
    pass


class MovingTextBox(FC.ScaledTextBox, MovingObjectMixin, ConnectorObjectMixin):
    """
    ScaledBitmap Object that can be moved
    """
    ## All we need to do is is inherit from:
    ##  ScaledBitmap, MovingObjectMixin and ConnectorObjectMixin
    pass


class ConnectorLine(FC.LineOnlyMixin, FC.DrawObject, ):
    """

    A Line that connects two objects -- it uses the objects to get its coordinates
    The objects must have a GetConnectPoint() method.

    """
    ##fixme: this should be added to the Main FloatCanvas Objects some day.
    def __init__(self,
                 Object1,
                 Object2,
                 LineColor="Black",
                 LineStyle="Solid",
                 LineWidth=1,
                 InForeground=False):
        FC.DrawObject.__init__(self, InForeground)

        self.Object1 = Object1
        self.Object2 = Object2
        self.LineColor = LineColor
        self.LineStyle = LineStyle
        self.LineWidth = LineWidth

        self.CalcBoundingBox()
        self.SetPen(LineColor, LineStyle, LineWidth)

        self.HitLineWidth = max(LineWidth, self.MinHitLineWidth)

    def CalcBoundingBox(self):
        self.BoundingBox = BBox.fromPoints((self.Object1.GetConnectPoint(),
                                            self.Object2.GetConnectPoint()))
        if self._Canvas:
            self._Canvas.BoundingBoxDirty = True


    def _Draw(self, dc, WorldToPixel, ScaleWorldToPixel, HTdc=None):
        Points = N.array((self.Object1.GetConnectPoint(),
                          self.Object2.GetConnectPoint()))
        Points = WorldToPixel(Points)
        dc.SetPen(self.Pen)
        dc.DrawLines(Points)
        if HTdc and self.HitAble:
            HTdc.SetPen(self.HitPen)
            HTdc.DrawLines(Points)

    # Override the default OutlinePoints
    def GetOutlinePoints(self):
        return self.Points

    def CompPoints(self, XY, L):
        c = L / N.sqrt(3)

        Points = N.array(((0, c),
                          ( L / 2.0, -c / 2.0),
                          (-L / 2.0, -c / 2.0)),
                         N.float_)

        Points += XY
        return Points


# Tree Utilities
# And some hard coded data...

class TreeNode:
    dx = 45
    dy = 4

    def __init__(self, name, Children=[]):
        self.Name = name
        #self.parent = None -- Is this needed?
        self.Children = Children
        self.Point = None  # The coords of the node.

    def __str__(self):
        return "TreeNode: %s" % self.Name

    __repr__ = __str__

## Build Tree:
#leaves = [TreeNode(name) for name in ["20", "19"]]
#VP1 = TreeNode("11", Children=leaves)
#VP2 = TreeNode("12", [
#    TreeNode("13", [TreeNode("14", [TreeNode("12", [TreeNode("16", [TreeNode("17", [TreeNode("18")])])])])])])

#CEO = TreeNode("5", [VP1, VP2])
#Father = TreeNode("2", [TreeNode("3"), TreeNode("4")])
#elements = TreeNode("1", [CEO, Father])

#CEO = TreeNode("3", [TreeNode("5",[TreeNode("6",[TreeNode("9",[TreeNode("8")])])])])
#Father = TreeNode("2", [TreeNode("7", [TreeNode("8",[TreeNode("10",[TreeNode("11",[TreeNode("12")])])])])])
#elements = TreeNode("mov eax, eax", [CEO, Father])

siete = TreeNode("mov eax,ebp+4",[TreeNode("mov [esp],eax", [TreeNode("call juas", [TreeNode("pop eax", [TreeNode("mov eax,ecx")])])])])
seis = TreeNode("mov eax,2A", [TreeNode("pop eax")])
cinco = TreeNode("mov eax, 0", [TreeNode("mov eax,2A")])
cuatro = TreeNode("cmp ebp+4,5", [cinco, seis])
elements = TreeNode("push ebp", [TreeNode("mov ebp,esp", [TreeNode("sub esp,4",[siete, cuatro])])])

#elements = TreeNode("push ebp", [TreeNode("mov ebp,esp",[ [TreeNode ("mov eax,ebp+4")],[ TreeNode("cmp ebp+4,5"), [ [TreeNode("move eax, 2A")],[TreeNode("mov eax, 0")])]]]]

class logica:
    aux = []

    def __init__(self, cadena):
        self.formula = []
        self.elements = elements
        for item in cadena:
            if len(item) > 0:
                self.formula.append(item)
        if self.checkFormula(self.formula, self.elements):
            wx.MessageBox('True', 'Result', wx.OK | wx.ICON_INFORMATION)
        else:
            wx.MessageBox('False', 'Result', wx.OK | wx.ICON_INFORMATION)

    def checkFormula(self, formula, arbol):
        if formula[0] == "EF":
            print "-> EF"
            if self.checkFormula(formula[1:], arbol):
                return True
            else:
                for node in arbol.Children:
                    if self.checkFormula(formula, node):
                        return True
                return False
        elif formula[0] == "AF":
            print "-> AF"
            if self.checkFormula(formula[1:], arbol):
                return True
            else:
                for node in arbol.Children:
                    if self.checkFormula(formula, node):
                        return True
                return False
        elif formula[0] == "EX":
            print "-> EX"
            for node in arbol.Children:
                if self.checkFormula(formula[1:], node):
                    return True
            return False
        elif formula[0] == "AX":
            print "-> AX"
            for node in arbol.Children:
                if not self.checkFormula(formula[1:], node):
                    return False
            return True
        elif formula[0] == "EG":
            print "-> EG"
            for node in arbol.Children:
                if self.checkFormula(formula, node):
                    if len(node.Children) > 0:
                        self.checkFormula(formula, node)
                    else:
                        return True
            return False

        elif formula[0] == "AG":
            print "-> AG"
            for node in arbol.Children:
                if self.checkFormula(formula, node):
                    if len(node.Children) > 0:
                        self.checkFormula(formula, node)
                else:
                    return False
            return True
        elif formula[0] == "E":
            print "-> EU"
            aux = []
            for part in formula[1:]:
                if part == "U":
                    break
                else:
                    aux.append(part)
            part1 = self.checkFormula(aux, arbol)
            encuentro = 0
            for part in formula[1:]:
                if encuentro == 0:
                    if part == "U":
                        encuentro = 1
                else:
                    part2 = self.checkFormula(["EF", part], arbol)
                    break
            if part1 & part2:
                return True
            else:
                return False
        elif formula[0] == "A":
            print "-> AU"
            aux = []
            for part in formula[1:]:
                if part == "U":
                    break
                else:
                    aux.append(part)
            part1 = self.checkFormula(aux, arbol)
            encuentro = 0
            for part in formula[1:]:
                if encuentro == 0:
                    if part == "U":
                        encuentro = 1
                else:
                    part2 = self.checkFormula(["AF", part], arbol)
                    break
            if part1 & part2:
                return True
            else:
                return False
        elif formula[0] == "And":
            print "-> and"
            if not self.checkFormula(formula[1:], arbol) or not self.aux[-1]:
                self.aux.pop()
                return False
            else:
                self.aux.pop()
                return True
        elif formula[0] == "Or":
            print "-> or"
            if self.checkFormula(formula[1:], arbol) or self.aux[-1]:
                self.aux.pop()
                return True
            else:
                self.aux.pop()
                return False
        elif formula[0] == "Not":
            if not self.checkFormula(formula[1:], arbol):
                return True
            else:
                return False
        else:
            print "---> " + formula[0]
            if formula[0] == arbol.Name:
                if len(formula) > 1:
                    self.aux.append(True)
                    return self.checkFormula(formula[1:], arbol)
                else:
                    return True
            else:
                if len(formula) > 1:
                    self.aux.append(False)
                    self.checkFormula(formula[1:], arbol)
                else:
                    return False

# Mirando el arbol para calcular las coordenadas de cada nodo

def LayoutTree(root, x, y, level):
    NumNodes = len(root.Children)
    root.Point = (x, y)
    x += root.dx
    y += (root.dy * level * (NumNodes - 1) / 2.0)
    for node in root.Children:
        print str(x)+":"+str(y)
        LayoutTree(node, x, y, level - 1)
        y -= root.dy * level


def TraverseTree(root, func):
    func(root)
    for child in root.Children:
        TraverseTree(child, func)


class DrawFrame(wx.Frame):
    url = ""
    title = ""
    ID_ONE = 1
    ID_TWO = 2
    ID_AND = 3
    ID_OR = 4
    ID_FUTURE = 5
    ID_NEXT = 6
    ID_UNTIL = 7
    ID_IMPLIES = 8
    ID_ALWAYS = 9
    ID_EXISTS = 10
    ID_FORALL = 11

    registers32 = ["eax", "ebx", "ecx", "edx","esi","edi","ebp","esp","eip","cs","ds","ss","es","fs","gs"]
    registers16 = ["ax","bx","cx","dx","si","di","sp","bp","ip"]
    registers8 = ["ah","al","bh","bl","ch","cl","dh","dl"]

    opcodes = ["mov","push","call","pop","cmp","jz","lea","test","jmp","add","jnz","retn","xor","and"]

    def __init__(self, *args, **kwargs):
        # ids para los botones
        self.dirname = ''
        self.position = 50


        wx.Frame.__init__(self, *args, **kwargs)

        # Set up the MenuBar

        menubar = wx.MenuBar()
        fileMenu = wx.Menu()
        editMenu = wx.Menu()
        poctplMenu = wx.Menu()
        helpMenu = wx.Menu()

        menubar.Append(fileMenu, '&File')
        menubar.Append(editMenu, '&Edit')
        menubar.Append(poctplMenu, '&Poctpl')
        menubar.Append(helpMenu, '&Help')
        self.SetMenuBar(menubar)

        # File menu
        menuOpen = fileMenu.Append(wx.ID_OPEN, '&Open', 'Open a file to edit')
        fitem = fileMenu.Append(wx.ID_EXIT, 'Quit', 'Quit application')

        # Menu Events
        self.Bind(wx.EVT_MENU, self.OnOpen, menuOpen)
        self.Bind(wx.EVT_MENU, self.OnQuit, fitem)


        # self.SetMenuBar(MenuBar)

        self.CreateStatusBar()
        self.SetStatusText("")

        wx.EVT_CLOSE(self, self.OnCloseWindow)

        self.toolbar = self.CreateToolBar()

        qtool1 = self.toolbar.AddLabelTool(self.ID_ONE, 'One', self.scale_bitmap(wx.Bitmap('images/1.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.On1, qtool1)

        qtool2 = self.toolbar.AddLabelTool(self.ID_TWO, 'Two', self.scale_bitmap(wx.Bitmap('images/2.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.On2, qtool2)

        qtoolAnd = self.toolbar.AddLabelTool(self.ID_AND, 'And', self.scale_bitmap(wx.Bitmap('images/and.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.OnAnd, qtoolAnd)

        qtoolOr = self.toolbar.AddLabelTool(self.ID_OR, 'Or', self.scale_bitmap(wx.Bitmap('images/or.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.OnOr, qtoolOr)

        qtoolFuture = self.toolbar.AddLabelTool(self.ID_FUTURE, 'Future', self.scale_bitmap(wx.Bitmap('images/future.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.OnFuture, qtoolFuture)

        qtoolNext = self.toolbar.AddLabelTool(self.ID_NEXT, 'Next', self.scale_bitmap(wx.Bitmap('images/next.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.OnNext, qtoolNext)

        qtoolUntil = self.toolbar.AddLabelTool(self.ID_UNTIL, 'Until', self.scale_bitmap(wx.Bitmap('images/until.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.OnUntil, qtoolUntil)

        qtoolImplies = self.toolbar.AddLabelTool(self.ID_IMPLIES, 'Implies', self.scale_bitmap(wx.Bitmap('images/implies.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.OnImplies, qtoolImplies)

        qtoolAlways = self.toolbar.AddLabelTool(self.ID_ALWAYS, 'Always', self.scale_bitmap(wx.Bitmap('images/always.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.OnAlways, qtoolAlways)

        qtoolExists = self.toolbar.AddLabelTool(self.ID_EXISTS, 'Exists', self.scale_bitmap(wx.Bitmap('images/exists.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.OnExists, qtoolExists)

        qtoolForAll = self.toolbar.AddLabelTool(self.ID_FORALL, 'Forall', self.scale_bitmap(wx.Bitmap('images/forall.png'), 80, 60))
        self.Bind(wx.EVT_TOOL, self.OnForAll, qtoolForAll)

        # Deshabilitar botones
        self.toolbar.EnableTool(self.ID_ONE, False)
        self.toolbar.EnableTool(self.ID_TWO, False)
        self.toolbar.EnableTool(self.ID_AND, False)
        self.toolbar.EnableTool(self.ID_OR, False)
        self.toolbar.EnableTool(self.ID_FUTURE, False)
        self.toolbar.EnableTool(self.ID_NEXT, False)
        self.toolbar.EnableTool(self.ID_UNTIL, False)
        self.toolbar.EnableTool(self.ID_IMPLIES, False)
        self.toolbar.EnableTool(self.ID_ALWAYS, False)
        self.toolbar.EnableTool(self.ID_EXISTS, False)
        self.toolbar.EnableTool(self.ID_FORALL, False)


        self.toolbar.Realize()

        # creamos el panel

        panel = wx.Panel(self)

        # preparando el lienzo para dibujar

        NC = NavCanvas.NavCanvas(panel, -1, (500, 500),
                                 ProjectionFun=None,
                                 Debug=0,
                                 BackgroundColor="White",
        )

        self.Canvas = NC.Canvas

        NC.Canvas.Bind(FC.EVT_MOTION, self.OnMove)
        NC.Canvas.Bind(FC.EVT_LEFT_UP, self.OnLeftUp)

        # self.elements = elements
        # LayoutTree(self.elements, 0, 0, 3)
        # self.AddTree(self.elements)



        # creamos la barra de la formula

        formula = wx.BoxSizer(wx.HORIZONTAL)
        # instructions = 'Formula:'
        # instructLbl = wx.StaticText(panel, label=instructions)
        self.FormulaField = wx.TextCtrl(panel, size=((2000, 20)))
        self.FormulaField.Disable()
        checkButton = wx.Button(panel, label='Check')
        checkButton.Bind(wx.EVT_BUTTON, self.check)
        # formula.Add(instructLbl,proportion=0, flag=wx.LEFT, border=5)
        formula.Add(self.FormulaField, proportion=1, flag=wx.EXPAND, border=5)
        formula.Add(checkButton, proportion=0, flag=wx.LEFT, border=5)


        # lay it out:
        S = wx.BoxSizer(wx.VERTICAL)
        # S.Add(butSizer, 0, wx.ALIGN_CENTER | wx.ALL, 5)
        # S.Add(hbox1,1, flag=wx.LEFT | wx.TOP, border=10)
        # S.Add(formula,1, wx.EXPAND)
        S.Add(NC, 2, wx.EXPAND | wx.ALL)

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add(formula, proportion=0, border=5)
        vbox.Add(S, proportion=1, flag=wx.EXPAND | wx.ALL, border=5)
        panel.SetSizer(vbox)

        # self.timer = wx.Timer(self)
        # self.Bind(wx.EVT_TIMER, self.MoveBall, self.timer)

        self.Show(True)
        # NC.Canvas.ZoomToBB()

        self.MoveObject = None
        self.Moving = False

    def check(self, event):
        juas = True
        aux = self.FormulaField.GetValue()
        aux2 = []
        if aux == "":
            self.FormulaField.SetBackgroundColour((255,0,0))
        else:
            self.FormulaField.SetBackgroundColour((255,255,255))
        formulaca = re.split("\W+", aux)
        logica(formulaca)



    def OnOpen(self, e):
        self.file_open()
        e.Skip()

    def OnQuit(self, event):
        self.Close(True)

    def OnCloseWindow(self, event):
        self.Destroy()

        # Menu para abrir un fichero

    def file_open(self):  # 9
        with wx.FileDialog(self, "Choose a file to open", self.dirname, "", "*.*", wx.OPEN) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                directory, filename = dlg.GetDirectory(), dlg.GetFilename()
                filename = dlg.GetPath()
                print filename
                self.elements = elements
                LayoutTree(self.elements, 0, 0, 3)
                self.AddTree(self.elements)
                self.Canvas.Draw()
                self.Canvas.ZoomToBB()
                self.FormulaField.Enable()
                # Deshabilitar botones
                self.toolbar.EnableTool(self.ID_ONE, True)
                self.toolbar.EnableTool(self.ID_TWO, True)
                self.toolbar.EnableTool(self.ID_AND, True)
                self.toolbar.EnableTool(self.ID_OR, True)
                self.toolbar.EnableTool(self.ID_FUTURE, True)
                self.toolbar.EnableTool(self.ID_NEXT, True)
                self.toolbar.EnableTool(self.ID_UNTIL, True)
                self.toolbar.EnableTool(self.ID_IMPLIES, True)
                self.toolbar.EnableTool(self.ID_ALWAYS, True)
                self.toolbar.EnableTool(self.ID_EXISTS, True)
                self.toolbar.EnableTool(self.ID_FORALL, True)

    def On1(self, e):
        # a=wx.MessageDialog(None, 'Boton 1', 'boton 1', style=wx.OK)
        # a.ShowModal()
        # self.photoTxt.SetValue('hello')
        # png=wx.StaticBitmap(self, -1, scale_bitmap(wx.Bitmap('images/1.png'),80,60))

        dialog = wx.TextEntryDialog(None, "Introduce a value:", "Value", "", style=wx.OK | wx.CANCEL)
        if dialog.ShowModal() == wx.ID_OK:
            aux = self.FormulaField.GetValue()
            self.FormulaField.SetValue(aux + "(" + dialog.GetValue() + ")")
        dialog.Destroy()

        # Descomentar para incluir imagenes
        # self.picture = wx.StaticBitmap(self,size=(80,60),pos=(self.position,50))
        # self.picture.SetBitmap(self.scale_bitmap(wx.Bitmap('images/1.png'),80,60))
        # self.position+=80

        # dc = wx.PaintDC(self)
        # png = wx.Image('images/1.png', wx.BITMAP_TYPE_PNG)
        # dc.DrawBitMap(self.png, 100,100)

    def On2(self, e):
        dialog = wx.TextEntryDialog(None, "Introduce a value:", "Not Value", "", style=wx.OK | wx.CANCEL)
        if dialog.ShowModal() == wx.ID_OK:
            aux = self.FormulaField.GetValue()
            self.FormulaField.SetValue(aux + "Not(" + dialog.GetValue())
        dialog.Destroy()

        # self.picture = wx.StaticBitmap(self,size=(80,60),pos=(self.position,50))
        # self.picture.SetBitmap(self.scale_bitmap(wx.Bitmap('images/2.png'),80,60))
        # self.position+=80

    def OnAnd(self, e):
        aux = self.FormulaField.GetValue()
        self.FormulaField.SetValue(aux + "And")


        # self.picture = wx.StaticBitmap(self,size=(80,60),pos=(self.position,50))
        # self.picture.SetBitmap(self.scale_bitmap(wx.Bitmap('images/and.png'),80,60))
        # self.position+=80

    def OnOr(self, e):
        # dialog = wx.TextEntryDialog(None, "Introduce a value:","Not Value", "", style=wx.OK|wx.CANCEL)
        aux = self.FormulaField.GetValue()
        self.FormulaField.SetValue(aux + "Or")

        # self.picture = wx.StaticBitmap(self,size=(80,60),pos=(self.position, 50))
        # self.picture.SetBitmap(self.scale_bitmap(wx.Bitmap('images/or.png'),80,60))
        # self.position+=80

    def OnFuture(self, e):
        chgdep = choosePaths(None,
                             title='Choose a value:')
        if chgdep.ShowModal() == wx.ID_OK:
            aux = self.FormulaField.GetValue()
            auxValue = chgdep.GetValue()
            if auxValue == 1:
                self.FormulaField.SetValue(aux + "EF")
            elif auxValue == 2:
                self.FormulaField.SetValue(aux + "AF")
            chgdep.Destroy()


            # self.picture = wx.StaticBitmap(self,size=(80,60),pos=(self.position, 50))
            # self.picture.SetBitmap(self.scale_bitmap(wx.Bitmap('images/future.png'),80,60))
            # self.position+=80

    def OnNext(self, e):
        chgdep = choosePaths(None,
                             title='Choose a value:')
        if chgdep.ShowModal() == wx.ID_OK:
            aux = self.FormulaField.GetValue()
            auxVal = chgdep.GetValue()
            if auxVal == 1:
                self.FormulaField.SetValue(aux + "EX")
            elif auxVal == 2:
                self.FormulaField.SetValue(aux + "AX")
            chgdep.Destroy()

            # self.picture = wx.StaticBitmap(self,size=(80,60),pos=(self.position, 50))
            # self.picture.SetBitmap(self.scale_bitmap(wx.Bitmap('images/next.png'),80,60))
            # self.position+=80

    def OnUntil(self, e):
        chgdep = choosePathsUntil(None,
                                  title='Choose a value:')
        if chgdep.ShowModal() == wx.ID_OK:
            aux = self.FormulaField.GetValue()
            auxVal = chgdep.GetValue()
            if auxVal == 1:
                self.FormulaField.SetValue(aux + "E")
            elif auxVal == 2:
                self.FormulaField.SetValue(aux + "A")
            elif auxVal == 3:
                self.FormulaField.SetValue(aux + "U")
            chgdep.Destroy()

            # self.picture = wx.StaticBitmap(self,size=(80,60),pos=(self.position, 50))
            # self.picture.SetBitmap(self.scale_bitmap(wx.Bitmap('images/until.png'),80,60))
            # self.position+=80

    def OnImplies(self, e):
        dialog = wx.TextEntryDialog(None, "Introduce a value:", "Not Value", "", style=wx.OK | wx.CANCEL)
        if dialog.ShowModal() == wx.ID_OK:
            aux = self.FormulaField.GetValue()
            self.FormulaField.SetValue(aux + "->")
        dialog.Destroy()

        # self.picture = wx.StaticBitmap(self,size=(80,60),pos=(self.position, 50))
        # self.picture.SetBitmap(self.scale_bitmap(wx.Bitmap('images/implies.png'),80,60))
        # self.position+=80

    def OnAlways(self, e):
        chgdep = choosePaths(None,
                             title='Choose a value:')
        if chgdep.ShowModal() == wx.ID_OK:
            aux = self.FormulaField.GetValue()
            auxVal = chgdep.GetValue()
            if auxVal == 1:
                self.FormulaField.SetValue(aux + "EU")
            elif auxVal == 2:
                self.FormulaField.SetValue(aux + "AU")
            chgdep.Destroy()

            # self.picture = wx.StaticBitmap(self,size=(80,60),pos=(self.position, 50))
            # self.picture.SetBitmap(self.scale_bitmap(wx.Bitmap('images/always.png'),80,60))
            # self.position+=80

            # def OnLeftParen(self, e):
            # a=wx.MessageDialog(None, 'Boton Until', 'boton Until', style=wx.OK)
            # a.ShowModal()
            #	self.picture = wx.StaticBitmap(self,size=(50,60),pos=(self.position, 50))
            #	self.picture.SetBitmap(self.scale_bitmap(wx.Bitmap('images/leftParen.png'),50,60))
            #	self.position+=50

            # def OnRightParen(self, e):
            # a=wx.MessageDialog(None, 'Boton Until', 'boton Until', style=wx.OK)
            # a.ShowModal()

    def OnExists(self, e):
        aux = self.FormulaField.GetValue()
        self.FormulaField.SetValue(aux + "Exists")

    def OnForAll(self, e):
        aux = self.FormulaField.GetValue()
        self.FormulaField.SetValue(aux + "ForAll")

    def scale_bitmap(self, bitmap, width, height):
        image = wx.ImageFromBitmap(bitmap)
        image = image.Scale(width, height, wx.IMAGE_QUALITY_HIGH)
        result = wx.BitmapFromImage(image)
        return result

    def AddTree(self, root):
        Nodes = []
        Connectors = []
        EllipseW = 15
        EllipseH = 4

        def CreateObject(node):
            encuentro = 0
            for nod in Nodes:
                for att in nod.__dict__:
                    if att == "Name":
                        if nod.Name == node.Name:
                            object = nod
                            encuentro = 1
            if encuentro == 0:
                object = NodeObject(node.Name,
                                    node.Point,
                                    (40, 7),
                                    BackgroundColor="light gray",
                                    TextColor="Black",
                                    )
            node.DrawObject = object
            Nodes.append(object)

        def AddConnectors(node):
            for child in node.Children:
                Connector = ConnectorLine(node.DrawObject, child.DrawObject, LineWidth=3, LineColor="Black")
                Connectors.append(Connector)
        # create the Objects
        TraverseTree(root, CreateObject)
        # create the Connectors
        TraverseTree(root, AddConnectors)
        # Add the conenctos to the Canvas first, so they are undernieth the nodes
        self.Canvas.AddObjects(Connectors)
        # now add the nodes
        self.Canvas.AddObjects(Nodes)
        # Now bind the Nodes -- DrawObjects must be Added to a Canvas before they can be bound.
        for node in Nodes:
            # pass
            node.Bind(FC.EVT_FC_LEFT_DOWN, self.ObjectHit)

    def ObjectHit(self, object):
        if not self.Moving:
            self.Moving = True
            self.StartPoint = object.HitCoordsPixel
            self.StartObject = self.Canvas.WorldToPixel(object.GetOutlinePoints())
            self.MoveObject = None
            self.MovingObject = object

    def OnMove(self, event):
        """
        Updates the status bar with the world coordinates
        and moves the object it is clicked on

        """
        self.SetStatusText("%.4f, %.4f" % tuple(event.Coords))

        if self.Moving:
            dxy = event.GetPosition() - self.StartPoint
            # Draw the Moving Object:
            dc = wx.ClientDC(self.Canvas)
            dc.SetPen(wx.Pen('WHITE', 2, wx.SHORT_DASH))
            dc.SetBrush(wx.TRANSPARENT_BRUSH)
            dc.SetLogicalFunction(wx.XOR)
            if self.MoveObject is not None:
                dc.DrawPolygon(self.MoveObject)
            self.MoveObject = self.StartObject + dxy
            dc.DrawPolygon(self.MoveObject)

    def OnLeftUp(self, event):
        if self.Moving:
            self.Moving = False
            if self.MoveObject is not None:
                dxy = event.GetPosition() - self.StartPoint
                dxy = self.Canvas.ScalePixelToWorld(dxy)
                self.MovingObject.Move(dxy)
            self.Canvas.Draw(True)

if __name__ == "__main__":
    app = wx.App(0)
    DrawFrame(None, -1, "POCTPL", wx.DefaultPosition, (1000, 700))
    app.MainLoop()