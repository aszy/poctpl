#######################################################
# Lexer
#######################################################

import ply.lex as lex

# Tokens para controlar la formula

tokens = (
    "FORMULA",
    "OPERACION",
    "MEMORIA",
	"OPCODE",
	"LLAMADA",
	"API",
    "REGISTRO",
    "LPAREN",
    "RPAREN",
	"LCORC",
	"RCORC",
	"COMA",
	"ORCAMINO",
	"ANDCAMINO",
	"IMPCAMINO",
	"COIMPCAMINO",
	"NOTCAMINO",
	"ORESTADO",
	"ANDESTADO",
	"IMPESTADO",
	"COIMPESTADO",
	"NOTESTADO",
	"UFORM"
)

reserved = {
	'EX' : 'EX'
}

# Expresiones regulares para los tokens

t_FORMULA =  (
    r"E[XFUG]|A[XFUG]"
)

t_LPAREN = r"\("

t_RPAREN = r"\)"

t_LCORC = r"\["

t_RCORC = r"\]"

t_MEMORIA = "MEM"

t_COMA = r"\,"

t_ignore  = ' \t'

t_UFORM = 'U'

t_ORCAMINO = r"\|\|"

t_ANDCAMINO = r"\&\&"

t_IMPCAMINO = r"\-\>"

t_COIMPCAMINO = r"\<\-\>"

t_NOTCAMINO = r"\-"

t_ORESTADO = "OR"

t_ANDESTADO = "AND"

T_IMPESTADO = "IMP"

T_COIMPESTADO = "COIMP"

T_NOTESTADO = "NOT"

t_OPCODE = (
	r"Mov|Push|Pop|Cmp|Jz|Lea|Test|Jmp|Add|Jnz|Xor|And|Retn"
)

t_LLAMADA = (
	r"call"
)

t_API = (
	r"strcpy|strcpyA|strcpyW|wcscpy|_tcscpy|_mbscpy|StrCpy|StrCpyA|StrCpyW|lstrcpy|lstrcpyA|lstrcpyW|_tccpy|_mbccpy|_ftcscpy|strncpy|wcsncpy|_tcsncpy|_mbsncpy|_mbsnbcpy|StrCpyN|StrCpyNA|StrCpyNW|StrNCpy|strcpynA|StrNCpyA|StrNCpyW|strcpyn|lstrcpynA|lstrcpynW|strcat|strcatA|strcatW|wcscat|_tcscat|_mbscat|StrCat|StrCatA|StrCatW|lstrcat|lstrcatA|lstrcatW|StrCatBuff|StrCatBuffA|StrCatBuffW|StrCatChainW|_tccat|_mbccat|_ftcscat|strncat|wcsncat|_tcsncat|_mbsncat|_mbsnbcat|StrCatN|StrCatNA|StrCatNW|StrNCat|StrNCatA|StrNCatW|lstrncat|lstrcatnA|lstrcatnW|lstrcatn|sprintfW|sprintfA|wsprintf|wsprintfW|wsprintfA|sprintf|swprintf|_stprintf|wvsprintf|wvsprintfA|wvsprintfW|vsprintf|_vstprintf|vswprintf|_fstrncpy|_fstrncat|_gets|_getts|_gettws|IsBadWritePtr|IsBadHugeWritePtr|IsBadReadPtr|IsBadHugeReadPtr|IsBadCodePtr|IsBadStringPtr|memcpy|RtlCopyMemory|CopyMemory|wmemcpy|ReadFile"
)

t_OPERACION = (
	r"RegAdd(32|16|8)|RegReg(32|16|8)|AddReg(32|16|8)|AddAdd(32|16|8)|Regi(32|16|8)"
)

t_REGISTRO = (
	r"Ah|Al|Bh|Bl|Ch|Cl|Dh|Dl|Ax|Bx|Cx|Dx|SI|DI|SP|IP|Eax|Ebx|ECx|EDx|ESI|EDI|EBP|ESP|EIP|CS|DS|SS|ES|FS|GS"
)

def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def t_error(t):
	print("Illegal character near: '%s'" % t.value)
	#raise TypeError("Unknown text '%s'" % (t.value))
	cont=1
	letra = lexer.lexdata[lexer.lexpos+cont]
	while (letra != ' ' and letra != '[' and letra != ']' and letra != '(' and letra != ')' and letra != ','):
		cont+=1
		letra = lexer.lexdata[lexer.lexpos+cont]
	t.lexer.skip(cont)
	

lexer = lex.lex()

#######################################################
# Parser
#######################################################

import ply.yacc as yacc

precedence = (
    ('left', 'ORCAMINO','ANDCAMINO','UFORM','NOTCAMINO'),
    ('left', 'IMPCAMINO','COIMPCAMINO'),
    ('left', 'ORESTADO','ANDESTADO','NOTESTADO'),
    ('left', 'IMPESTADO','COIMPESTADO')
)

def p_FormulaCaminos_Simple(p):
	'FormulaCaminos : FORMULA LCORC FormulaCaminos RCORC'
	if (p[3] != None):
		p[0] = p[1]+"2 ("+ p[3] +")"
	
# Controlando el error en la formula anterior
def p_FormulaCaminos_Simple_error(p):
	'FormulaCaminos : FORMULA LCORC error RCORC'
	#print "Error en FormulaCaminos_Simple"
	
def p_FormulaCaminos_Compuesta(p):
	'FormulaCaminos : FORMULA LCORC FormulaCaminos UFORM FormulaCaminos RCORC'
	p[0] = p[2] + 'U' + p[4]
	
# Controlando el error en FormulaCaminos_Compuesta
def p_FormulaCaminos_Compuesta_error(p):
	'FormulaCaminos : FORMULA LCORC FormulaCaminos UFORM error RCORC'
	#print "Error en FormulaCaminos_Compuesta"
	
def p_FormulaCaminos_OR(p):
	'FormulaCaminos : FormulaCaminos ORCAMINO FormulaCaminos'
	p[0] = p[1] + '||' + p[3]
	
# Controlando los errores en FormulaCaminos_Or
def p_FormulaCaminos_OR_error(p):
	'FormulaCaminos : FormulaCaminos ORCAMINO error'
	#print "Error en FormulaCaminos_OR"
	
def p_FormulaCaminos_AND(p):
	'FormulaCaminos : FormulaCaminos ANDCAMINO FormulaCaminos'
	p[0] = p[1] + '&&' + p[3]
	
# Controlando el error en FormulaCaminos_AND
def p_FormulaCaminos_AND_error(p):
	'FormulaCaminos : FormulaCaminos ANDCAMINO error'
	#print "Error en FormulaCaminos_AND"
	
def p_FormulaCaminos_IMP(p):
	'FormulaCaminos : FormulaCaminos IMPCAMINO FormulaCaminos'
	p[0] = p[1] + '->' + p[3]
	
# Controlando el error de formulaCaminos_IMP
def p_FormulaCaminos_IMP_error(p):
	'FormulaCaminos : FormulaCaminos IMPCAMINO error'
	#print "Error en FormulaCaminos_IMP"
	
def p_FormulaCaminos_COIMP(p):
	'FormulaCaminos : FormulaCaminos COIMPCAMINO FormulaCaminos'
	p[0] = p[1] + '<->' + p[3]
	
# Controlando los errores de FormulaCaminos_COIMP
def p_FormulaCaminos_COIMP_error(p):
	'FormulaCaminos : FormulaCaminos COIMPCAMINO error'
	#print "Error en FormulaCaminos_COIMP"
	
def p_FormulaCaminos_NOT(p):
	'FormulaCaminos : NOTCAMINO FormulaCaminos'
	p[0] = 'NOTCAMINO' + p[3]
	
# Controlando los errores de FormulaCaminos_NOT
def p_FormulaCaminos_NOT_error(p):
	'FormulaCaminos : NOTCAMINO error'
	#print "Error en FormulaCaminos_NOT"
	
def p_FormulaCaminos_FormulaEstado(p):
	'FormulaCaminos : FormulaEstados'
	p[0] = p[1]
	
def p_FormulaEstados_Simple(p):
	'FormulaEstados : FORMULA LPAREN FormulaEstados RPAREN'
	#print p[0]
	p[0] = p[1] + " (" + str(p[3]) + ")"
	
# Controlando el error en FormulaEstados_Simple
def p_FormulaEstados_Simple_error(p):
	'FormulaEstados : FORMULA LPAREN error RPAREN'
	#print "Error en FormulaEstados_Simple"
	#raise SyntaxError
	
def p_FormulaEstados_Compuesta(p):
	'FormulaEstados : FORMULA LPAREN FormulaEstados UFORM FormulaEstados RPAREN'
	p[0] = p[2] + 'U' + p[4]
	
# Controlando los errores en FormulaEstados_Compuesta
def p_FormulaEstados_Compuesta_error(p):
	'FormulaEstados : FORMULA LPAREN FormulaEstados UFORM error RPAREN'
	#print "Error en FormulaEstados_Compuesta"
	
def p_FormulaEstados_OR(p):
	'FormulaEstados : FormulaEstados ORESTADO FormulaEstados'
	p[0] = p[1] + 'OR' + p[3]
	
# Comprobando los errores en FormulaEstados_OR
def p_FormulaEstados_OR_error(p):
	'FormulaEstados : FormulaEstados ORESTADO error'
	#print "Error en FormulaEstados_OR"
	
def p_FormulaEstados_AND(p):
	'FormulaEstados : FormulaEstados ANDESTADO FormulaEstados'
	p[0] = p[1] + 'AND' + p[3]
	
# Comprobando errores en FormulaEstados_AND
def p_FormulaEstados_AND_error(p):
	'FormulaEstados : FormulaEstados ANDESTADO error'
	#print "Error en FormulaEstados_AND"
	
def p_FormulaEstados_IMP(p):
	'FormulaEstados : FormulaEstados IMPESTADO FormulaEstados'
	p[0] = p[1] + 'IMP' + p[3]
	
# Comprobando errores en FormulaEstados_IMP
def p_FormulaEstados_IMP_error(p):
	'FormulaEstados : FormulaEstados IMPESTADO error'
	#print "Error en FormulaEstados_IMP"
	
def p_FormulaEstados_COIMP(p):
	'FormulaEstados : FormulaEstados COIMPESTADO FormulaEstados'
	p[0] = p[1] + 'COIMP' + p[3]
	
# Comprobando errores en FormulaEstados_COIMP
def p_FormulaEstados_COIMP_error(p):
	'FormulaEstados : FormulaEstados COIMPESTADO error'
	#print "Error en FormulaEstados_COIMP"
	
def p_FormulaEstados_NOT(p):
	'FormulaEstados : NOTESTADO FormulaEstados'
	p[0] = 'NOTESTADO' + p[3]
	
# Comprobrando los errores en FormulaEstados_NOT
def p_FormulaEstados_NOT_error(p):
	'FormulaEstados : NOTESTADO error'
	#print "Error en FormulaEstados_NOT"

def p_FormulaEstados_Llamada(p):
	'FormulaEstados : Llamada'
	p[0] = p[1]	

def p_Llamada(p):
	'Llamada : LLAMADA API'
	p[0] = 'Op ('+ p[1] + ' ' + p[2] +')'
	
def p_FormulaEstados_Opcode(p):
	'FormulaEstados : Opcode'
	p[0] = p[1]
	
def p_Opcode(p):
	'Opcode : OPCODE Operacion'
	p[0] = 'Op ('+ p[1] + ' (' + p[2] +'))'
	

	
def p_Opcode_error(p):
	'Opcode : OPCODE error'
	raise SyntaxError
	
def p_Operacion_MemReg(p):
	'Operacion : OPERACION MEMORIA COMA REGISTRO'
	p[0] = p[1]+'('+p[2] + ',' + p[4]+')'
	
def p_Operacion_MemMem(p):
	'Operacion : OPERACION MEMORIA COMA MEMORIA'
	p[0] = p[1]+'('+p[2] + ',' + p[4]+')'
	
def p_Operacion_RegMem(p):
	'Operacion : OPERACION REGISTRO COMA MEMORIA'
	p[0] = p[1]+'('+p[2] + ',' + p[4]+')'
	
def p_Operacion_RegReg(p):
	'Operacion : OPERACION REGISTRO COMA REGISTRO'
	p[0] = p[1]+'('+p[2] + ',' + p[4]+')'

# Error rule for syntax errors
def p_error(p):
	if p != None:
		print "Syntax error in input!"
		print("Illegal character near: '%s'" % p.value)
	
parser = yacc.yacc()

#######################################################
# Main
#######################################################

def analizador():
	formula = None
	while formula != "":
		try:
			s = raw_input('poctpl > ')
		except EOFError:
			break
		if not s: continue
		formula = parser.parse(s)
		if formula != None:
			return formula

