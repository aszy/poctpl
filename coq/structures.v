(** En esta seccion definiremos las estructuras con las
cuales CTPL va a realizar su posterior logica *)


(* begin hide *)
Require Import Arith.
Require Import List.
Require Import String.
(* end hide *)

(**
Creamos el tipo inductivo registro que va a contener los registros de la arquitectura x86
*)

Inductive register32:Set :=
   | eax : register32
   | eax_plus : nat -> register32
   | eax_minus : nat -> register32
   | ebx : register32
   | ebx_plus : nat -> register32
   | ebx_minus : nat -> register32
   | ecx : register32
   | ecx_plus : nat -> register32
   | ecx_minus : nat -> register32
   | edx : register32
   | edx_plus : nat -> register32
   | edx_minus : nat -> register32
   | esi : register32
   | esi_plus : nat -> register32
   | esi_minus : nat -> register32
   | edi : register32
   | edi_plus : nat -> register32
   | edi_minus : nat -> register32
   | ebp : register32
   | ebp_plus : nat -> register32
   | ebp_minus : nat -> register32
   | esp : register32
   | esp_plus : nat -> register32
   | esp_minus : nat -> register32
   | eip : register32
   | cs : register32
   | ds : register32
   | ss : register32
   | es : register32
   | fs : register32
   | gs : register32.

Inductive register16:Set :=
   | Ax : register16
   | Bx : register16
   | Cx : register16
   | Dx : register16
   | SI : register16
   | DI : register16
   | SP : register16
   | SP_plus : nat -> register16
   | SP_minus : nat -> register16
   | BP : register16
   | BP_plus : nat -> register16
   | BP_minus : nat -> register16
   | IP : register16.

Inductive register8: Set :=
   | AH : register8
   | AL : register8
   | BH : register8
   | BL : register8
   | CH : register8
   | CL : register8
   | DH : register8
   | DL : register8.

Inductive Mem: Set :=
   mem : Mem.

(**
El tipo contain seran los datos que va manejar un determinado opcode, que seran los
siguientes:
   - regAdd: sera un opcode del tipo "mov EAX 0x00432321"
   - AddAdd: sera un opcode del tipo "mov 0x00F23A4B 0x00C3FA48"
   - addReg: sera un opcode del tipo "mov 0x00F23A4B EAX"
   - regReg: sera un opcode del tipo "mov EBX EAX"
   - regi: sera para un opcode del tipo "jmp EAX"
   - addr: sera para un opcode del tipo "jmp 0x00F23A4B"
*)

Inductive hold:Set:=
   | regAdd32 : register32 -> Mem -> hold
   | addAdd32 : Mem -> Mem -> hold
   | addReg32 : Mem -> register32 -> hold
   | regReg32 : register32 -> register32 -> hold
   | regVal32 : register32 -> nat -> hold
   | addVal32 : Mem -> nat -> hold
   | regi32 : register32 -> hold
   | addr32 : Mem -> hold
   | regAdd16 : register16 -> Mem -> hold
   | addAdd16 : Mem -> Mem -> hold
   | addReg16 : Mem -> register16 -> hold
   | regReg16 : register16 -> register16 -> hold
   | regVal16 : register16 -> nat -> hold
   | addVal16 : Mem -> nat -> hold
   | regi16 : register16 -> hold
   | addr16 : Mem -> hold
   | regAdd8 : register8 -> Mem -> hold
   | addAdd8 : Mem -> Mem -> hold
   | addReg8 : Mem -> register8 -> hold
   | regReg8 : register8 -> register8 -> hold
   | regVal8 : register8 -> nat -> hold
   | addVal8 : Mem -> nat -> hold
   | regi8 : register8 -> hold
   | addr8 : Mem -> hold.


Inductive function:Set :=
   | strcpy : function
   | strcpyA : function
   | strcpyW : function
   | wcscpy : function
   | _tcscpy : function
   | _mbscpy : function
   | StrCpy : function
   | StrCpyA : function
   | StrCpyW : function
   | lstrcpy : function
   | lstrcpyA : function
   | lstrcpyW : function
   | _tccpy : function
   | _mbccpy : function
   | _ftcscpy : function
   | strncpy : function
   | wcsncpy : function
   | _tcsncpy : function
   | _mbsncpy : function
   | _mbsnbcpy : function
   | StrCpyN : function
   | StrCpyNA : function
   | StrCpyNW : function
   | StrNCpy : function
   | strcpynA : function
   | StrNCpyA : function
   | StrNCpyW : function
   | lstrcpyn : function
   | lstrcpynA : function
   | lstrcpynW : function
   | strcat : function
   | strcatA : function
   | strcatW : function
   | wcscat : function
   | _tcscat : function
   | _mbscat : function
   | StrCat : function
   | StrCatA : function
   | StrCatW : function
   | lstrcat : function
   | lstrcatA : function
   | lstrcatW : function
   | StrCatBuff : function
   | StrCatBuffA : function
   | StrCatBuffW : function
   | StrCatChainW : function
   | _tccat : function
   | _mbccat : function
   | _ftcscat : function
   | strncat : function
   | wcsncat : function
   | _tcsncat : function
   | _mbsncat : function
   | _mbsnbcat : function
   | StrCatN : function
   | StrCatNA : function
   | StrCatNW : function
   | StrNCat : function
   | StrNCatA : function
   | StrNCatW : function
   | lstrncat : function
   | lstrcatnA : function
   | lstrcatnW : function
   | lstrcatn : function
   | sprintfW : function
   | sprintfA : function
   | wsprintf : function
   | wsprintfW : function
   | wsprintfA : function
   | sprintf : function
   | swprintf : function
   | _stprintf : function
   | wvsprintf : function
   | wvsprintfA : function
   | wvsprintfW : function
   | vsprintf : function
   | _vstprintf : function
   | vswprintf : function
   | _fstrncpy : function
   | _fstrncat : function
   | gets : function
   | _getts : function
   | _gettws : function
   | IsBadWritePtr : function
   | IsBadHugeWritePtr : function
   | IsBadReadPtr : function
   | IsBadHugeReadPtr : function
   | IsBadCodePtr : function
   | IsBadStringPtr : function
   | memcpy : function
   | RtlCopyMemory : function
   | CopyMemory : function
   | wmemcpy : function
   | generic : function.

(**
Definimos los opcodes con los cuales vamos a trabajar. Escogemos estos ops, porque son
los mas utilizados segun el paper "fingerprinting malicious code through statistical 
opcode analysis"
*)

Inductive opcode:Set :=
   | mov: hold -> opcode
   | push: hold -> opcode
   | call: function -> opcode
   | pop: hold -> opcode
   | cmp: hold -> opcode
   | jz: hold -> opcode
   | lea: hold -> opcode
   | test: hold -> opcode
   | jmp: hold -> opcode
   | add: hold -> opcode
   | jnz: hold -> opcode
   | retn: opcode
   | xor: hold -> opcode
   | and: hold -> opcode.



(**
Creamos una funcion para que nos devuelva un booleano en una comparacion entre
enteros cuando estos son iguales. Y otra funcion similar que nos va a ayudar a
la hora de hacer las demostraciones.
*)

Definition natCompB (a b:nat):bool :=
   match (nat_compare a b) with 
      | Eq => true 
      | Lt => false 
      | Gt => false
   end.

Definition natCompBAux (a b:nat):bool :=
   if natCompB a b
      then true
      else false.

Theorem natCompBT1: forall (n:nat),
    natCompB n n = true.
Proof.
   intros.
   induction n.
   compute.
   reflexivity.
   unfold natCompB.
   rewrite nat_compare_S.
   unfold natCompB in IHn.
   apply IHn.
Qed.

Theorem natCompBT2: forall (n m:nat),
    natCompB n m = natCompB m n.
Proof.
   intro n; induction n as [| n' IHn'];
   intro m; destruct m as [| m'];
   simpl; try (rewrite IHn'); trivial. 
   unfold natCompB;rewrite nat_compare_S.
   symmetry.
   rewrite nat_compare_S.
   unfold natCompB in IHn'.
   symmetry.
   apply IHn'.
Qed.

Theorem natCompBAuxT1: forall (n:nat),
    natCompBAux n n = true.
Proof.
   intros.
   induction n.
   compute.
   reflexivity.
   unfold natCompBAux.
   unfold natCompB.
   rewrite nat_compare_S.
   unfold natCompBAux in IHn.
   unfold natCompB in IHn.
   apply IHn.
Qed.

Theorem natCompBAuxT2: forall (n m:nat),
    natCompBAux n m = natCompBAux m n.
Proof.
   intro n; induction n as [| n' IHn'];
   intro m; destruct m as [| m'];
   simpl; try (rewrite IHn'); trivial. 
   unfold natCompBAux; unfold natCompB; rewrite nat_compare_S.
   symmetry.
   rewrite nat_compare_S.
   unfold natCompBAux in IHn'; unfold natCompB in IHn'.
   symmetry.
   apply IHn'.
Qed.

(**
Creamos una funcion para comparar los registros. Y otra que como
en el caso anterior nos va a ayudar con las demostraciones.
*)

Definition regCompare32 (r1 r2:register32): bool :=
   match r1 with
      | eax => match r2 with
            | eax => true
            | _ => false
         end
      | eax_plus x => match r2 with
            | eax_plus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | eax_minus x => match r2 with
            | eax_minus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | ebx => match r2 with
            | ebx => true
            | _ => false
         end
      | ebx_plus x => match r2 with
            | ebx_plus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | ebx_minus x => match r2 with
            | ebx_minus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | ecx => match r2 with
            | ecx => true
            | _ => false
         end
      | ecx_plus x => match r2 with
            | ecx_plus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | ecx_minus x => match r2 with
            | ecx_minus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | edx => match r2 with
            | edx => true
            | _ => false
         end
      | edx_plus x => match r2 with
            | edx_plus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | edx_minus x => match r2 with
            | edx_minus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | esi => match r2 with
            | esi => true
            | _ => false
         end
      | esi_plus x => match r2 with
            | esi_plus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | esi_minus x => match r2 with
            | esi_minus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | edi => match r2 with
            | edi => true
            | _ => false
         end
      | edi_plus x => match r2 with
            | edi_plus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | edi_minus x => match r2 with
            | edi_minus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | ebp => match r2 with
            | ebp => true
            | _ => false
         end
      | ebp_plus x => match r2 with
            | ebp_plus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | ebp_minus x => match r2 with
            | ebp_minus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | esp => match r2 with
            | esp => true
            | _ => false
         end
      | esp_plus x => match r2 with
            | esp_plus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | esp_minus x => match r2 with
            | esp_minus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | eip => match r2 with
            | eip => true
            | _ => false
         end
      | cs => match r2 with
            | cs => true
            | _ => false
         end
      | ds => match r2 with
            | ds => true
            | _ => false
         end
      | ss => match r2 with
            | ss => true
            | _ => false
         end
      | es => match r2 with
            | es => true
            | _ => false
         end
      | fs => match r2 with
            | fs => true
            | _ => false
         end
      | gs => match r2 with
            | gs => true
            | _ => false
         end
   end.

Definition regCompare32Aux (r1 r2:register32): bool :=
   if regCompare32 r1 r2
      then true
      else false.

Theorem regCompare32T1: forall (r:register32),
    regCompare32 r r = true.
Proof.
   intros.
   induction r; simpl; auto;
   rewrite natCompBT1; reflexivity.
Qed.

Theorem regCompare32T2: forall (r1 r2:register32),
    regCompare32 r1 r2 = regCompare32 r2 r1.
Proof.
   intros.
   induction r1; elim r2; intros; simpl; auto;
   rewrite natCompBT2; reflexivity.
Qed.

Theorem regCompare32AuxT1: forall (r:register32),
    regCompare32Aux r r = true.
Proof.
   intros.
   induction r; simpl; auto;
   unfold regCompare32Aux; rewrite regCompare32T1; reflexivity.
Qed.

Theorem regCompare32AuxT2: forall (r1 r2:register32),
    regCompare32Aux r1 r2 = regCompare32Aux r2 r1.
Proof.
   intros.
   induction r1; elim r2; intros; simpl; auto;
   unfold regCompare32Aux; rewrite regCompare32T2; reflexivity.
Qed.

(**
Las mismas pruebas y funciones para los registros de 16 bits.
*)

Definition regCompare16 (r1 r2:register16): bool :=
   match r1 with
      | Ax => match r2 with
            | Ax => true
            | _ => false
         end
      | Bx => match r2 with
            | Bx => true
            | _ => false
         end
      | Cx => match r2 with
            | Cx => true
            | _ => false
         end
      | Dx => match r2 with
            | Dx => true
            | _ => false
         end
      | SI => match r2 with
            | SI => true
            | _ => false
         end
      | DI => match r2 with
            | DI => true
            | _ => false
         end
      | BP => match r2 with
            | BP => true
            | _ => false
         end
      | BP_plus x => match r2 with
            | BP_plus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | BP_minus x => match r2 with
            | BP_minus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | SP => match r2 with
            | SP => true
            | _ => false
         end
      | SP_plus x => match r2 with
            | SP_plus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | SP_minus x => match r2 with
            | SP_minus y => if (natCompB x y)
                  then true
                  else false
            | _ => false
         end
      | IP => match r2 with
            | IP => true
            | _ => false
         end
   end.

Definition regCompare16Aux (r1 r2:register16): bool :=
   if regCompare16 r1 r2
      then true
      else false.

Theorem regCompare16T1: forall (r:register16),
    regCompare16 r r = true.
Proof.
   intros.
   induction r; simpl;auto;
   rewrite natCompBT1; reflexivity.
Qed.

Theorem regCompare16T2: forall (r1 r2:register16),
    regCompare16 r1 r2 = regCompare16 r2 r1.
Proof.
   intros.
   induction r1; elim r2; intros; simpl; auto;
   rewrite natCompBT2; reflexivity.
Qed.

Theorem regCompare16AuxT1: forall (r:register16),
    regCompare16Aux r r = true.
Proof.
   intros.
   induction r; simpl;
   unfold regCompare16Aux; rewrite regCompare16T1; reflexivity.
Qed.

Theorem regCompare16AuxT2: forall (r1 r2:register16),
    regCompare16Aux r1 r2 = regCompare16Aux r2 r1.
Proof.
   intros.
   induction r1; elim r2; intros; simpl; auto;
   unfold regCompare16Aux; rewrite regCompare16T2; reflexivity.
Qed.

(**
Lo mismo pero para los registros de 8 bits.
*)

Definition regCompare8 (r1 r2:register8): bool :=
   match r1 with
      | AH => match r2 with
            | AH => true
            | _ => false
         end
      | AL => match r2 with
            | AL => true
            | _ => false
         end
      | BH => match r2 with
            | BH => true
            | _ => false
         end
      | BL => match r2 with
            | BL => true
            | _ => false
         end
      | CH => match r2 with
            | CH => true
            | _ => false
         end
      | CL => match r2 with
            | CL => true
            | _ => false
         end
      | DH => match r2 with
            | DH => true
            | _ => false
         end
      | DL => match r2 with
            | DL => true
            | _ => false
         end
   end.

Definition regCompare8Aux (r1 r2:register8): bool :=
   if regCompare8 r1 r2
      then true
      else false.

Theorem regCompare8T1: forall (r:register8),
    regCompare8 r r = true.
Proof.
   intros.
   induction r; simpl; reflexivity.
Qed.

Theorem regCompare8T2: forall (r1 r2:register8),
    regCompare8 r1 r2 = regCompare8 r2 r1.
Proof.
   intros.
   induction r1; elim r2; simpl; auto.
Qed.

Theorem regCompare8AuxT1: forall (r:register8),
    regCompare8Aux r r = true.
Proof.
   intros.
   induction r; simpl; reflexivity.
Qed.

Theorem regCompare8AuxT2: forall (r1 r2:register8),
    regCompare8Aux r1 r2 = regCompare8Aux r2 r1.
Proof.
   intros.
   induction r1; elim r2; simpl; auto.
Qed.

(**
Ahora creamos una funcion para compara los contenidos de los opcodes.
*)

Definition containCompare (c1 c2:hold):bool :=
   match c1 with
      | regAdd32 a b => match c2 with
            | regAdd32 x y => if (regCompare32 a x)
               then true
               else false
            | _ => false
         end
      | addAdd32 a b => match c2 with
            | addAdd32 x y => true
            | _ => false
         end
      | addReg32 a b => match c2 with
            | addReg32 x y =>regCompare32 b y
            | _ => false
         end
   
      | regReg32 a b => match c2 with
            | regReg32 x y => if (regCompare32 a x)
               then (regCompare32Aux b y)
               else false
            | _ => false
         end
      | regVal32 a b => match c2 with
            | regVal32 x y => if (regCompare32 a x)
               then if (natCompB b y)
                  then true
                  else false
               else false
            | _ => false
         end
      | addVal32 a b => match c2 with
            | addVal32 x y => if (natCompB b y)
                  then true
                  else false
            | _ => false
         end
      | regi32 a => match c2 with
            | regi32 x => if (regCompare32 a x)
               then true
               else false
            | _ => false
         end
      | addr32 a => match c2 with
            | addr32 x => true
            | _ => false
         end
      | regAdd16 a b => match c2 with
            | regAdd16 x y => if (regCompare16 a x)
               then true
               else false
            | _ => false
         end
      | addAdd16 a b => match c2 with
            | addAdd16 x y => true
            | _ => false
         end
      | addReg16 a b => match c2 with
            | addReg16 x y => regCompare16 b y 
            | _ => false
         end
      | regReg16 a b => match c2 with
            | regReg16 x y => if (regCompare16 a x)
               then (regCompare16Aux b y)
               else false
            | _ => false
         end
      | regVal16 a b => match c2 with
            | regVal16 x y => if (regCompare16 a x)
               then if (natCompB b y)
                  then true
                  else false
               else false
            | _ => false
         end
      | addVal16 a b => match c2 with
            | addVal16 x y => natCompB b y
            | _ => false
         end
      | regi16 a => match c2 with
            | regi16 x => if (regCompare16 a x)
               then true
               else false
            | _ => false
         end
      | addr16 a => match c2 with
            | addr16 x => true
            | _ => false
         end
      | regAdd8 a b => match c2 with
            | regAdd8 x y => regCompare8 a x
            | _ => false
         end
      | addAdd8 a b => match c2 with
            | addAdd8 x y => true
            | _ => false
         end
      | addReg8 a b => match c2 with
            | addReg8 x y => regCompare8 b y
            | _ => false
         end
      | regReg8 a b => match c2 with
            | regReg8 x y => if (regCompare8 a x)
               then (regCompare8Aux b y)
               else false
            | _ => false
         end
      | regVal8 a b => match c2 with
            | regVal8 x y => if (regCompare8 a x)
               then if (natCompB b y)
                  then true
                  else false
               else false
            | _ => false
         end
      | addVal8 a b => match c2 with
            | addVal8 x y => natCompB b y
            | _ => false
         end
      | regi8 a => match c2 with
            | regi8 x => if (regCompare8 a x)
               then true
               else false
            | _ => false
         end
      | addr8 a => match c2 with
            | addr8 x => true
            | _ => false
         end
   end.

Theorem containCompareT1: forall (c:hold),
    containCompare c c = true.
Proof.
   intros.
   induction c; simpl;auto.
   rewrite regCompare32T1.
   reflexivity.
   rewrite regCompare32T1.
   reflexivity.
   rewrite regCompare32T1.
   rewrite regCompare32AuxT1.
   reflexivity.
   rewrite natCompBT1.
   rewrite regCompare32T1.
   reflexivity.
   rewrite natCompBT1.
   reflexivity.
   rewrite regCompare32T1.
   reflexivity.
   rewrite regCompare16T1.
   reflexivity.
   rewrite regCompare16T1.
   reflexivity.
   rewrite regCompare16AuxT1.
   rewrite regCompare16T1.
   reflexivity.
   rewrite natCompBT1.
   rewrite regCompare16T1.
   reflexivity.
   rewrite natCompBT1.
   reflexivity.
   rewrite regCompare16T1.
   reflexivity.
   rewrite regCompare8T1.
   reflexivity.
   rewrite regCompare8T1.
   reflexivity.
   rewrite regCompare8T1.
   rewrite regCompare8AuxT1.
   reflexivity.
   rewrite natCompBT1.
   rewrite regCompare8T1.
   reflexivity.
   rewrite natCompBT1.
   reflexivity.
   rewrite regCompare8T1.
   reflexivity.
Qed.

Theorem containCompareT2: forall (c d:hold),
    containCompare c d = containCompare d c.
Proof.
   intros.
   induction c; elim d; intros; simpl; auto.
   rewrite regCompare32T2.
   reflexivity.
   rewrite regCompare32T2.
   reflexivity.
   rewrite regCompare32T2.
   rewrite regCompare32AuxT2.
   reflexivity.
   rewrite natCompBT2.
   rewrite regCompare32T2.
   reflexivity.
   rewrite natCompBT2.
   reflexivity.
   rewrite regCompare32T2.
   reflexivity.
   rewrite regCompare16T2.
   reflexivity.
   rewrite regCompare16T2.
   reflexivity.
   rewrite regCompare16T2.
   rewrite regCompare16AuxT2.
   reflexivity.
   rewrite natCompBT2.
   rewrite regCompare16T2.
   reflexivity.
   rewrite natCompBT2.
   reflexivity.
   rewrite regCompare16T2.
   reflexivity.
   rewrite regCompare8T2.
   reflexivity.
   rewrite regCompare8T2.
   reflexivity.
   rewrite regCompare8T2.
   rewrite regCompare8AuxT2.
   reflexivity.
   rewrite regCompare8T2.
   rewrite natCompBT2.
   reflexivity.
   rewrite natCompBT2.
   reflexivity.
   rewrite regCompare8T2.
   reflexivity.
Qed.

Definition funCompare (a b:function): bool :=
    match a with
       | strcpy => match b with
                      | strcpy => true
                      | _ => false
                   end
       | strcpyA => match b with
                       | strcpyA => true
                       | _ => false
                    end
       | strcpyW => match b with
                       | strcpyW => true
                       | _ => false
                    end
       | wcscpy => match b with
                      | wcscpy => true
                      | _ => false
                   end
       | _tcscpy => match b with
                      | _tcscpy => true
                      | _ => false
                   end
       | _mbscpy => match b with
                      | _mbscpy => true
                      | _ => false
                   end
       | StrCpy => match b with
                      | StrCpy => true
                      | _ => false
                   end
       | StrCpyA => match b with
                      | StrCpyA => true
                      | _ => false
                   end
       | StrCpyW => match b with
                      | StrCpyW => true
                      | _ => false
                   end
       | lstrcpy => match b with
                      | lstrcpy => true
                      | _ => false
                   end
       | lstrcpyA => match b with
                      | lstrcpyA => true
                      | _ => false
                   end
       | lstrcpyW => match b with
                      | lstrcpyW => true
                      | _ => false
                   end
       | _tccpy => match b with
                      | _tccpy => true
                      | _ => false
                   end
       | _mbccpy => match b with
                      | _mbccpy => true
                      | _ => false
                   end
       | _ftcscpy => match b with
                      | _ftcscpy => true
                      | _ => false
                   end
       | strncpy => match b with
                      | strncpy => true
                      | _ => false
                   end
       | wcsncpy => match b with
                      | wcsncpy => true
                      | _ => false
                   end
       | _tcsncpy => match b with
                      | _tcsncpy => true
                      | _ => false
                   end
       | _mbsncpy => match b with
                      | _mbsncpy => true
                      | _ => false
                   end
       | _mbsnbcpy => match b with
                      | _mbsnbcpy => true
                      | _ => false
                   end
       | StrCpyN => match b with
                      | StrCpyN => true
                      | _ => false
                   end
       | StrCpyNA => match b with
                      | StrCpyNA => true
                      | _ => false
                   end
       | StrCpyNW => match b with
                      | StrCpyNW => true
                      | _ => false
                   end
       | StrNCpy => match b with
                      | StrNCpy => true
                      | _ => false
                   end
       | strcpynA => match b with
                      | strcpynA => true
                      | _ => false
                   end
       | StrNCpyA => match b with
                      | StrNCpyA => true
                      | _ => false
                   end
       | StrNCpyW => match b with
                      | StrNCpyW => true
                      | _ => false
                   end
       | lstrcpyn => match b with
                      | lstrcpyn => true
                      | _ => false
                   end
       | lstrcpynA => match b with
                      | lstrcpynA => true
                      | _ => false
                   end
       | lstrcpynW => match b with
                      | lstrcpynW => true
                      | _ => false
                   end
       | strcat => match b with
                      | strcat => true
                      | _ => false
                   end
       | strcatA => match b with
                      | strcatA => true
                      | _ => false
                   end
       | strcatW => match b with
                      | strcatW => true
                      | _ => false
                   end
       | wcscat => match b with
                      | wcscat => true
                      | _ => false
                   end
       | _tcscat => match b with
                      | _tcscat => true
                      | _ => false
                   end
       | _mbscat => match b with
                      | _mbscat => true
                      | _ => false
                   end
       | StrCat => match b with
                      | StrCat => true
                      | _ => false
                   end
       | StrCatA => match b with
                      | StrCatA => true
                      | _ => false
                   end
       | StrCatW => match b with
                      | StrCatW => true
                      | _ => false
                   end
       | lstrcat => match b with
                      | lstrcat => true
                      | _ => false
                   end
       | lstrcatA => match b with
                      | lstrcatA => true
                      | _ => false
                   end
       | lstrcatW => match b with
                      | lstrcatW => true
                      | _ => false
                   end
       | StrCatBuff => match b with
                      | StrCatBuff => true
                      | _ => false
                   end
       | StrCatBuffA => match b with
                      | StrCatBuffA => true
                      | _ => false
                   end
       | StrCatBuffW => match b with
                      | StrCatBuffW => true
                      | _ => false
                   end
       | StrCatChainW => match b with
                      | StrCatChainW => true
                      | _ => false
                   end
       | _tccat => match b with
                      | _tccat => true
                      | _ => false
                   end
       | _mbccat => match b with
                      | _mbccat => true
                      | _ => false
                   end
       | _ftcscat => match b with
                      | _ftcscat => true
                      | _ => false
                   end
       | strncat => match b with
                      | strncat => true
                      | _ => false
                   end
       | wcsncat => match b with
                      | wcsncat => true
                      | _ => false
                   end
       | _tcsncat => match b with
                      | _tcsncat => true
                      | _ => false
                   end
       | _mbsncat => match b with
                      | _mbsncat => true
                      | _ => false
                   end
       | _mbsnbcat => match b with
                      | _mbsnbcat => true
                      | _ => false
                   end
       | StrCatN => match b with
                      | StrCatN => true
                      | _ => false
                   end
       | StrCatNA => match b with
                      | StrCatNA => true
                      | _ => false
                   end
       | StrCatNW => match b with
                      | StrCatNW => true
                      | _ => false
                   end
       | StrNCat => match b with
                      | StrNCat => true
                      | _ => false
                   end
       | StrNCatA => match b with
                      | StrNCatA => true
                      | _ => false
                   end
       | StrNCatW => match b with
                      | StrNCatW => true
                      | _ => false
                   end
       | lstrncat => match b with
                      | lstrncat => true
                      | _ => false
                   end
       | lstrcatnA => match b with
                      | lstrcatnA => true
                      | _ => false
                   end
       | lstrcatnW => match b with
                      | lstrcatnW => true
                      | _ => false
                   end
       | lstrcatn => match b with
                      | lstrcatn => true
                      | _ => false
                   end
       | sprintfW => match b with
                      | sprintfW => true
                      | _ => false
                   end
       | sprintfA => match b with
                      | sprintfA => true
                      | _ => false
                   end
       | wsprintf => match b with
                      | wsprintf => true
                      | _ => false
                   end
       | wsprintfW => match b with
                      | wsprintfW => true
                      | _ => false
                   end
       | wsprintfA => match b with
                      | wsprintfA => true
                      | _ => false
                   end
       | sprintf => match b with
                      | sprintf => true
                      | _ => false
                   end
       | swprintf => match b with
                      | swprintf => true
                      | _ => false
                   end
       | _stprintf => match b with
                      | _stprintf => true
                      | _ => false
                   end
       | wvsprintf => match b with
                      | wvsprintf => true
                      | _ => false
                   end
       | wvsprintfA => match b with
                      | wvsprintfA => true
                      | _ => false
                   end
       | wvsprintfW => match b with
                      | wvsprintfW => true
                      | _ => false
                   end
       | vsprintf => match b with
                      | vsprintf => true
                      | _ => false
                   end
       | _vstprintf => match b with
                      | _vstprintf => true
                      | _ => false
                   end
       | vswprintf => match b with
                      | vswprintf => true
                      | _ => false
                   end
       | _fstrncpy => match b with
                      | _fstrncpy => true
                      | _ => false
                   end
       | _fstrncat => match b with
                      | _fstrncat => true
                      | _ => false
                   end
       | gets => match b with
                      | gets => true
                      | _ => false
                   end
       | _getts => match b with
                      | _getts => true
                      | _ => false
                   end
       | _gettws => match b with
                      | _gettws => true
                      | _ => false
                   end
       | IsBadWritePtr => match b with
                      | IsBadWritePtr => true
                      | _ => false
                   end
       | IsBadHugeWritePtr => match b with
                      | IsBadHugeWritePtr => true
                      | _ => false
                   end
       | IsBadReadPtr => match b with
                      | IsBadReadPtr => true
                      | _ => false
                   end
       | IsBadHugeReadPtr => match b with
                      | IsBadHugeReadPtr => true
                      | _ => false
                   end
       | IsBadCodePtr => match b with
                      | IsBadCodePtr => true
                      | _ => false
                   end
       | IsBadStringPtr => match b with
                      | IsBadStringPtr => true
                      | _ => false
                   end
       | memcpy => match b with
                      | memcpy => true
                      | _ => false
                   end
       | RtlCopyMemory => match b with
                      | RtlCopyMemory => true
                      | _ => false
                   end 
       | CopyMemory  => match b with
                      | CopyMemory => true
                      | _ => false
                   end
       | wmemcpy  => match b with
                      | wmemcpy => true
                      | _ => false
                   end
       | generic => match b with
                      | generic => true
                      | _ => false
                   end
   end.

Theorem funCompareT1: forall (a b:function),
    funCompare a b = funCompare b a.
Proof.
   intros.
   induction a; elim b; simpl; auto.
Qed.


Theorem funCompareT2: forall (a:function),
    funCompare a a = true.
Proof.
   intro a.
   induction a; simpl; auto.
Qed.

(**
Y ahora hacemos la funcion que nos va a servir para comparar opcodes
y que se va a apoyar en las funciones creadas anteriormente.
*)

Definition opCompare (a b:opcode): bool :=
   match a with
      | mov x => match b with
            | mov y => containCompare x y
            | _ => false
         end
      | push x => match b with
            | push y => containCompare x y
            | _ => false
         end
      | call x => match b with
            | call y => funCompare x y
            | _ => false
         end
      | pop op => match b with
            | pop op' => containCompare op op'
            | _ => false
         end
      | retn => match b with
            | retn => true
            | _ => false
         end
       | cmp op => match b with
            | cmp op' => containCompare op op'
            | _ => false
         end
      | jz op => match b with
            | jz op' => containCompare op op'
            | _ => false
         end
      | lea op => match b with
            | lea op' => containCompare op op'
            | _ => false
         end
      | test op => match b with
            | test op' => containCompare op op'
            | _ => false
         end
      | jmp op => match b with
            | jmp op' => containCompare op op'
            | _ => false
         end
      | add op => match b with
            | add op' => containCompare op op'
            | _ => false
         end
      | jnz op => match b with
            | jnz op' => containCompare op op'
            | _ => false
         end
      | xor op => match b with
            | xor op' => containCompare op op'
            | _ => false
         end
      | and op => match b with
            | and op' => containCompare op op'
            | _ => false
         end
   end.

Theorem opCompareT1: forall (a:opcode),
   opCompare a a = true.
Proof.
   intros.
(*   induction a; simpl; rewrite containCompareT1; reflexivity.*)
   induction a; simpl; trivial.
   rewrite containCompareT1.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.
   rewrite funCompareT2.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.
   rewrite containCompareT1.
   reflexivity.   
Qed.

Theorem opCompareT2: forall (a b:opcode),
   opCompare a b = opCompare b a.
Proof.
   intros.
   elim a; elim b; simpl; intros; auto. 
   rewrite containCompareT2.
   reflexivity.
   rewrite containCompareT2.
   reflexivity.
   rewrite funCompareT1; reflexivity.
   rewrite containCompareT2.
   reflexivity.
   rewrite containCompareT2.
   reflexivity.
   rewrite containCompareT2.
   reflexivity.
   rewrite containCompareT2.
   reflexivity.
   rewrite containCompareT2.
   reflexivity.
   rewrite containCompareT2.
   reflexivity.
   rewrite containCompareT2.
   reflexivity.
   rewrite containCompareT2.
   reflexivity.
   rewrite containCompareT2.
   reflexivity.
   rewrite containCompareT2.
   reflexivity.
Qed.

(**
Esta funcion nos devolvera el opcode que corresponde con un el numero del estado
*)

Fixpoint getOpcode (n:nat)(l:list (nat*opcode)):option opcode :=
   match l with
      | nil => None
      | (a,b)::l' => if (natCompB n a)
          then Some b
          else getOpcode n l'
   end.
