(************************************************************************)
(* Pruebas para hacer el final                                          *)
(************************************************************************)

(** La seccion CTPL es donde va el grueso de las operaciones de logica
CTPL. Esta seccion es donde veremos las operaciones que vamos a hacer
con CTPL y donde hacemos las pruebas para comprobar que todo funcione
correctamente *)

(* begin hide *)
Section CTPL.

Require Import Arith.
Require Import List.

Require Import structures.
Require Import kripke.


(* end hide *)

(** * Estructuras.
Detallamos las formulas de estado que vamos a utilizar. Estas 
formulas solo funcionaran en el caso de que les pasemos codigos
operacionales, y son las siguientes:
   - True: Devolvera todos los OPs.
   - op: Sirve para definir el OP de la formula.
   - noS $\phi$: Operacion de negacion para los OPs.
   - andS $\phi$ $\psi$: Operacion de implicacion para los OPs.
   - orS $\phi$ $\psi$: Operacion de interseccion para los OPs.
   - impS $\phi$ $\psi$: Operacion de implicacion para los OPs.
*)

Inductive opStateFormula :=
   | TRUE : opStateFormula
   | op : opcode -> opStateFormula
   | noS : opStateFormula -> opStateFormula
   | andS : opStateFormula -> opStateFormula -> opStateFormula
   | orS : opStateFormula -> opStateFormula -> opStateFormula
   | impS : opStateFormula -> opStateFormula -> opStateFormula
   | coimpS : opStateFormula->opStateFormula ->opStateFormula.

(**

Si antes teniamos las formulas de estado, ahora tenemos las 
formulas de Camino, a estas formulas en vez de pasarles un
estado les pasaremos los caminos, en forma de arbol. Parte de
las operaciones son las mismas que en en las formulas de estado,
pero tenemos nuevas operaciones para los caminos. Vamos a detallarlas:
   - id: Sirve para definir una formula de estado.
   - noC $\phi$: operacion de negacion para los arboles CTPL.
   - andC $\phi$ $\psi$: operacion de implicacion para los arboles CTPL.
   - orC $\phi$ $\psi$: operacion de interseccion para los arboles CTPL.
   - impC $\phi$ $\psi$: operacion de impliciacion para los arboles CTPL.
   - EX $\phi$: Existe un camino cuyo siguiente estado es $\phi$.
   - EU $\phi$ $\psi$: Existe un camino que empieza por $\phi$ y mas tarde
tendra un $\psi$.
   - EF $\phi$: Existe un camino que en algun momento tendra un $\phi$.
   - EG $\phi$: Existe un camino que contendra siempre $\phi$.
   - AX $\phi$: Similar a EX pero para todos los caminos
   - AU $\phi$ $\psi$: Similar a EU pero para todos los caminos.
   - AF $\phi$: Similar a EF pero para todos los caminos.
   - AG $\phi$: Similar a EG pero para todos los caminos.
Los tipos que contienen el numero dos son las mismas versiones que las anteriores
pero en vez de estados estaremos siempre comprobando caminos.
*)


Inductive opType :=
   | id : opStateFormula -> opType
   | noC : opType -> opType
   | orC: opType -> opType -> opType
   | andC : opType -> opType -> opType
   | impC : opType -> opType -> opType
   | coimpC : opType -> opType -> opType
   | EX: opType -> opType
   | EX2: opStateFormula -> opType
   | EU : opType -> opType -> opType
   | EU2: opStateFormula -> opStateFormula -> opType
   | EG: opType -> opType
   | EG2: opStateFormula -> opType
   | EF: opType -> opType
   | EF2: opStateFormula -> opType
   | EFX : opType -> opType -> opType
   | EFX2: opStateFormula -> opStateFormula -> opType
   | AG: opType -> opType
   | AG2: opStateFormula -> opType
   | AX: opType -> opType
   | AX2: opStateFormula -> opType
   | AF: opType -> opType
   | AF2: opStateFormula -> opType
   | AU: opType -> opType -> opType
   | AU2: opStateFormula -> opStateFormula -> opType
   | AFX: opType -> opType -> opType
   | AFX2: opStateFormula -> opStateFormula -> opType.

(**
Definimos lo que va a ser un camino
*)
(*
Inductive Path (A:Type):Type :=
   | PNil : Path A
   | PCons : opcode -> Path A -> Path A.
*)
(**
Y la notacion que vamos a usar para mostrarlo. En este caso usaremos la 
notacion "-->" para expresar el paso de un elemento a otro y "$[$ $]$" para
denotar el final del camino
*)

Infix "-->" := (PCons opcode) (at level 60, right associativity) : list_scope.

Notation " [ ] " := (PNil opcode) (at level 30).

(*Definition CTPLPath: Type := Path opcode.

Definition CTPLTree: Type := list CTPLPath.*)

(** * Funciones utiles.
Creamos una funcion que comparara dos codigos operacionales y
nos devolvera un booleano
*)

(** ** Comparacion de caminos.
Definimos un par de funciones que nos sirvira
para comparar caminos.
*)

Definition pathCompare' (a b:opcode) :=
   match opCompare a b with
      | true => true
      | false => false
   end.


Fixpoint pathCompare (l1 l2:CTPLPath): bool :=
   match l1, l2 with
      | PNil, PNil => true
      | PNil, _ => false
      | _, PNil => false
      | PCons a l1', PCons b l2' =>  match opCompare a b with
            |true =>  pathCompare l1' l2'
            |false => false
         end
   end.

(**
En este teorema comprobamos que si tenemos un camino y
lo comparamos con un camino vacio, su comparacion
siempre devolvera false.
*)

Theorem pathCompareT1: forall (o:opcode)(l:CTPLPath),
   pathCompare (o-->l) ([]) = false.
Proof.
   intros.
   simpl.
   reflexivity.
Qed.

(**
Misma comprobacion que en el caso anterior, pero cambiando
el orden de las variables.
*)

Theorem pathCompareT2: forall (o:opcode)(l:CTPLPath),
   pathCompare ([]) (o-->l) = false.
Proof.
   intros.
   simpl.
   reflexivity.
Qed.

(**
Teorema que demuestra que si comparamos un camino con
si mismo siempre devolvera true.
*)

Theorem pathCompareT3: forall l1:CTPLPath,
   pathCompare l1 l1 = true.
Proof.
   intros.
   elim l1. 
   simpl.
   reflexivity.
   intros.
   simpl.
(*   elim o; simpl;intros;rewrite containCompareT1; assumption.*)
   rewrite opCompareT1.
   rewrite H.
   trivial.
Qed.

(**
Teorema que demuestra con conmutatividad de la funcion
de comparacion de caminos.
*)

Theorem pathCompareT4: forall l1 l2:CTPLPath,
   pathCompare l1 l2 = pathCompare l2 l1.
Proof.
   intro n; induction n as [| n' IHn'];
   intro m; destruct m as [| m'];
   simpl; try (rewrite IHn'); trivial. 
   rewrite opCompareT2. rewrite IHIHn'.
   reflexivity.
Qed.

(* begin hide *)
Hint Rewrite pathCompareT1 pathCompareT2 pathCompareT3: rules_chules.
(* end hide *)

(** ** Cortar caminos.
Creamos una funcion que nos ayude a cortar un camino dependiendo de otro.
*)

Fixpoint cutPath (l1 l2:CTPLPath):CTPLPath := 
   match l1, l2 with
      | PNil, PNil => []
      | PNil, l => []
      | l, PNil => []
      | PCons a l1', PCons b l2' => if opCompare a b 
                             then []
                             else b-->(cutPath l1 l2')
   end.

(** ** Camino contenido en otro.
Estas funciones comprobarán si el primer camino está contenido en el segundo.
*)

Fixpoint pathHoldFirst (l1 l2:CTPLPath): bool :=
   match l1, l2 with
      | PNil, PNil => true
      | PNil, _ => true
      | _, PNil => false
      | PCons a l1', PCons b l2' =>  match opCompare a b with
            |true =>  pathHoldFirst l1' l2'
            |false => false
         end
   end.

Fixpoint pathHold (l1 l2:CTPLPath):bool :=
   match l1, l2 with
      | PNil, PNil => true
      | PNil, _ => true
      | _, PNil => false
      | PCons a l1', PCons b l2' => if opCompare a b
                             then pathHoldFirst l1 l2
                             else pathHold l1 l2'
   end.

(** ** Comparacion de arboles.
Creamos una funcion que nos ayude a comparar dos arboles.
*)

Fixpoint CTPLTreeCompare (l1 l2:CTPLTree): bool :=
   match l1, l2 with
      | nil, nil => true
      | nil, _ => false
      | _, nil => false
      | a::l1', b::l2' => if pathCompare a b
         then CTPLTreeCompare l1' l2'
         else false
   end.

(**
Teorema que comprueba que si comparamos un arbol contra si
mismo siempre devolvera true.
*)

Theorem CTPLTreeCompareT1: forall (l:CTPLTree),
   CTPLTreeCompare l l = true.
Proof.
   intros.
   elim l.
   simpl.
   reflexivity.
   intros.
   simpl.
   autorewrite with rules_chules.
   assumption.
Qed.

(**
Teorema de la propiedad conmutativa de la funcion de
comparacion de arboles.
*)

Theorem CTPLTreeCompareT2: forall (l1 l2:CTPLTree),
   CTPLTreeCompare l1 l2 = CTPLTreeCompare l2 l1.
Proof.
   intro l1; induction l1 as [| l1' IHn'];
   intro l2; destruct l2 as [| m'];
   simpl; try (rewrite IHn'); trivial.
   rewrite pathCompareT4; rewrite IHIHn'.
   reflexivity.
Qed.

(* begin hide *)
Hint Rewrite CTPLTreeCompareT1: rules_chules.
(* end hide *)

(** ** Otras
*)

(**
Esta funcion corta el primer elemento de un camino, es util
para la funciones de X.
*)

Definition cutFirst (l:CTPLPath):CTPLPath :=
   match l with
      | PNil => PNil opcode
      | PCons a l' => l'
   end.

(**
Cardinalidad de un CTPLTree.
*)

Fixpoint cardinality (l:CTPLTree):nat :=
   match l with
      | nil => 0
      | a::l' => S (cardinality l')
   end.

(**
Esta funcion unira dos arboles. Es util para multiples operaciones
con los arboles.
*)

Fixpoint joinTree (l1 l2:CTPLTree): CTPLTree :=
   match l1 with
      | nil => l2
      | a::l1' => a::(joinTree l1' l2)
   end.

Definition addTree (l1:CTPLPath)(l2:CTPLTree): CTPLTree :=
   match l1 with
      | PNil => l2
      | _ => l1::l2
   end.

Fixpoint opInList (o:opcode)(l:list opcode): list opcode :=
   match l with
      | nil => nil
      | a::l' => if opCompare o a
                    then a::nil
                    else opInList o l'
   end.

(**
Teoremas relacionados con opInList
*)

Theorem in_eq : forall (a:opcode) (l:list opcode), 
   opInList a (a :: l) = a::nil.
Proof.
   intros.
   simpl.
   rewrite opCompareT1.
   trivial.
Qed.
(*
Theorem in_cons : forall (a b:opcode) (l:list opcode), 
   opInList b l = b::nil -> opInList b (a :: l) = b::nil.
Proof.
   intros.
   simpl.
   rewrite H.
   simpl.
   case a; case b; simpl; trivial.
   intros; induction f; destruct f0; simpl; trivial.
Qed.*)

Theorem in_nil : forall a:opcode, ~ (opInList a nil = a::nil).
Proof.
   unfold not; intros a H; inversion_clear H.
Qed.
(* No chuta por lo de los calls 
Lemma in_inv : forall (a b:opcode) (l:list opcode), 
   opInList b (a :: l) = b::nil -> a = b \/ opInList b l = b::nil.
Proof.
   intros.
   induction b; destruct a; simpl; trivial.
   left; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   left; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   left.
   induction f; elim f0.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   right;
   rewrite <- H;
   induction l; simpl; trivial.
   left; trivial.
Qed.*)

(* ¿está una lista incluida en otra? *)
Fixpoint include (l1 l2: list opcode): Prop :=
   match l1 with
      | nil => True
      | a::l => match opInList a l2 with
                   | nil => False
                   | l' => include l l2
                end
   end.

(**
Funciones para contar cual es la rama más larga del arbol
*)

Fixpoint max n m : bool :=
  match n, m with
    | O, _ => false
    | S n', O => true
    | S n', S m' => max n' m'
  end.
(*
Fixpoint opStateNumber(op: opStateFormula): nat :=
   match op with
      | TRUE => 1
      | op a => 1
      | noS a => S(opStateNumber a)
      | andS a b => if(max (opStateNumber a) (opStateNumber b))
                       then S(opStateNumber a)
                       else S(opStateNumber b)
      | orS a b => if(max (opStateNumber a) (opStateNumber b))
                       then S(opStateNumber a)
                       else S(opStateNumber b)
      | impS a b => if(max (opStateNumber a) (opStateNumber b))
                       then S(opStateNumber a)
                       else S(opStateNumber b)
   end.

Fixpoint opTypeNumber (op:opType): nat :=
   match op with
         | id a => S(opStateNumber a)
         | noC a => S(opTypeNumber a)
         | orC a b => if (max (opTypeNumber a) (opTypeNumber b))
                         then S(opTypeNumber a)
                         else S(opTypeNumber b)
         | andC a b => if (max (opTypeNumber a) (opTypeNumber b))
                         then S(opTypeNumber a)
                         else S(opTypeNumber b)
         | impC a b => if (max (opTypeNumber a) (opTypeNumber b))
                         then S(opTypeNumber a)
                         else S(opTypeNumber b)
         | EX a => S(opTypeNumber a)
         | EX2 a => S(opStateNumber a)
         | EU a b => if (max (opTypeNumber a) (opTypeNumber b))
                         then S(opTypeNumber a)
                         else S(opTypeNumber b)
         | EU2 a b => if (max (opStateNumber a) (opStateNumber b))
                         then S(opStateNumber a)
                         else S(opStateNumber b)
         | EFX a b => if (max (opTypeNumber a) (opTypeNumber b))
                         then S(opTypeNumber a)
                         else S(opTypeNumber b)
         | EFX2 a b => if (max (opStateNumber a) (opStateNumber b))
                         then S(opStateNumber a)
                         else S(opStateNumber b)
         | EG a => S(opTypeNumber a)
         | EG2 a => S(opStateNumber a)
         | EF a => S(opTypeNumber a)
         | EF2 a => S(opStateNumber a)
         | AG a => S(opTypeNumber a)
         | AG2 a => S(opStateNumber a)
         | AX a => S(opTypeNumber a)
         | AX2 a => S(opStateNumber a)
         | AF a => S(opTypeNumber a)
         | AF2 a => S(opStateNumber a)
         | AU a b => if (max (opTypeNumber a) (opTypeNumber b))
                         then S(opTypeNumber a)
                         else S(opTypeNumber b)
         | AU2 a b => if (max (opStateNumber a) (opStateNumber b))
                         then S(opStateNumber a)
                         else S(opStateNumber b)
         | AFX a b => if (max (opTypeNumber a) (opTypeNumber b))
                         then S(opTypeNumber a)
                         else S(opTypeNumber b)
         | AFX2 a b => if (max (opStateNumber a) (opStateNumber b))
                         then S(opStateNumber a)
                         else S(opStateNumber b)
   end.
*)

(*
Fixpoint neg3 (l1 l2:CTPLPath):option CTPLPath :=
   if pathCompare l1 l2
      then None
      else Some l1.

Theorem neg3T: forall l1:CTPLPath,
   neg3 l1 l1 = None.
Proof.
   intro.
   elim l1.
   simpl.
   reflexivity.
   intros.
   simpl.
   autorewrite with rules_chules.
   reflexivity.
Qed.

Hint Rewrite neg3T: rules_chules.
*)

(** * Operaciones con arboles
*)

(** ** Negacion.
En estas funciones implementa la operacion de negacion
entre dos arboles.
*)

Fixpoint neg' (l1:CTPLPath)(l2:CTPLTree): bool :=
   match l2 with
      | nil => false
      | b::l' => if pathCompare l1 b
           then true
           else neg' l1 l'
   end.

Fixpoint neg (l1 l2:CTPLTree):CTPLTree :=
   match l1, l2 with
      | nil, nil => nil
      | nil, _ => nil
      | _, nil => l1
      | a::l1', b::l2' =>if neg' a l2
            then neg l1' l2
            else a::(neg l1' l2)
   end.

(**
Demostramos que si cualquier arbol es comparado
con un arbol vacion la negacion es un arbol vacio.
*)

Theorem negT1: forall (l:CTPLTree),
   neg nil l = nil.
Proof.
   intros.
   induction l.
   simpl.
   reflexivity.
   intros.
   simpl.
   reflexivity.
Qed.

(**
La misma operacion pero al reves.
*)

Theorem negT2: forall (l:CTPLTree),
   neg l nil = l.
Proof.
   intros.
   induction l.
   simpl.
   reflexivity.
   intros.
   simpl.
   reflexivity.
Qed.

Axiom negT3: forall (a:CTPLPath)(l:CTPLTree),
   neg l (a::l) = nil.


(* Esto funcionaba poniendo un CompareCTPLTree en neg antes del "if neg'" *)
Theorem negT4: forall (l:CTPLTree),
   neg l l = nil.
Proof.
   intros.
   induction l.
   simpl.
   reflexivity.
   simpl.
   rewrite pathCompareT3.
   rewrite negT3.
   reflexivity.
Qed.

Axiom negT5: forall (l1 l2:CTPLTree),
   neg l2 (neg l2 l1) = l1.

Hint Rewrite negT1 negT2 negT3 negT4: rules_chules.

(**
Definimos cual va a ser la notacion que vamos a utilizar
para definir la operacion de negacion entre arboles. En
este caso la contrabarra "\".
*)

Notation "A \ B" := (neg A B) (at level 30).

(** ** Interseccion.
Definimos un par de funciones que efectuaran la operacion de
interseccion entre dos arboles (lo que viene a ser la operacion
and).
*)

Fixpoint interL2 (l1:CTPLTree)(path:CTPLPath): option CTPLPath :=
   match l1 with
      | nil => None
      | a::l1' => if pathCompare a path
                     then Some path
                     else interL2 l1' path
   end.



Fixpoint inter (l1 l2:CTPLTree):CTPLTree :=
   match l1 with
      | nil => nil
      | a::l1' => match (interL2 l2 a) with
                     | None => (inter l1' l2)
                     | Some b => (b)::(inter l1' l2)
                  end
   end.

(**
Demostracion de que si en la primera formula hacemos una comparacion en la cual
si existe un camino, va a devolverlo correctamente.
*)

(*Theorem InterL2_eq : forall (l1:CTPLTree)(p:CTPLPath), interL2 (p :: l1) p = p.
Proof.
   intros.
   simpl.
   rewrite pathCompareT3.
   reflexivity.
Qed.*)

Axiom simplInter: forall (a:CTPLPath)(l1 l2:CTPLTree),
match interL2 l2 a with | Some b => b :: inter l1 l2 | None => inter l1 l2 end = inter l2 (a :: l1).

(**
Demostracion que una operacion de interseccion le
pasamos un arbol vacio, la interseccion es un arbol
vacio.
*)

Theorem interT1: forall (a:CTPLTree),
   inter a nil = nil.
Proof.
   intros.
   elim a.
   simpl.
   reflexivity.
   intros.
   simpl.
   rewrite H.
   reflexivity.
Qed.

Theorem interT2: forall (a:CTPLTree),
   inter nil a = nil.
Proof.
   intros.
   elim a; simpl;[idtac| intros]; reflexivity.
Qed.

(**
Axioma para la conmutatividad de la funcion de interseccion.
*)

Theorem  interT3: forall (x y:CTPLTree),
    inter x y = inter y x.
Proof.
   intro l1; induction l1 as [| a' l1' ];
   intro l2; induction l2 as [| b' l2' ].
   simpl. trivial.
   simpl; rewrite interT1; trivial.
   simpl; rewrite interT1; trivial.
   generalize (b'::l2') as l2.
   intros.
   simpl.
   rewrite simplInter.
   reflexivity.
Qed.

(**
Axioma para la asociatividad de la funcion de interseccion.
*)
Axiom interT4: forall (x y z:CTPLTree),
   inter x (inter y z) = inter (inter x y) (inter x z).

(**
Axioma para la asociatividad de la funcion de interseccion.
*)

Axiom interT5: forall (x y z:CTPLTree), 
   inter x (inter y z) = inter (inter x y) (inter x z).

(**
Axioma por demostrar.
*)

Axiom interT6: forall (x y:CTPLTree),
   inter x (y \ x) = y.

(**
Axioma de que la interseccion de un arbol por si mismo es
el mismo arbol.
*)

Theorem interT7: forall (x:CTPLTree),
   inter x x = x.
Proof.
   intros.
   induction x.
   simpl; auto.
   simpl.
   rewrite pathCompareT3.
   rewrite interT3.
   simpl.
   rewrite IHx.
   admit.
Qed.

Axiom interT8: forall (c:CTPLPath)(x:CTPLTree),
   inter x (c :: x) = x.

Axiom interT9: forall (l1 l2 l:CTPLTree),
inter l1 (neg l (inter (neg l l1) (neg l l2))) = l1.

Axiom interT10: forall (l1 l2 l:CTPLTree),
neg l (inter (neg l l1) (neg l (inter l1 l2))) = l1.

Hint Rewrite interT1 interT2 interT3 interT4 interT5 interT6 interT7: rules_chules.

(**
Definimos la notacion que vamos a utilizar para la operacion de
interseccion entre dos arboles.
*)

Notation "A <--> B" := (inter A B) (at level 30, right associativity).

(** ** Operacion X.
Aqui definimos las tres funciones que van a realizar la operacion de X.
*)

(**
Definimos una nueva comparativa porque en este caso cuando no se acabe la primera
cadena, como todo ha sido correcto hasta el momento tiene que devolver true.
*)

Fixpoint pathCompareXC (l1 l2:CTPLPath): bool :=
   match l1, l2 with
      | PNil, PNil => true
      | PNil, _ => true
      | _, PNil => false
      | PCons a l1', PCons b l2' =>  if opCompare a b
                                        then pathCompareXC l1' l2'
                                        else false
   end.

Fixpoint AuxOperationXC (l1:CTPLTree)(l2:CTPLPath): CTPLTree :=
    match l1 with
       | nil => nil
       | a::l1' => if pathCompareXC a l2
                      then l2::(AuxOperationXC l1' l2)
                      else (AuxOperationXC l1' l2)
   end.

Theorem AuxOperationXCT1: forall (l:CTPLPath),
   AuxOperationXC nil l = nil.
Proof.
   intro l; induction l.
   simpl; trivial.
   simpl; trivial.
Qed.

Fixpoint operationXC (l1 l2:CTPLTree): CTPLTree :=
   match l2 with
      | nil => nil
      | a::l2' => match AuxOperationXC l1 (cutFirst a) with
                     | nil =>(operationXC l1 l2')
                     | l => joinTree (a::nil) (operationXC l1 l2')
                  end
   end.

Theorem operationXCT1: forall (l:CTPLTree),
   operationXC nil l = nil.
Proof.
   intro l; elim l.
   simpl.
   trivial.
   intros.
   simpl.
   rewrite H.
   trivial.
Qed.

Theorem operationXCT2: forall (l:CTPLTree),
   operationXC l nil = nil.
Proof.
   intro l; induction l.
   simpl; trivial.
   simpl;trivial.
Qed.

(** ** Operacion U.
Definimos las funciones que van a realizar la operacion de U con dos
arboles.
*)
(*
Fixpoint operationU2C'' (l1 l2:CTPLPath){struct l2}: bool :=
   match l1, l2 with
      | PNil, PNil => true
      | _, PNil => false
      | PNil, _ => true
      | PCons a l1', PCons b l2' => if opCompare a b 
                                       then operationU2C'' l1' l2'
                                       else false
   end.

Fixpoint operationU2C' (l1:CTPLPath)(l2:CTPLTree):bool :=
   match l2 with 
      | nil => false
      | a::l2' => if operationU2C'' l1 a
                             then true
                             else operationU2C' l1 l2'
   end.


Fixpoint operationU2C (l1 l2:CTPLTree):CTPLTree :=
   match l1 with 
      | nil => nil
      | a::l1' => if operationU2C' a l2
                             then a::(operationU2C l1' l2)
                             else operationU2C l1' l2
   end.

Theorem operationU2CT1: forall (l:CTPLTree),
   operationU2C l nil = nil.
Proof.
   intro l; induction l; simpl; trivial.
Qed.

Hint Rewrite operationU2CT1: rules_chules.

Fixpoint operationU1C' (l1 l2:CTPLPath):bool :=
   match l2 with
      | PNil => false
      | PCons a l2' => pathCompare l1 l2'
  end.

Fixpoint operationU1C (l1: CTPLTree)(l2:CTPLPath){struct l1}: CTPLTree :=
   match l1 with
      | nil => nil
      | a::l1' => if operationU1C' a l2
                     then a::(operationU1C l1' l2)
                     else operationU1C l1' l2
   end.

Fixpoint operationUC (l1 l2:CTPLTree):CTPLTree :=
   match l2 with
      | nil => nil
      | a::l2' => match operationU1C l1 a with
                     | nil => operationUC l1 l2'
                     | l => a::operationUC l1 l2'
                  end
   end.*)

Fixpoint operationUC2 (l1 l2:CTPLPath):CTPLPath :=
   match l2 with
      | PNil => []
      | PCons a l => if pathHold l1 l2
               then cutPath l1 l2
               else operationUC2 l1 l
   end.

Fixpoint operationUC1 (lfirst lU2 l:CTPLPath): CTPLPath :=
   match lfirst with
      | PNil => []
      | PCons a lf' => if pathHold (a-->lf') lU2
                     then l
                     else operationUC1 lf' lU2 l
   end.

Definition operationUCPath (lf ll l:CTPLPath):CTPLPath :=
   operationUC1 lf (operationUC2 ll l) l.

Fixpoint operationUCPathJuas2 (l1 l2:CTPLPath): CTPLPath :=
   match l2 with
      | PNil => []
      | PCons a l => if pathHoldFirst l1 l2
                        then l2
                        else operationUCPathJuas2 l1 l
   end.


Definition operationUCPathJuas (l1 l2 l:CTPLPath): CTPLPath :=
   operationUCPathJuas2 l2 (operationUCPath l1 l2 l).

Fixpoint operationUC'' (lf ll:CTPLPath)(l:CTPLTree):CTPLTree :=
   match l with
      | nil => nil
      | a::l' => match operationUCPathJuas lf ll a with
                    | PNil => operationUC'' lf ll l'
                    | path => path::(operationUC'' lf ll l')
                 end
   end.

Fixpoint operationUC' (lf:CTPLPath) (ll l:CTPLTree):CTPLTree :=
   match ll with
      | nil => nil
      | a::ll' => match operationUC'' lf a l with
                     | nil => operationUC' lf ll' l
                     | b::lf'' => b::operationUC' lf ll' l
                  end
   end.

Fixpoint operationUC (lf ll l:CTPLTree):CTPLTree :=
   match lf with
      | nil => nil
      | a::lf' => match operationUC' a ll l with
                     | nil => operationUC lf' ll l
                     | b::lf'' => b::operationUC lf' ll l
                  end
   end.

(* Comprobar este teorema
Theorem operationUCT1: forall (l:CTPLTree),
   operationUC l l nil = nil.
Proof.
   intro l; induction l.
   simpl; trivial.
   simpl.
Qed.

Hint Rewrite operationUCT1: rules_chules. *)

(** ** Operacion G.
Definimos las funciones que van a realizar la operacion de G con un arbol.
*)

Fixpoint operationGC' (l1 l2 lt:CTPLPath){struct l2}:bool :=
   match l1, l2 with
      | PNil, PNil => true
      | _, PNil => false
      | PNil, PCons b l2' => match lt with
                          | PNil => false
                          | PCons x lt' => if opCompare b x
                                              then operationGC' lt' l2' lt
                                              else false
                       end
      | PCons a l1', PCons b l2' => if opCompare a b
                             then operationGC' l1' l2' lt
                             else false
   end.

Fixpoint AuxOperationGC (l1:CTPLTree)(l2:CTPLPath): bool :=
    match l1 with
       | nil => false
       | a::l1' => if operationGC' a l2 a
                      then true
                      else AuxOperationGC l1' l2
   end.

Fixpoint operationGC (l1 l2:CTPLTree): CTPLTree :=
   match l2 with
      | nil => nil
      | a::l2' => if AuxOperationGC l1 a
                  then a::(operationGC l1 l2')
                  else operationGC l1 l2'
   end.

(** ** Operacion FX.
Definimos las funciones que van a realizar la operacion de FX con un arbol.
*)

Fixpoint operationFXC2 (l1 l2:CTPLPath):bool :=
   match l1, l2 with
      | PNil, PNil => true
      | PNil, _ => true
      | _ , PNil => false
      | PCons a l1', PCons b l2' => if opCompare a b
                                       then operationFXC2 l1' l2'
                                       else false
   end.

Fixpoint operationFXC1 (lf lFX2 l l2:CTPLPath){struct l}:CTPLPath :=
   match lf, l with
      | PNil, PNil => []
      | PNil, l' => if operationFXC2 lFX2 l'
                   then l
                   else []
      | _, PNil => []
      | PCons a l1', PCons b l' => if opCompare a b
                                       then operationFXC1 l1' lFX2 l' l2
                                       else operationFXC1 lf lFX2 l' l2
   end.

Fixpoint operationFXC'' (lf ll:CTPLPath)(l:CTPLTree):CTPLTree :=
   match l with
      | nil => nil
      | a::l' => match operationFXC1 lf ll a a with
                    | PNil => operationFXC'' lf ll l'
                    | path => path::(operationFXC'' lf ll l')
                 end
   end.

Fixpoint operationFXC' (lf:CTPLPath) (ll l:CTPLTree):CTPLTree :=
   match ll with
      | nil => nil
      | a::ll' => match operationFXC'' lf a l with
                     | nil => operationFXC' lf ll' l
                     | b::lf'' => b::operationFXC' lf ll' l
                  end
   end.

Fixpoint operationFXC (lf ll l:CTPLTree):CTPLTree :=
   match lf with
      | nil => nil
      | a::lf' => match operationFXC' a ll l with
                     | nil => operationFXC lf' ll l
                     | b::lf'' => b::operationFXC lf' ll l
                  end
   end.

(** * Operaciones con opcodes.
*)

(** ** Operacion In.
Funciones que se usaran para saber si la primera posicion de
un arbol coincide con uno o varios opcodes.
*)

Definition inTree' (o:opcode)(l:CTPLPath):option CTPLPath :=
   match l with
      | PNil => None
      | PCons a l' => if opCompare a o
         then Some l
         else None
   end.


Fixpoint inTree2 (o:opcode)(l:CTPLTree):CTPLTree :=
   match l with
      | nil => nil
      | a::l' => match (inTree' o a) with
             | None => (inTree2 o l')
             | Some a' => a'::(inTree2 o l')
          end
   end.

Theorem inTree2T1: forall (o:opcode),
   inTree2 o nil = nil.
Proof.
   intro o; induction o;
   simpl; trivial.
Qed.

Fixpoint inTree (l1:list opcode)(l2:CTPLTree): CTPLTree :=
   match l1 with
      | nil => nil
      | a::l1' => match (inTree2 a l2) with
                     | nil => inTree l1' l2
                     | l' => joinTree l' (inTree l1' l2)
                  end
   end. 

Theorem inTreeT1: forall (l:list opcode),
   inTree l nil = nil.
Proof.
   intro l; induction l.
   simpl.
   trivial.
   simpl.
   rewrite IHl.
   trivial.
Qed.

Hint Rewrite inTree2T1 inTreeT1: rules_chules.   

(** ** Negacion.
Aqui empezaremos las operaciones basicas con opcodes en vez
de utilizar arboles. Empezamos dos funciones que mostrar
*)

Fixpoint negState' (o:opcode)(l:list opcode): bool :=
   match l with
      | nil => false
      | a::l' => if opCompare o a
                    then true
                    else negState' o l'
   end.

Fixpoint negState (l1 l2:list opcode):list opcode :=
   match l1 with
      | nil => nil
      | a::l1'=> if negState' a l2
            then negState l1' l2
            else a::(negState l1' l2)
   end.

Theorem negStateT1: forall (l:list opcode),
   negState nil l = nil.
Proof.
   intros.
   simpl; trivial.
Qed.

Theorem negStateT2: forall (l:list opcode),
   negState l nil = l.
Proof.
   intro.
   induction l.
   simpl; trivial.
   simpl.
   rewrite IHl.
   trivial.
Qed.
   
Axiom negStateT4: forall (a:opcode)(l1:list opcode),
   negState l1 (a::l1) = nil.

Axiom negStateT5: forall (l1 l2:list opcode),
   negState l1 (negState l1 l2) = l2.

    

(** ** Interseccion.
Aqui tenemos las dos funciones que utilizamos para la operacion
de interseccion entre dos conjuntos de opcodes.
*)

Fixpoint interState' (o:opcode)(l:list opcode): bool :=
   match l with
      | nil => false
      | a::l' => if opCompare o a
                    then true
                    else interState' o l'
   end.

Fixpoint interState (l1 l2:list opcode):list opcode :=
   match l1 with
      | nil => nil
      | a::l1'=> if interState' a l2
            then a::(interState l1' l2)
            else interState l1' l2
   end.

Lemma interStateT1: forall (l:list opcode),
    interState l nil = nil.
Proof.
   intro l; induction l;
   simpl; trivial.
Qed.


Lemma interStateT2: forall (l:list opcode),
   interState nil l = nil.
Proof.
   intro l; induction l;
   simpl; trivial.
Qed.

Axiom interStateT3: forall (a:opcode)(l:list opcode),
   interState l (a::l) = l.

Lemma interStateT4: forall (l:list opcode),
   interState l l = l.
Proof.
   intros.
   elim l.
   simpl;trivial.
   intros.
   simpl.
   rewrite opCompareT1.
   rewrite interStateT3.
   trivial.
Qed. 

Axiom interStateT5: forall (a b:list opcode),
   interState a b = interState b a.

Axiom interStateT6: forall (l1 l2 l:list opcode),
interState l1 (negState l (interState (negState l l1) (negState l l2))) = l1.

Axiom interStateT7: forall (l1 l2 l:list opcode),
negState l (interState (negState l l1)(negState l (interState l1 l))) = l1.

(** ** Operacion X.
Aqui definimos las tres funciones que van a realizar la operacion de X.
*)

Definition second (l:CTPLPath): option opcode :=
   match l with
      | PNil => None
      | PCons a l' => match l' with
                    | PNil => None
                    | PCons b l'' => Some b
                 end
   end.

Fixpoint AuxOperationX (l1:list opcode)(l2:CTPLPath): CTPLTree :=
    match l1 with
       | nil => nil
       | a::l1' => match second l2 with
                      | None => AuxOperationX l1' l2
                      | Some b => if opCompare a b
                                     then (cutFirst l2)::(AuxOperationX l1' l2)
                                     else (AuxOperationX l1' l2)
                   end
   end.

Fixpoint operationX (l1:list opcode) (l2:CTPLTree): CTPLTree :=
   match l2 with
      | nil => nil
      | a::l2' => match AuxOperationX l1 a with
                     | nil =>(operationX l1 l2')
                     | l => joinTree l (operationX l1 l2')
                  end
   end.

(** ** Operacion U.
Aqui definimos las seis funciones que van a realizar la operacion de U.
*)

Fixpoint operationU1_3 (o:opcode)(o2:opcode)(l2:CTPLPath):bool :=
   match l2 with
      | PNil => false
      | PCons a l2' => if opCompare o2 a
                          then false
                          else if opCompare o a
                             then true
                             else operationU1_3 o o2 l2'
   end.

Fixpoint operationU1_2 (l1:list opcode)(o2: opcode)(l2:CTPLPath):bool :=
   match l1 with
      | nil => false
      | a::l1' => if operationU1_3 a o2 l2
                     then true
                     else operationU1_2 l1' o2 l2
   end.

Fixpoint operationU1_1 (l1:list opcode)(l2:list opcode)(l3:CTPLPath):bool :=
   match l2 with
      | nil => false
      | a::l2' => if operationU1_2 l1 a l3
                     then true
                     else operationU1_1 l1 l2' l3
   end.

Fixpoint operationU1 (l1:list opcode)(l2:list opcode)(l3:CTPLTree): CTPLTree :=
   match l3 with
      | nil => nil
      | a::l3' => if operationU1_1 l1 l2 a
                     then a::(operationU1 l1 l2 l3')
                     else (operationU1 l1 l2 l3')
   end.

Fixpoint isInPath (o:opcode) (l:CTPLPath): bool :=
   match l with
      | PNil => false
      | PCons a l' => if opCompare o a
                         then true
                         else isInPath o l'
  end.

Fixpoint operationU2'' (o:opcode) (l:CTPLPath): CTPLPath:=
   match l with
      | PNil => []
      | PCons a l' => if opCompare o a
                         then l
                         else operationU2'' o l'
   end.

Fixpoint operationU2' (l1:list opcode) (l2:CTPLPath): CTPLPath :=
   match l1 with
      | nil => []
      | a::l1' => match operationU2'' a l2 with
                     | PNil => operationU2' l1' l2
                     | l2' => l2'
                  end
   end.

Fixpoint operationU2 (o:list opcode) (l2:CTPLTree):CTPLTree :=
   match l2 with
      | nil => nil
      | a::l2' => match operationU2' o a with
                     | PNil => (operationU2 o l2')
                     | l => l::(operationU2 o l2')
                  end
   end.

Definition operationU (l1:list opcode)(l2:list opcode)(l3:CTPLTree):CTPLTree :=
   operationU2 l2 (operationU1 l1 l2 l3).

Theorem operationU2T1: forall (l:list opcode),
   operationU2 l nil = nil.
Proof.
   intro l; induction l; simpl; trivial.
Qed.
(*
Fixpoint operationU1' (o:opcode)(l:CTPLPath):CTPLPath :=
   match l with
      | PNil => []
      | PCons a l' => if opCompare o a
                         then l
                         else operationU1' o l'
   end.

Fixpoint operationU1 (o:opcode)(l:CTPLTree): CTPLTree :=
   match l with
      | nil => nil
      | a::l1' => match operationU1' o a with
                     | PNil => (operationU1 o l1')
                     | li => li::operationU1 o l1'
                  end
   end.

Fixpoint operationU (l1:list opcode) (l2:CTPLTree):CTPLTree :=
   match l1 with
      | nil => nil
      | a::l1' => match operationU1 a l2 with
                     | nil => operationU l1' l2
                     | l => joinTree l (operationU l1' l2)
                  end
   end.

Theorem operationUT1: forall (l:list opcode),
    operationU l nil = nil.
Proof.
   intro l; induction l; simpl; trivial.
Qed.

Hint Rewrite operationU2T1 operationUT1: rules_chules.
*)
(** ** Operacion G.
Aqui definimos las tres funciones que van a realizar la operacion de G.
*)

Fixpoint operationG' (o:opcode)(l:CTPLPath):bool :=
   match l with
      | PNil => true
      | PCons a l' => if opCompare o a
                         then operationG' o l'
                         else false
   end.

Fixpoint AuxOperationG (o:opcode)(l1:CTPLTree): CTPLTree :=
    match l1 with
       | nil => nil
       | a::l1' => if operationG' o a
                      then a::(AuxOperationG o l1')
                      else AuxOperationG o l1'
   end.

Fixpoint operationG (l1:list opcode)(l2:CTPLTree): CTPLTree :=
   match l1 with
      | nil => nil
      | a::l1' => match AuxOperationG a l2 with
                     | nil => operationG l1' l2
                     | l => joinTree l (operationG l1' l2)
                  end
   end.

Theorem operationGT1: forall (l:list opcode),
   operationG l nil = nil.
Proof.
   intro l; induction l; simpl; trivial.
Qed.

Hint Rewrite operationGT1: rules_chules.

(** ** Operacion FX.
Aqui definimos las tres funciones que van a realizar la operacion de FX.
*)

Fixpoint operationFX''' (o1 o2:opcode) (l:CTPLPath):CTPLPath :=
   match l with
      | PNil => []
      | PCons x PNil => []
      | PCons a l' => if opCompare a o1
                         then (match l' with
                                 | PNil => []
                                 | PCons b l'' => if opCompare b o2
                                                     then l
                                                     else operationFX''' o1 o2 l'
                              end)
                         else operationFX''' o1 o2 l'
   end.

Fixpoint operationFX'' (o:opcode) (l2:list opcode) (l:CTPLPath):CTPLTree:=
   match l2 with
      | nil => nil
      | a::l2' => match operationFX''' o a l with
                     | PNil => operationFX'' o l2' l
                     | b => b::(operationFX'' o l2' l)
                  end
   end.

Fixpoint operationFX' (l1 l2:list opcode) (l:CTPLPath):CTPLTree :=
   match l1 with
      | nil => nil
      | a::l1' => match operationFX'' a l2 l with
                     | nil => operationFX' l1' l2 l
                     | l' => joinTree l' (operationFX' l1' l2 l)
                  end
   end.

Fixpoint operationFX (l1 l2:list opcode) (l:CTPLTree):CTPLTree :=
   match l with
      | nil => nil
      | a::l' => match operationFX' l1 l2 a with
                     | nil => operationFX l1 l2 l'
                     | l => joinTree l (operationFX l1 l2 l')
                  end
   end.

(** * Operaciones CTPL.
En esta seccion definimos las operaciones que se van a ejecutar cada vez que
hagamos las formulas CTPL.
*)

(**
Empecemos con las fomulas de estado. En este caso son las formulas que van a realizar
operaciones con los opcodes.
*)

Fixpoint SatState (o:opStateFormula)(l:list opcode): list opcode :=
    match o with
       (* Sat(true) = S *)
       | TRUE => l
       | op a => opInList a l
       | noS a => negState l (SatState a l)
       | andS a b => interState (SatState a l)(SatState b l)
       | orS a b => negState l (interState (negState l (SatState a l))(negState l (SatState b l)))
       | impS a b => negState l (interState (SatState a l)(negState l (SatState b l)))
       | coimpS a b => interState (negState l (interState (SatState a l) (negState l (SatState b l)))) (negState l (interState (SatState b l) (negState l (SatState a l)))) 
   end.

(**
Ahora definimos las formulas que van a realizar las operaciones con arboles.
*)

(*
id   -> (* Sat(a) = { s ∈ S_list | a ∈ L(s) }, for any a ∈ AP *)
noC  -> (* Sat(¬Φ) = S \ Sat(Φ) *)
orC  -> (* Sat(Φ ∨ Ψ) =  S \ ((S \ Sat(Φ)) ∩ (S \ Sat(Ψ)) *)
andC -> (* Sat(Φ ∧ Ψ) = Sat(Φ) ∩ Sat(Ψ) *)
impC -> (* Sat(Φ → Ψ) = S  \ (Sat(Φ) ∩ (S \ Sat(Ψ))) *)
EX   -> (* Sat(∃Φ) = { s ∈ S | Post(s) ∩ Sat(Φ) ≠ ∅ } *)
EU   -> (* Sat(∃(Φ U Ψ))
            T = Sat(Ψ);
            while {s ∈ Sat(Φ) \ T | Post(s) ∩ T ≠ ∅} ≠ ∅ 
            do
               let s ∈ { s ∈ Sat(Φ) \ T | Post(s) ∩ T ≠ ∅};
               T :=  T ∪ {s};
            od;
            return T;
         *)
EG    -> (* Sat(∃✷ Φ) is the largest subset T of S, such that:
               (3) T ⊆ Sat(Φ) and 
               (4) s ∈ T implies Post(s) ∩ T ≠ ∅ *)
EF    -> (* EF Φ = EU TRUE Φ *)
AG    -> (* AG Φ = noC (EF (noC Φ)) *)
AX    -> (* AX Φ = noC (EX (noC Φ)) *)
AF    -> (* AF Φ = noC (EG (noC Φ)) *)
AU    -> (* AU Φ Ψ = andC (EU (noC Ψ) (andC (noC Φ)(noC Ψ))) (noC (EG (noC Ψ))) *)
*)

Fixpoint lo2CTPLTree (lo:list opcode):CTPLTree :=
   match lo with
      | nil => nil
      | a::l => (a-->[])::(lo2CTPLTree l)
   end.

Fixpoint Sat (o:opType)(lo:list opcode)(l:CTPLTree){struct o}: CTPLTree :=
      match o with
         | id a => inTree (SatState a lo) l
         | noC a => l \ (Sat a lo l)
         | orC a b => l \ ((l \ (Sat a lo l)) <--> (l \ (Sat b lo l)))
         | andC a b => (Sat a lo l) <--> (Sat b lo l)
         | impC a b => (l \ ((Sat a lo l) <--> (l \ (Sat b lo l))))
         | coimpC a b => inter (neg l (inter (Sat a lo l) (neg l (Sat b lo l)))) (neg l (inter (Sat b lo l) (neg l (Sat a lo l))))
         | EX a => operationXC (Sat a lo l) l
         | EX2 a => operationX (SatState a lo) l     
         | EU a b => operationUC (Sat a lo l) (Sat b lo l) l
         | EU2 a b => operationU (SatState a lo) (SatState b lo) l
         | EG a => operationGC (Sat a lo l) l
         | EG2 a => operationG (SatState a lo) l
         | EF a => (operationUC (lo2CTPLTree lo) (Sat a lo l) l)
         | EF2 a => operationU (SatState TRUE lo) (SatState a lo) l
         | EFX a b => operationFXC (Sat a lo l) (Sat b lo l) l
         | EFX2 a b => operationFX (SatState a lo) (SatState b lo) l
         | AG a => l \ (operationUC (lo2CTPLTree lo) (l \ (Sat a lo l)) l)
         | AG2 a => l \ operationU (SatState TRUE lo) (negState lo (SatState a lo)) l
         | AX a => l \ (operationXC (l \ (Sat a lo l)) l)
         | AX2 a => l \ (operationX (negState lo (SatState a lo)) l)
         | AF a => l \ (operationGC (l \ (Sat a lo l)) l)
         | AF2 a => l \ (operationG (negState lo (SatState a lo)) l)
         | AU a b => l \ (operationUC (l \ (Sat b lo l)) ((l \ (Sat a lo l)) <--> (l \ (Sat b lo l))) l) <--> (l \ (operationGC (l \ (Sat b lo l)) l))
         | AU2 a b => l \ (operationU (negState (SatState b lo) lo) (interState (negState (SatState a lo) lo) (negState (SatState b lo) lo)) l) <--> (l \ (operationG (negState (SatState b lo) lo) l))
         | AFX a b => l \ (operationFXC (l \ (Sat b lo l)) ((l \ (Sat a lo l)) <--> (l \ (Sat b lo l))) l) <--> (l \ (operationGC (l \ (Sat b lo l)) l))
         | AFX2 a b => l \(operationFX (negState (SatState b lo) lo) (interState (negState (SatState a lo) lo) (negState (SatState b lo) lo)) l) <--> (l \ (operationG (negState (SatState b lo) lo) l))
   end.

(**
Creamos una gramatica para que se llame practicamente de la misma forma
a las funciones que son con arboles o las que son con opcodes. Las
llamadas con arboles se llamaran normalmente mientras que las que
son con opcodes se llamaran con corchetes.
*)
(*Notation "| A |" := (cardinality A) (at level 30).*)

Notation "EX[ A ]" := (EX2 A) (at level 30).
Notation "EU( A , B )" := (EU A B) (at level 31).
Notation "EU[ A , B ]" := (EU2 A B) (at level 32).
Notation "EF[ A ]" := (EF2 A) (at level 33).
Notation "EG[ A ]" := (EG2 A) (at level 34).
Notation "EFX( A , B )" := (EFX A B) (at level 35).
Notation "EFX[ A , B ]" := (EFX2 A B) (at level 36).
Notation "AX[ A ]" := (AX2 A) (at level 37).
Notation "AU( A , B )" := (AU A B) (at level 38).
Notation "AU[ A , B ]" := (AU2 A B) (at level 39).
Notation "AF[ A ]" := (AF2 A) (at level 40).
Notation "AG[ A ]" := (AG2 A) (at level 41).
Notation "AFX( A , B )" := (AFX A B) (at level 42).
Notation "AFX[ A , B ]" := (AFX2 A B) (at level 43).
Notation "A 'andC' B" := (andC A B) (at level 80, right associativity).
Notation "[ A 'andS' B ]" := (andS A B) (at level 80, right associativity).
Notation "A 'orC' B" := (orC A B) (at level 80, right associativity).
Notation "[ A 'orS' B ]" := (orS A B) (at level 80, right associativity).
Notation "A 'imp' B" := (impC A B) (at level 80, right associativity).
Notation "A 'impS' B" := (impS A B) (at level 80, right associativity).
Notation "'no' A " := (noC A) (at level 10).
Notation "'noS' A " := (noS A) (at level 1).
Notation "| A |" := (op A) (at level 30).


(**
Creamos una funcion para que sea posible validar teoremas, con facilidad.
*)

Definition validate (o:opType)(lo:list opcode)(l:CTPLTree):Prop :=
   match Sat o lo l with
       | nil => False
       | _ => True
   end.



(** * Pruebas simples.
Aqui realizamos algunas pruebas para comprobar que las formulas funcionan. Estas pruebas
son simplemente eso, pruebas. No son teoremas ni prueban nada en particular, solo
que con estas pruebas vemos que las formulas funcionan en determinados casos.
*)

Fixpoint sonIgualesCaminos_2 (l1 l2:CTPLPath):bool :=
   match l1, l2 with
      | PNil, PNil => true
      | PNil, _ => false
      | _, PNil => false
      | PCons a l1', PCons b l2' => if opCompare a b
                                       then sonIgualesCaminos_2 l1' l2'
                                       else false
   end.

Fixpoint sonIgualesCaminos (l1 l2:CTPLPath):bool :=
   match l1, l2 with
      | PNil, PNil => false
      | PNil, _ => false
      | _, PNil => false
      | PCons a l1', PCons b l2' => if sonIgualesCaminos_2 l1 l2
                                       then true
                                       else sonIgualesCaminos l1 l2'
   end.

Fixpoint buscarCaminosCompletos_2 (l1:CTPLPath)(l2:CTPLTree):CTPLPath :=
   match l2 with
      | nil => PNil opcode
      | a::l2' => if sonIgualesCaminos l1 a
                     then a
                     else (buscarCaminosCompletos_2 l1 l2')
   end.

Fixpoint buscarCaminosCompletos (l1 l2:CTPLTree):CTPLTree :=
   match l1 with
      | nil => nil
      | a::l1' => match buscarCaminosCompletos_2 a l2 with
                     | PNil => buscarCaminosCompletos l1' l2
                     | b => b::(buscarCaminosCompletos l1' l2)
                  end
   end.

Fixpoint comprobarIguales_2 (l1:CTPLPath)(l2:CTPLTree):bool :=
   match l2 with
      | nil => false
      | a::l2' => if pathCompare l1 a
                   then true
                   else comprobarIguales_2 l1 l2'
   end.

Fixpoint comprobarIguales (l:CTPLTree):CTPLTree :=
   match l with
      | nil => nil
      | a::l' => if comprobarIguales_2 a l'
                    then (comprobarIguales l')
                    else a::(comprobarIguales l')
   end.


Definition start2 (o:opType)(lo:list opcode)(l:CTPLTree):CTPLTree :=
   comprobarIguales (buscarCaminosCompletos (Sat o lo l) l).

Definition start (o:opType)(k:kripke): CTPLTree :=
   start2 o (opcodeList (L_List k)) (natToOpcode(searchPath (R_List k) 1 (getRet (L_List k)))(L_List k)).

Definition validateFormula (o:opType)(k:kripke): Prop :=
   match (Sat o (opcodeList (L_List k)) (natToOpcode(searchPath (R_List k) 1 (getRet (L_List k)))(L_List k))) with
      | nil => False
      | _ => True
   end.

(*
Theorem un: exists x:opcode,
   validateFormula (id(op x))(KripkeS (1::2::3::nil) ((1,2)::(1,3)::(2,3)::nil) ((1,mov)::(2,mov)::(3,retn)::nil)).
Proof.
   exists mov.
   compute.
   tauto.
Qed.

Eval compute in validateFormula (id(|call strcpyA|))(KripkeS (1::2::3::nil) ((1,2)::(1,3)::(2,3)::nil) ((1,call strcpyA)::(2,mov (regReg32 EAX EBX))::(3,retn)::nil)).

Eval compute in validateFormula (EF[|call strcpyA|])(KripkeS (1::2::3::4::5::nil) ((1,2)::(2,3)::(3,4)::(4,5)::nil) ((1,push (regi32 EBP))::(2,mov (regReg32 EBP ESP))::(3,call strcpyA)::(4,mov (regReg32 EAX EBX))::(5,retn)::nil)).


Eval compute in SatState (|mov (regVal32 EAX 5)|) (mov(regVal32 EAX 5)::push(regi32 EAX)::nil).

Eval compute in SatState ([(|mov (regVal32 EAX 5)|) orS (|push (regi32 EAX)|)]) (mov(regVal32 EAX 5)::push(regi32 EAX)::nil).

Eval compute in Sat (id (|mov (regVal32 EAX 5)|)) (mov(regVal32 EAX 5)::push(regVal32 EAX 1)::jmp(regi32 EAX)::nil) ((mov(regVal32 EAX 5)-->mov(regVal32 EAX 6)-->[])::(push(regVal32 EAX 1)-->push(regVal32 EAX 1)-->[])::(jmp(regi32 EAX)-->jmp(regi32 EAX)-->[])::nil).

Eval compute in start (AU[|call strcpyA|,|mov(addReg32 mem EAX)|]) (KripkeS (1::2::3::nil) ((1,2)::(1,3)::(2,3)::nil)
((1,(call strcpyA))::(2,mov(addReg32 mem EAX))::(3,retn)::nil)).

Eval compute in start (EG((id (|call strcpyA|)) and EX[|retn|])) (KripkeS (1::2::3::nil) ((1,2)::(1,3)::(2,3)::nil) ((1,(call strcpyA))::(2,mov(addReg32 mem EAX))::(3,retn)::nil)).

Eval compute in start (no((id(|call strcpyA|)) or id(|call gets|))) (KripkeS (1::2::3::nil) ((1,2)::(1,3)::(2,3)::nil) ((1,(call strcpyA))::(2,mov(addReg32 mem EAX))::(3,retn)::nil)).

Eval compute in start (AF[|call gets|])(KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::nil)
((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(9,11)::(10,11)::(11,12)::(11,19)::(12,13)::
(13,14)::(14,15)::(15,16)::(16,17)::(17,18)::(18,20)::(19,20)::(20,21)::nil)
((1,push (regi32 EBP))::(2,mov (regReg32 EBP ESP))::(3,mov (regReg32 EAX (EBP_plus 8)))::
(4,mov (regReg16 (BP_minus 28) Ax))::(5,mov (regVal32 (EBP_minus 19) 0))::(6,mov (regVal32 (EBP_minus 15) 0))::
(7,mov (regVal32 (EBP_minus 11) 0))::(8,mov (regVal32 (EBP_minus 9) 110))::(9,cmp (regVal32 (EBP_plus 12) 1))::
(10,mov (regAdd32 ESP mem))::(11,cmp (regVal32 (EBP_minus 28) 121))::(12,mov (regAdd32 ESP mem))::(13,lea (regReg32 EAX (EBP_minus 19)))::
(14,mov (regReg32 ESP EAX))::(15,call gets)::(16,lea (regReg32 EAX (EBP_minus 19)))::(17,mov (regReg32 (ESP_plus 4) EAX))::
(18,mov (regAdd32 ESP mem))::(19,mov (regAdd32 ESP mem))::(20,lea (regReg32 EAX (EBP_minus 19)))::(21,retn)::nil)). 


Eval compute in start ((EF[|mov (regAdd32 ECX mem)|]) and (EF[|call gets|]))(KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::nil)
((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(9,11)::(10,11)::(11,12)::(11,19)::(12,13)::
(13,14)::(14,15)::(15,16)::(16,17)::(17,18)::(18,20)::(19,20)::(20,21)::nil)
((1,push (regi32 EBP))::(2,mov (regReg32 EBP ESP))::(3,mov (regReg32 EAX (EBP_plus 8)))::
(4,mov (regReg16 (BP_minus 28) Ax))::(5,mov (regVal32 (EBP_minus 19) 0))::(6,mov (regVal32 (EBP_minus 15) 0))::
(7,mov (regVal32 (EBP_minus 11) 0))::(8,mov (regVal32 (EBP_minus 9) 110))::(9,cmp (regVal32 (EBP_plus 12) 1))::
(10,mov (regAdd32 ECX mem))::(11,cmp (regVal32 (EBP_minus 28) 121))::(12,mov (regAdd32 ESP mem))::(13,lea (regReg32 EAX (EBP_minus 19)))::
(14,mov (regReg32 ESP EAX))::(15,call gets)::(16,lea (regReg32 EAX (EBP_minus 19)))::(17,mov (regReg32 (ESP_plus 4) EAX))::
(18,mov (regAdd32 ESP mem))::(19,mov (regAdd32 ESP mem))::(20,lea (regReg32 EAX (EBP_minus 19)))::(21,retn)::nil)).

Eval compute in start (EF[|push (regi32 EBP)|])(KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::nil)
((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(9,11)::(10,11)::(11,12)::(11,19)::(12,13)::
(13,14)::(14,15)::(15,16)::(16,17)::(17,18)::(18,20)::(19,20)::(20,21)::nil)
((1,push (regi32 EBP))::(2,mov (regReg32 EBP ESP))::(3,mov (regReg32 EAX (EBP_plus 8)))::
(4,mov (regReg16 (BP_minus 28) Ax))::(5,mov (regVal32 (EBP_minus 19) 0))::(6,mov (regVal32 (EBP_minus 15) 0))::
(7,mov (regVal32 (EBP_minus 11) 0))::(8,mov (regVal32 (EBP_minus 9) 110))::(9,cmp (regVal32 (EBP_plus 12) 1))::
(10,mov (regAdd32 ECX mem))::(11,cmp (regVal32 (EBP_minus 28) 121))::(12,mov (regAdd32 ESP mem))::(13,lea (regReg32 EAX (EBP_minus 19)))::
(14,mov (regReg32 EBX EAX))::(15,call gets)::(16,lea (regReg32 EAX (EBP_minus 19)))::(17,mov (regReg32 (ESP_plus 4) EAX))::
(18,mov (regAdd32 ESP mem))::(19,mov (regAdd32 ESP mem))::(20,lea (regReg32 EAX (EBP_minus 19)))::(21,retn)::nil)).

Eval compute in start (EF[|push (regi32 EBP)|])(KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::nil)
((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(9,11)::(10,11)::(11,12)::(11,19)::(12,13)::
(13,14)::(14,15)::(15,16)::(16,17)::(17,18)::(18,20)::(19,20)::(20,21)::nil)
((1,push (regi32 EBP))::(2,mov (regReg32 EBP ESP))::(3,mov (regReg32 EAX (EBP_plus 8)))::
(4,mov (regReg16 (BP_minus 28) Ax))::(5,mov (regVal32 (EBP_minus 19) 0))::(6,mov (regVal32 (EBP_minus 15) 0))::
(7,mov (regVal32 (EBP_minus 11) 0))::(8,mov (regVal32 (EBP_minus 9) 110))::(9,cmp (regVal32 (EBP_plus 12) 1))::
(10,mov (regAdd32 ECX mem))::(11,cmp (regVal32 (EBP_minus 28) 121))::(12,mov (regAdd32 ESP mem))::(13,lea (regReg32 EAX (EBP_minus 19)))::
(14,mov (regReg32 EBX EAX))::(15,call gets)::(16,lea (regReg32 EAX (EBP_minus 19)))::(17,mov (regReg32 (ESP_plus 4) EAX))::
(18,mov (regAdd32 ESP mem))::(19,mov (regAdd32 ESP mem))::(20,lea (regReg32 EAX (EBP_minus 19)))::(21,retn)::nil)).

Eval compute in start (EF(EF[|call gets|]))(KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::nil)
((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(9,11)::(10,11)::(11,12)::(11,19)::(12,13)::
(13,14)::(14,15)::(15,16)::(16,17)::(17,18)::(18,20)::(19,20)::(20,21)::nil)
((1,push (regi32 EBP))::(2,mov (regReg32 EBP ESP))::(3,mov (regReg32 EAX (EBP_plus 8)))::
(4,mov (regReg16 (BP_minus 28) Ax))::(5,mov (regVal32 (EBP_minus 19) 0))::(6,mov (regVal32 (EBP_minus 15) 0))::
(7,mov (regVal32 (EBP_minus 11) 0))::(8,mov (regVal32 (EBP_minus 9) 110))::(9,cmp (regVal32 (EBP_plus 12) 1))::
(10,mov (regAdd32 ECX mem))::(11,cmp (regVal32 (EBP_minus 28) 121))::(12,mov (regAdd32 ESP mem))::(13,lea (regReg32 EAX (EBP_minus 19)))::
(14,mov (regReg32 EBX EAX))::(15,call gets)::(16,lea (regReg32 EAX (EBP_minus 19)))::(17,mov (regReg32 (ESP_plus 4) EAX))::
(18,mov (regAdd32 ESP mem))::(19,mov (regAdd32 ESP mem))::(20,lea (regReg32 EAX (EBP_minus 19)))::(21,retn)::nil)).

Eval compute in start (EF[|call gets|]) (KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::nil)
((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(9,11)::(10,11)::(11,12)::(11,19)::(12,13)::
(13,14)::(14,15)::(15,16)::(16,17)::(17,18)::(18,20)::(19,20)::(20,21)::nil)
((1,push (regi32 EBP))::(2,mov (regReg32 EBP ESP))::(3,mov (regReg32 EAX (EBP_plus 8)))::
(4,mov (regReg16 (BP_minus 28) Ax))::(5,mov (regVal32 (EBP_minus 19) 0))::(6,mov (regVal32 (EBP_minus 15) 0))::
(7,mov (regVal32 (EBP_minus 11) 0))::(8,mov (regVal32 (EBP_minus 9) 110))::(9,cmp (regVal32 (EBP_plus 12) 1))::
(10,mov (regAdd32 ECX mem))::(11,cmp (regVal32 (EBP_minus 28) 121))::(12,mov (regAdd32 ESP mem))::(13,lea (regReg32 EAX (EBP_minus 19)))::
(14,mov (regReg32 EBX EAX))::(15,call gets)::(16,lea (regReg32 EAX (EBP_minus 19)))::(17,mov (regReg32 (ESP_plus 4) EAX))::
(18,mov (regAdd32 ESP mem))::(19,mov (regAdd32 ESP mem))::(20,lea (regReg32 EAX (EBP_minus 19)))::(21,retn)::nil)).

Eval compute in start ((EFX[op (mov (regReg32 EBX EAX)),op (call gets)]) and (EFX[op (mov (regReg32 EBP ESP)),op (mov (regReg32 EAX (EBP_plus 8)))]))(KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::nil)
((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(9,11)::(10,11)::(11,12)::(11,19)::(12,13)::
(13,14)::(14,15)::(15,16)::(16,17)::(17,18)::(18,20)::(19,20)::(20,21)::nil)
((1,push (regi32 EBP))::(2,mov (regReg32 EBP ESP))::(3,mov (regReg32 EAX (EBP_plus 8)))::
(4,mov (regReg16 (BP_minus 28) Ax))::(5,mov (regVal32 (EBP_minus 19) 0))::(6,mov (regVal32 (EBP_minus 15) 0))::
(7,mov (regVal32 (EBP_minus 11) 0))::(8,mov (regVal32 (EBP_minus 9) 110))::(9,cmp (regVal32 (EBP_plus 12) 1))::
(10,mov (regAdd32 ECX mem))::(11,cmp (regVal32 (EBP_minus 28) 121))::(12,mov (regAdd32 ESP mem))::(13,lea (regReg32 EAX (EBP_minus 19)))::
(14,mov (regReg32 EBX EAX))::(15,call gets)::(16,lea (regReg32 EAX (EBP_minus 19)))::(17,mov (regReg32 (ESP_plus 4) EAX))::
(18,mov (regAdd32 ESP mem))::(19,mov (regAdd32 ESP mem))::(20,lea (regReg32 EAX (EBP_minus 19)))::(21,retn)::nil)).

Eval compute in start ((EFX[|mov (regReg32 EBX EAX)|,|call gets|]) and (EFX[|mov (regReg32 EBP ESP)|,|mov (regReg32 EAX (EBP_plus 8))|]) and (EFX[|mov (regReg32 EAX (EBP_plus 8))|,|mov (regReg16 (BP_minus 28) Ax)|]))

(KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::nil)
((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(9,11)::(10,11)::(11,12)::(11,19)::(12,13)::
(13,14)::(14,15)::(15,16)::(16,17)::(17,18)::(18,20)::(19,20)::(20,21)::nil)
((1,push (regi32 EBP))::(2,mov (regReg32 EBP ESP))::(3,mov (regReg32 EAX (EBP_plus 8)))::
(4,mov (regReg16 (BP_minus 28) Ax))::(5,mov (regVal32 (EBP_minus 19) 0))::(6,mov (regVal32 (EBP_minus 15) 0))::
(7,mov (regVal32 (EBP_minus 11) 0))::(8,mov (regVal32 (EBP_minus 9) 110))::(9,cmp (regVal32 (EBP_plus 12) 1))::
(10,mov (regAdd32 ECX mem))::(11,cmp (regVal32 (EBP_minus 28) 121))::(12,mov (regAdd32 ESP mem))::(13,lea (regReg32 EAX (EBP_minus 19)))::
(14,mov (regReg32 EBX EAX))::(15,call gets)::(16,lea (regReg32 EAX (EBP_minus 19)))::(17,mov (regReg32 (ESP_plus 4) EAX))::
(18,mov (regAdd32 ESP mem))::(19,mov (regAdd32 ESP mem))::(20,lea (regReg32 EAX (EBP_minus 19)))::(21,retn)::nil)).

Eval compute in start (EU[|push (regi32 EBP)|,|mov (regAdd32 ESP mem)|])

(KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::nil)
((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(9,11)::(10,11)::(11,12)::(11,19)::(12,13)::
(13,14)::(14,15)::(15,16)::(16,17)::(17,18)::(18,20)::(19,20)::(20,21)::nil)
((1,push (regi32 EBP))::(2,mov (regReg32 EBP ESP))::(3,mov (regReg32 EAX (EBP_plus 8)))::
(4,mov (regReg16 (BP_minus 28) Ax))::(5,mov (regVal32 (EBP_minus 19) 0))::(6,mov (regVal32 (EBP_minus 15) 0))::
(7,mov (regVal32 (EBP_minus 11) 0))::(8,mov (regVal32 (EBP_minus 9) 110))::(9,cmp (regVal32 (EBP_plus 12) 1))::
(10,mov (regAdd32 ECX mem))::(11,cmp (regVal32 (EBP_minus 28) 121))::(12,mov (regAdd32 ESP mem))::(13,lea (regReg32 EAX (EBP_minus 19)))::
(14,mov (regReg32 EBX EAX))::(15,call gets)::(16,lea (regReg32 EAX (EBP_minus 19)))::(17,mov (regReg32 (ESP_plus 4) EAX))::
(18,mov (regAdd32 ESP mem))::(19,mov (regAdd32 ESP mem))::(20,lea (regReg32 EAX (EBP_minus 19)))::(21,retn)::nil)).


Time Eval compute in start (EF[|and (regAdd32 esp mem)|])
(KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::23::24::25::26::27::28::29::30::31::32::33::34::35::36::37::38::38::39::40::41::42::43::44::45::46::47::48::49::50::51::52::53::54::55::56::57::58::59::60::61::62::63::64::65::66::67::68::69::70::71::72::73::74::75::76::77::78::79::80::81::82::83::84::85::86::87::88::89::90::91::92::93::94::95::96::97::98::99::100::101::102::103::104::105::106::107::108::109::110::111::112::113::114::115::116::117::118::119::120::121::122::123::124::125::126::127::128::129::130::131::132::133::134::135::136::137::138::139::140::141::142::143::144::145::146::147::148::149::150::151::152::153::154::155::156::157::158::159::160::161::162::163::164::165::166::167::168::169::170::171::172::173::174::175::176::177::178::179::180::181::182::183::184::185::186::187::188::189::190::191::192::193::194::195::196::197::198::199::200::201::202::203::204::205::nil) ((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(10,11)::(11,12)::(12,13)::(12,156)::(13,14)::(13,60)::(14,15)::(15,16)::(15,26)::(16,17)::(17,18)::(18,19)::(19,20)::(20,21)::(21,22)::(22,23)::(23,24)::(24,25)::(25,35)::(26,27)::(27,28)::(28,29)::(29,30)::(30,31)::(31,32)::(32,33)::(33,34)::(34,35)::(35,36)::(36,37)::(37,38)::(38,39)::(39,40)::(40,41)::(41,42)::(42,49)::(43,44)::(44,45)::(45,46)::(46,47)::(47,48)::(48,49)::(49,50)::(50,51)::(51,52)::(51,44)::(52,53)::(54,55)::(55,56)::(56,57)::(57,58)::(58,59)::(60,61)::(61,62)::(62,63)::(63,64)::(64,65)::(65,66)::(66,67)::(67,68)::(67,86)::(68,69)::(69,70)::(70,71)::(71,72)::(72,73)::(72,91)::(73,74)::(74,75)::(75,76)::(76,77)::(76,83)::(77,78)::(78,79)::(79,80)::(80,81)::(81,82)::(82,83)::(82,79)::(83,84)::(84,85)::(85,86)::(86,87)::(87,88)::(88,89)::(89,90)::(91,91)::(92,92)::(93,93)::(94,94)::(95,100)::(96,96)::(97,97)::(98,98)::(99,99)::(100,100)::(101,101)::(101,96)::(102,73)::(103,104)::(104,105)::(105,106)::(106,107)::(107,108)::(108,109)::(109,110)::(110,111)::(111,112)::(112,113)::(113,114)::(114,115)::(115,116)::(116,117)::(117,118)::(118,119)::(119,120)::(120,121)::(120,126)::(121,150)::(122,123)::(123,124)::(124,125)::(125,126)::(125,133)::(126,127)::(127,128)::(128,129)::(128,122)::(129,130)::(130,131)::(131,132)::(132,133)::(132,126)::(133,134)::(134,135)::(135,136)::(135,140)::(136,137)::(137,138)::(138,139)::(140,140)::(141,141)::(142,142)::(143,143)::(144,144)::(145,145)::(146,146)::(147,147)::(148,148)::(149,149)::(149,175)::(150,150)::(151,151)::(152,152)::(153,153)::(154,154)::(155,145)::(156,157)::(157,158)::(158,159)::(159,160)::(160,161)::(161,162)::(162,163)::(162,167)::(163,164)::(164,165)::(165,166)::(166,160)::(167,168)::(168,169)::(169,170)::(170,171)::(171,172)::(172,173)::(173,174)::(175,176)::(176,177)::(177,178)::(178,179)::(179,180)::(180,181)::(181,182)::(182,183)::(183,184)::(184,185)::(185,186)::(186,187)::(187,188)::(188,189)::(189,190)::(190,191)::(191,192)::(192,193)::(193,194)::(194,195)::(195,196)::(196,197)::(197,198)::(198,199)::(199,200)::(200,201)::(201,202)::(202,203)::(203,204)::(204,205)::nil) ((1, push (regi32 ebp))::(2, mov (regReg32 ebp esp))::(3, mov (regReg32 ebp esp))::(4, and (regAdd32 esp mem))::(5, mov (regAdd32 eax mem))::(6, add (regAdd32 eax mem))::(7, add (regAdd32 eax mem))::(8, mov (regReg32 ebp esp))::(9, mov (regReg32 ebp esp))::(10, mov (regReg32 (ebp_minus 44) eax))::(11, mov (regReg32 eax (ebp_minus 44)))::(12, call generic)::(13, call generic)::(14, cmp (regAdd32 (ebp_plus 8) mem))::(15, mov (regReg32 ebp esp))::(16, mov (regReg32 eax (ebp_plus 12)))::(17, mov (regReg32 eax eax))::(18, mov (regReg32 (esp_plus 8) eax))::(19, mov (regAdd32 (esp_plus 4) mem))::(20, mov (regAdd32 eax mem))::(21, add (regAdd32 eax mem))::(22, mov (regReg32 esp eax))::(23, call generic)::(24, mov (regAdd32 (ebp_minus 28) mem))::(25, jmp (addr32 mem))::(26, mov (regReg32 ebp esp))::(27, mov (regReg32 eax (ebp_plus 12)))::(28, add (regAdd32 eax mem))::(29, mov (regReg32 eax eax))::(30, mov (regReg32 (esp_plus 4) eax))::(31, lea (regReg32 eax (ebp_minus 24)))::(32, mov (regReg32 esp eax))::(33, call generic)::(34, mov (regAdd32 (ebp_minus 28) mem))::(35, mov (regReg32 ebp esp))::(36, mov (regReg32 eax (ebp_minus 28)))::(37, mov (regReg32 ebp esp))::(38, retn)::(38, mov (regReg32 ebp esp))::(39, push (regi32 ebp))::(40, mov (regAdd32 ecx mem))::(41, mov (regReg32 ebp esp))::(42, jmp (addr32 mem))::(43, mov (regReg32 ebp esp))::(44, mov (regReg32 ebp esp))::(45, mov (regReg32 edx (ecx_plus 4)))::(46, mov (regReg32 eax ecx))::(47, add (regAdd32 ecx mem))::(48, add (regReg32 (edx_plus 419) eax))::(49, mov (regReg32 ebp esp))::(50, cmp (regAdd32 ecx mem))::(51, mov (regReg32 ebp esp))::(52, pop (regi32 ebp))::(53, retn)::(54, mov (regReg32 ebp esp))::(55, push (regi32 ebp))::(56, mov (regReg32 ebp esp))::(57, mov (regReg32 ebp esp))::(58, pop (regi32 ebp))::(59, retn)::(60, mov (regReg32 ebp esp))::(61, push (regi32 ebp))::(62, mov (regReg32 ebp esp))::(63, push (regi32 ebx))::(64, mov (regReg32 ebp esp))::(65, mov (regAdd32 eax mem ))::(66, test (regReg32 eax eax))::(67, jnz (addr32 mem))::(68, mov (regAdd32 eax mem ))::(69, mov (regAdd32 ebx mem))::(70, mov (addReg32 mem  ebx))::(71, cmp (regAdd32 eax mem))::(72, jz (addr32 mem))::(73, mov (regReg32 ebp esp))::(74, test (regReg32 eax eax))::(75, mov (regReg32 ebx eax))::(76, jz (addr32 mem))::(77, mov (regReg32 ebp esp))::(78, lea (regReg32 esi (esi_plus 0)))::(79, mov (regReg32 ebp esp))::(80, call generic)::(81, mov (regReg32 ebp esp))::(82, jnz (addr32 mem))::(83, mov (regReg32 ebp esp))::(84, mov (regAdd32 esp mem))::(85, call generic)::(86, mov (regReg32 ebp esp))::(87, pop (regi32 ebx))::(88, pop (regi32 ebx))::(89, pop (regi32 ebp))::(90, retn)::(91, mov (regReg32 ebp esp))::(92, mov (regAdd32 ecx mem ))::(93, xor (regReg32 eax eax))::(94, test (regReg32 ecx ecx))::(95, jmp (addr32 mem))::(96, mov (regReg32 ebp esp))::(97, mov (regReg32 ebp esp))::(98, mov (regReg32 edx eax))::(99, test (regReg32 edx edx))::(100, mov (regReg32 ebp esp))::(101, jnz (addr32 mem))::(102, jmp (addr32 mem))::(103, mov (regReg32 ebp esp))::(104, push (regi32 ebp))::(105, mov (regAdd32 edx mem))::(106, mov (regReg32 ebp esp))::(107, push (regi32 ebx))::(108, mov (regReg32 ebp esp))::(109, mov (regReg32 ebp esp))::(110, mov (regReg32 (esp_plus 8) edx))::(111, lea (regReg32 edx (ebp_minus 88)))::(112, xor (regReg32 ebx ebx))::(113, mov (regReg32 (esp_plus 4) edx))::(114, mov (regReg32 esp eax))::(115, call generic)::(116, mov (regAdd32 edx mem))::(117, mov (regAdd32 ecx mem))::(118, mov (regReg32 ebp esp))::(119, test (regReg32 eax eax))::(120, jnz (addr32 mem))::(121, jmp (addr32 mem))::(122, mov (regReg32 ebp esp))::(123, add (regReg32 ecx ecx))::(124, mov (regReg32 ebp esp))::(125, mov (regReg32 ebp esp))::(126, mov (regReg32 ebp esp))::(127, cmp (regAdd32 (edx_plus 88) mem))::(128, jnz (addr32 mem))::(129, mov (regReg32 ebp esp))::(130, add (regReg32 ecx ecx))::(131, mov (regReg32 ebp esp))::(132, mov (regReg32 ebp esp))::(133, mov (regReg32 ebp esp))::(134, cmp (regAdd32 ebx mem))::(135, jnz (addr32 mem))::(136, mov (regReg32 eax ebx))::(137, mov (regReg32 ebx (ebp_minus 4)))::(138, mov (regReg32 ebp esp))::(139, retn)::(140, mov (regReg32 ebp esp))::(141, mov (regAdd32 ecx mem))::(142, mov (regAdd32 edx mem))::(143, mov (regReg32 (esp_plus 12) ecx))::(144, mov (regReg32 (esp_plus 8) edx))::(145, mov (regReg32 ebp esp))::(146, mov (regAdd32 esp mem))::(147, mov (regAdd32 eax mem))::(148, mov (regReg32 (esp_plus 4) eax))::(149, call generic)::(150, mov (regReg32 ebp esp))::(151, mov (regAdd32 eax mem))::(152, mov (regAdd32 ebx mem))::(153, mov (regReg32 (esp_plus 12) eax))::(154, mov (regReg32 (esp_plus 8) ebx))::(155, jmp (addr32 mem))::(156, mov (regReg32 ebp esp))::(157, push (regi32 ecx))::(158, mov (regReg32 ecx esp))::(159, add (regAdd32 ecx mem))::(160, mov (regReg32 ebp esp))::(161, cmp (regAdd32 eax mem))::(162, mov (regReg32 ebp esp))::(163, mov (regReg32 ebp esp))::(164, mov (regReg32 ebp esp))::(165, mov (regReg32 ebp esp))::(166, jmp (addr32 mem))::(167, mov (regReg32 ebp esp))::(168, mov (regReg32 ebp esp))::(169, mov (regReg32 ebp esp))::(170, mov (regReg32 eax esp))::(171, mov (regReg32 esp ecx))::(172, mov (regReg32 ecx eax))::(173, mov (regReg32 eax (eax_plus 4)))::(174,  jmp (addr32 mem))::(175, mov (regReg32 ebp esp))::(176, push (regi32 ebp))::(177, mov (regReg32 ebp esp))::(178, mov (regReg32 ebp esp))::(179, mov (regReg32 eax (ebp_plus 20)))::(180, mov (regReg32 (esp_plus 16) eax))::(181, mov (regReg32 eax (ebp_plus 16)))::(182, mov (regReg32 (esp_plus 12) eax))::(183, mov (regReg32 eax (ebp_plus 12)))::(184, mov (regReg32 (esp_plus 8) eax))::(185, mov (regReg32 eax (ebp_plus 8)))::(186, mov (regReg32 (esp_plus 4) eax))::(187, mov (regAdd32 eax mem))::(188, add (regAdd32 eax mem))::(189, mov (regReg32 esp eax))::(190, call generic)::(191, mov (regAdd32 eax mem))::(192, add (regAdd32 eax mem))::(193, mov (regReg32 esp eax))::(194, call generic)::(195, call generic)::(196, mov (regReg32 ebp esp))::(197, mov (regReg32 ebp esp))::(198, mov (regReg32 ebp esp))::(199, mov (regReg32 ebp esp))::(200, mov (regReg32 ebp esp))::(201, mov (regReg32 ebp esp))::(202, mov (regReg32 ebp esp))::(203, mov (regReg32 ebp esp))::(204, mov (regReg32 ebp esp))::(205,  jmp (addr32 mem))::nil)).

Time Eval compute in start (EF[|and (regAdd32 esp mem)|])



(KripkeS (1::2::3::4::5::6::7::8::9::10::11::12::13::14::15::16::17::18::19::20::21::22::23::24::25::26::27::28::29::30::31::32::33::34::35::36::37::38::38::39::40::41::42::43::44::45::46::47::48::49::50::51::52::53::54::55::56::57::58::59::60::61::62::63::64::65::66::67::68::69::70::71::72::73::74::75::76::77::78::79::80::81::82::83::84::85::86::87::88::89::90::91::92::93::94::95::96::97::98::99::100::101::102::103::104::105::106::107::108::109::110::111::112::113::114::115::116::117::118::119::120::121::122::123::124::125::126::127::128::129::130::131::132::133::134::135::136::137::138::139::140::141::142::143::144::145::146::147::148::149::150::151::152::153::154::155::156::157::158::159::160::161::162::163::164::165::166::167::168::169::170::171::172::173::174::175::176::177::178::179::180::181::182::183::184::185::186::187::188::189::190::191::192::193::194::195::196::197::198::199::200::201::202::203::204::205::nil) ((1,2)::(2,3)::(3,4)::(4,5)::(5,6)::(6,7)::(7,8)::(8,9)::(9,10)::(10,11)::(11,12)::(12,13)::(12,156)::(13,14)::(13,60)::(14,15)::(15,16)::(15,26)::(16,17)::(17,18)::(18,19)::(19,20)::(20,21)::(21,22)::(22,23)::(23,24)::(24,25)::(25,35)::(26,27)::(27,28)::(28,29)::(29,30)::(30,31)::(31,32)::(32,33)::(33,34)::(34,35)::(35,36)::(36,37)::(37,38)::(38,39)::(39,40)::(40,41)::(41,42)::(42,49)::(43,44)::(44,45)::(45,46)::(46,47)::(47,48)::(48,49)::(49,50)::(50,51)::(51,52)::(51,44)::(52,53)::(54,55)::(55,56)::(56,57)::(57,58)::(58,59)::(60,61)::(61,62)::(62,63)::(63,64)::(64,65)::(65,66)::(66,67)::(67,68)::(67,86)::(68,69)::(69,70)::(70,71)::(71,72)::(72,73)::(72,91)::(73,74)::(74,75)::(75,76)::(76,77)::(76,83)::(77,78)::(78,79)::(79,80)::(80,81)::(81,82)::(82,83)::(82,79)::(83,84)::(84,85)::(85,86)::(86,87)::(87,88)::(88,89)::(89,90)::(91,91)::(92,92)::(93,93)::(94,94)::(95,100)::(96,96)::(97,97)::(98,98)::(99,99)::(100,100)::(101,101)::(101,96)::(102,73)::(103,104)::(104,105)::(105,106)::(106,107)::(107,108)::(108,109)::(109,110)::(110,111)::(111,112)::(112,113)::(113,114)::(114,115)::(115,116)::(116,117)::(117,118)::(118,119)::(119,120)::(120,121)::(120,126)::(121,150)::(122,123)::(123,124)::(124,125)::(125,126)::(125,133)::(126,127)::(127,128)::(128,129)::(128,122)::(129,130)::(130,131)::(131,132)::(132,133)::(132,126)::(133,134)::(134,135)::(135,136)::(135,140)::(136,137)::(137,138)::(138,139)::(140,140)::(141,141)::(142,142)::(143,143)::(144,144)::(145,145)::(146,146)::(147,147)::(148,148)::(149,149)::(149,175)::(150,150)::(151,151)::(152,152)::(153,153)::(154,154)::(155,145)::(156,157)::(157,158)::(158,159)::(159,160)::(160,161)::(161,162)::(162,163)::(162,167)::(163,164)::(164,165)::(165,166)::(166,160)::(167,168)::(168,169)::(169,170)::(170,171)::(171,172)::(172,173)::(173,174)::(175,176)::(176,177)::(177,178)::(178,179)::(179,180)::(180,181)::(181,182)::(182,183)::(183,184)::(184,185)::(185,186)::(186,187)::(187,188)::(188,189)::(189,190)::(190,191)::(191,192)::(192,193)::(193,194)::(194,195)::(195,196)::(196,197)::(197,198)::(198,199)::(199,200)::(200,201)::(201,202)::(202,203)::(203,204)::(204,205)::nil) ((1, push (regi32 ebp))::(2, mov (regReg32 ebp esp))::(3, mov (regReg32 ebp esp))::(4, mov (regReg32 ebp esp))::(5, mov (regAdd32 eax mem))::(6, add (regAdd32 eax mem))::(7, add (regAdd32 eax mem))::(8, add (regAdd32 eax mem))::(9, add (regAdd32 eax mem))::(10, mov (regReg32 (ebp_minus 2) eax))::(11, mov (regReg32 eax (ebp_minus 2)))::(12, call strcpy)::(13, call strcpy)::(14, cmp (regAdd32 (ebp_plus 8) mem))::(15, add (regAdd32 eax mem))::(16, mov (regReg32 eax (ebp_plus 0)))::(17, mov (regReg32 eax eax))::(18, mov (regReg32 (esp_plus 8) eax))::(19, mov (regAdd32 (esp_plus 4) mem))::(20, mov (regAdd32 eax  mem))::(21, add (regAdd32 eax mem))::(22, mov (regReg32 esp eax))::(23, call strcpy)::(24, mov (regAdd32 (ebp_minus 1) mem))::(25, jmp (addr32 mem))::(26,mov (regReg32 ebp esp))::(27, mov (regReg32 eax eax))::(28, add (regAdd32 eax mem))::(29, mov (regReg32 eax eax))::(30, mov (regReg32 (esp_plus 4) eax))::(31, lea (regReg32 eax (ebp_minus 18)))::(32, mov (regReg32 esp eax))::(33, call strcpy)::(34, mov (regAdd32 (ebp_minus 1) mem))::(35, mov (regReg32 ebp esp))::(36, mov (regReg32 eax (ebp_minus 1)))::(37, add (regAdd32 eax mem))::(38, retn)::(38, add (regAdd32 eax mem))::(39, push (regi32 ebp))::(40, mov (regAdd32 ecx mem))::(41, mov (regReg32 ebp esp))::(42, jmp (addr32 mem))::(43, add (regAdd32 eax mem))::(44, mov (regReg32 ebp esp))::(45, mov (regReg32 edx (ebp_plus 4)))::(46, mov (regReg32 eax ecx))::(47, add (regAdd32 ecx mem))::(48, add (regReg32 (ebp_plus 40) eax))::(49, mov (regReg32 ebp esp))::(50, cmp (regAdd32 ecx mem))::(51, add (regAdd32 eax mem))::(52, pop (regi32 ebp))::(53, retn)::(54, add (regAdd32 eax mem))::(55, push (regi32 ebp))::(56, mov (regReg32 ebp esp))::(57, add (regAdd32 eax mem))::(58, pop (regi32 ebp))::(59, retn)::(60, add (regAdd32 eax mem))::(61, push (regi32 ebp))::(62, mov (regReg32 ebp esp))::(63, push (regi32 ebx))::(64, add (regAdd32 eax mem))::(65, mov (regAdd32 eax mem))::(66, test (regReg32 eax eax))::(67, jnz (addr32 mem))::(68, mov (regAdd32 eax mem) )::(69, mov (regAdd32 ebx mem))::(70, mov (addReg32 mem ebx))::(71, cmp (regAdd32 eax mem))::(72, jz (addr32 mem))::(73, mov (regReg32 ebp esp))::(74, test (regReg32 eax eax))::(75, mov (regReg32 ebx eax))::(76, jz (addr32 mem))::(77, add (regAdd32 eax mem))::(78, lea (regReg32 esi (ebp_plus 0)))::(79, mov (regReg32 ebp esp))::(80, call strcpy)::(81, add (regAdd32 eax mem))::(82, jnz (addr32 mem))::(83,mov (regReg32 ebp esp))::(84, mov (regAdd32 esp mem))::(85, call strcpy)::(86, mov (regReg32 ebp esp))::(87, pop (regi32 ebx))::(88, pop (regi32 ebx))::(89, pop (regi32 ebp))::(90, retn)::(91, mov (regReg32 ebp esp))::(92, mov (regAdd32 ecx mem))::(93, xor (regReg32 eax eax))::(94, test (regReg32 ecx ecx))::(95, jmp (addr32 mem))::(96, mov (regReg32 ebp esp))::(97, add (regAdd32 eax mem))::(98, mov (regReg32 edx eax))::(99, test (regReg32 edx edx))::(100, mov (regReg32 ebp esp))::(101, jnz (addr32 mem))::(102, jmp (addr32 mem))::(103, add (regAdd32 eax mem))::(104, push (regi32 ebp))::(105, mov (regAdd32 edx mem))::(106, mov (regReg32 ebp esp))::(107, push (regi32 ebx))::(108, add (regAdd32 eax mem))::(109, add (regAdd32 eax mem))::(110, mov (regReg32 (esp_plus 8) edx))::(111, lea (regReg32 edx (ebp_minus 58)))::(112, xor (regReg32 ebx ebx))::(113, mov (regReg32 (esp_plus 4) edx))::(114, mov (regReg32 esp eax))::(115, call strcpy)::(116, mov (regAdd32 edx mem))::(117, mov (regAdd32 ecx mem))::(118, add (regAdd32 eax mem))::(119, test (regReg32 eax eax))::(120, jnz (addr32 mem))::(121, jmp (addr32 mem))::(122, mov (regReg32 ebp esp))::(123, add (regReg32 ecx ecx))::(124, add (regAdd32 eax mem))::(125, add (regAdd32 eax mem))::(126, mov (regReg32 ebp esp))::(127, cmp (regAdd32 (ebp_plus 58) mem))::(128, jnz (addr32 mem))::(129, mov (regReg32 ebp esp))::(130, add (regReg32 ecx ecx))::(131, add (regAdd32 eax mem))::(132, add (regAdd32 eax mem))::(133,mov (regReg32 ebp esp))::(134, cmp (regAdd32 ebx mem))::(135, jnz (addr32 mem))::(136, mov (regReg32 eax ebx))::(137, mov (regReg32 ebx (ebp_minus 4)))::(138, add (regAdd32 eax mem))::(139, retn)::(140, mov (regReg32 ebp esp))::(141, mov (regAdd32 ecx mem))::(142, mov (regAdd32 edx mem))::(143, mov (regReg32 (esp_plus 0) ecx))::(144, mov (regReg32 (esp_plus 8) edx))::(145, mov (regReg32 ebp esp))::(146, mov (regAdd32 esp mem))::(147, mov (regAdd32 eax mem))::(148, mov (regReg32 (esp_plus 4) eax))::(149, call strcpy)::(150, mov (regReg32 ebp esp))::(151, mov (regAdd32 eax mem))::(152, mov (regAdd32 ebx mem))::(153, mov (regReg32 (esp_plus 0) eax))::(154, mov (regReg32 (esp_plus 8) ebx))::(155, jmp (addr32 mem))::(156, add (regAdd32 eax mem))::(157, push (regi32 ecx))::(158, mov (regReg32 ecx esp))::(159, add (regAdd32 ecx mem))::(160, mov (regReg32 ebp esp))::(161, cmp (regAdd32 eax mem))::(162, add (regAdd32 eax mem))::(163, add (regAdd32 eax mem))::(164, mov (regReg32 ebp esp))::(165, add (regAdd32 eax mem))::(166, add (regAdd32 eax mem))::(167, mov (regReg32 ebp esp))::(168, add (regAdd32 eax mem))::(169, mov (regReg32 ebp esp))::(170, mov (regReg32 eax esp))::(171, mov (regReg32 esp ecx))::(172, mov (regReg32 ecx eax))::(173, mov (regReg32 eax (ebp_plus 4)))::(174, jmp (addr32 mem))::(175, add (regAdd32 eax mem))::(176, push (regi32 ebp))::(177, mov (regReg32 ebp esp))::(178, add (regAdd32 eax mem))::(179, mov (regReg32 eax (ebp_plus 14)))::(180, mov (regReg32 (esp_plus 10) eax))::(181, mov (regReg32 eax (ebp_plus 10)))::(182, mov (regReg32 (esp_plus 0) eax))::(183, mov (regReg32 eax (ebp_plus 0)))::(184, mov (regReg32 (esp_plus 8) eax))::(185, mov (regReg32 eax (ebp_plus 8)))::(186, mov (regReg32 (esp_plus 4) eax))::(187, mov (regAdd32 eax mem))::(188, add (regAdd32 eax mem))::(189, mov (regReg32 esp eax))::(190, call strcpy)::(191, mov (regAdd32 eax mem))::(192, add (regAdd32 eax mem))::(193, mov (regReg32 esp eax))::(194, call strcpy)::(195, call strcpy)::(196, add (regAdd32 eax mem))::(197, add (regAdd32 eax mem))::(198, add (regAdd32 eax mem))::(199, add (regAdd32 eax mem))::(200, add (regAdd32 eax mem))::(201, add (regAdd32 eax mem))::(202, add (regAdd32 eax mem))::(203, add (regAdd32 eax mem))::(204, add (regAdd32 eax mem))::(205, jmp (addr32 mem))::nil)).
*)

(* begin hide *)
End CTPL.

Extraction "poctpl.ml" start . 