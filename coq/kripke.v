Section kripke.

Require Import List.
Require Export structures.

(**
Definimos lo que va a ser un camino
*)

Inductive Path (A:Type):Type :=
   | PNil : Path A
   | PCons : opcode -> Path A -> Path A.

(**
Y la notacion que vamos a usar para mostrarlo. En este caso usaremos la 
notacion "-->" para expresar el paso de un elemento a otro y "$[$ $]$" para
denotar el final del camino
*)

Infix "-->" := (PCons opcode) (at level 60, right associativity) : list_scope.

Notation " [ ] " := (PNil opcode) (at level 30).

Definition path: Type := list nat.

Definition nodeTree: Type := list path.

(*Definition CTPLPath: Type := list opcode.

Definition CTPLTree: Type := list CTPLPath.*)

Definition CTPLPath: Type := Path opcode. 

Definition CTPLTree: Type := list CTPLPath. 

Record kripke: Set :=
   KripkeS {
       S_List :> list nat;
       R_List : list (nat*nat);
       L_List : list (nat*opcode)
   }.



Fixpoint searchNode (x:nat)(l:list (nat*nat)){struct l}: path :=
   match l with
      | nil => nil
      | (a,b)::m => match (natCompB a x) with
            | true =>  b::(searchNode x m)
            | false => (searchNode x m)
         end
   end.

Fixpoint addToAll (x:nat)(l:nodeTree): nodeTree :=
   match l with
      | nil => nil
      | n::m => (x::n)::(addToAll x m)
   end.

Fixpoint searchPath (lNodos:list (nat*nat))(nI nF:nat){struct lNodos}: nodeTree := 
   let fix allNodes (lNodosInt:list nat):list (list nat) :=
      match lNodosInt with
         | nInt::l =>  match lNodos with
               | nil => nil
               | (n',m')::l' => (addToAll nI (searchPath l' nInt nF))++(allNodes l)
            end
         | nil => nil
      end
   in 
      if (natCompB nI nF)
         then (nF::nil)::nil
         else allNodes (searchNode nI lNodos).

Fixpoint next(l:CTPLTree):CTPLTree :=
   match l with
      | nil => nil
      | a::l' => match a with
            | PNil => nil
            | PCons b l'' => (b-->[])::(next l')
         end
   end.

Fixpoint getListOpcode (l3:path)(l4:list (nat*opcode)):CTPLPath :=
   match l3 with
      | nil => []
      | n::l'' => match (getOpcode n l4) with
            | None => getListOpcode l'' l4
            | Some x => x-->(getListOpcode l'' l4)
         end
   end.

Fixpoint natToOpcode (l1:nodeTree)(l2:list (nat*opcode)):CTPLTree :=
   match l1 with
      | nil => nil
      | a::l' => (getListOpcode a l2)::(natToOpcode l' l2)
   end.

Fixpoint cutList (o:opcode)(l:CTPLPath){struct l}: CTPLPath :=
   match l with 
      | PNil => []
      | PCons a l' => match (opCompare a o ) with
            | true => l'
            | false => cutList o l'
         end
   end.

Fixpoint compareList (n:opcode)(l:CTPLPath):bool :=
   match l with
      | PNil => false
      | PCons a l' => match opCompare a n with
            | true => true
            | false => compareList n l'
         end
   end.

Fixpoint compareListG (n:opcode)(l:CTPLPath):bool :=
   match l with
      | PNil => true
      | PCons a l' => match opCompare a n with
            | true => compareListG n l'
            | false => false
         end
   end.

Fixpoint compareListF (n:opcode)(l:CTPLPath):bool :=
   match l with
      | PNil => false
      | PCons a l' => match opCompare a n with
            | true => true
            | false => compareListF n l'
         end
   end.

Definition compareListX (n:opcode)(l:CTPLPath):bool :=
   match l with
      | PNil => false
      | PCons a l' => match opCompare a n with
            | true => true
            | false => false
         end
   end.

Fixpoint opcodeList (l:list (nat*opcode)):list opcode :=
   match l with
      | nil => nil
      | (n,o)::l' => o::(opcodeList l')
   end.

Fixpoint getRet (l:list (nat*opcode)):nat :=
   match l with
      | nil => 0
      | (n,o)::l'=> match o with
                       | retn => n
                       | _ => getRet l'
                    end
   end.

End kripke.