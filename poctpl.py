import modulaco
import analysis

import os
import sys
import shutil


def ayuda():
	print '''\
Version 0.2
Introduce: main.py <ruta binario>

Programa para buscar formulas POCTPL en binarios.
Introduce: main.py <ruta binario> 

Options include:
  --version : Muestra la version del programa
  --help	: Muestra esta ayuda'''

if __name__ == "__main__":
	if len(sys.argv) <= 1:
		ayuda()
	else:
		if sys.argv[1].startswith('--'):
			option = sys.argv[1][2:]
			# fetch sys.argv[1] but without the first two characters
			if option == 'version':
				print 'Version 0.2'
			elif option == 'help':
				ayuda()
			else:
				print 'Unknown option.'
			sys.exit()
		else:
			if os.path.exists(sys.argv[1]):
				formula = analysis.analizador()
				#print formula
				#raw_input("pulsa para continuar")
				print "[ Empezamos ]"
				directorio = os.path.join(os.getcwd(), 'results')
				if not os.path.isdir(directorio):
					os.mkdir(directorio)
				directorio = os.path.join(os.getcwd(), 'tmp')
				if not os.path.isdir(directorio):
					os.mkdir(directorio)
				fileName, fileExtension = os.path.splitext(sys.argv[1])
				print "[ Generando ensamblador ]"
				os.system('\"C:\Program Files\IDA Free\idag.exe\" -B '+sys.argv[1]+" -otest")
				print "[ Borrando ficheros auxiliares ]"
				#raw_input("pulsa para continuar")
				os.system('del test.idb')
				print fileName
				modulaco.asmToPoctpl("test.asm", 1)
				shutil.move("test.asm",os.getcwd()+"\\results\\assembly.asm")
				# Generar el grafico
				#os.system("dot -v -Tpng results\\grafico.dot -o results\\grafico.png")
				print "Buscando los nodos iniciales"
				
				print "Comprobando formula en la estructura kripke"
				#print sys.argv[2]
				f = open(os.getcwd()+"\\results\\kripke.txt", "r")
				formulaca = ""
				for linea in f:
					formulaca+= linea
					if "), )) @->" in linea:
						print linea
				f.close()
				
				f = open(os.getcwd()+"\\tmp\inicial.dat","r")
				for inicio in f:
				# Generando el fichero ml para compilar con ocaml
					shutil.copy("logic\\poctpl.ml", "tmp\\")
					f = open("tmp\\poctpl.ml", "a")
					f.write("let rec startFalso o k l =\n")
					f.write("\tmatch l with\n")
					f.write("\t\t| Nil ->  Nil\n")
					f.write("\t\t| Cons (p, m) ->\n")
					f.write("\t\t\tmatch (start3 o (opcodeList k.l_List)\n")
					f.write("\t\t\t(natToOpcode (searchPath k.r_List ("+inicio+") p) k.l_List)) with\n")
					f.write("\t\t\t| Nil -> startFalso o k m\n")
					f.write("\t\t\t| a -> joinTree a (startFalso o k m)\n\n\n")
					f.write("let start o k =\n")
					f.write("\tstartFalso o k (getRetList k.l_List)\n\n\n")
					f.write("let (@->) el l = Cons (el,l);;\n")
					f.write("let (@>) el l = PCons (el,l);;\n")
					f.write("let z = (start ("+formula+")\n")
					f.write(formulaca)
					f.write("\n);;\n\nprint_list2 z;;")
					f.close()
					# compilar con ocaml
					os.system("ocaml tmp\\poctpl.ml")
			else:
				print "[ERROR] Ruta incorrecta"

